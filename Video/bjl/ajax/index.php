
	<?php
date_default_timezone_set("Asia/Shanghai");
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
include_once "../../../Public/config.php";
include_once "../../../Public/Bjl.php";
$ajaxHandler = $_GET['ajaxHandler'];
switch ($ajaxHandler) {
    case 'GetBac1AwardData':
        $data = array();
        $bjl = new Bjl();
        $cur = $bjl->get_period_info($bjl->getTodayCur());
        $codes = $bjl->newCode(false);
        if (!$codes) {
            exit();
        }
        $pb = $bjl->getPB($codes);
        $data['time'] = time() * 1000;
        $data['result'] = array(
            "Bet" => array(
                'P',
                'B'
            ),
            "Player" => $pb['p'],
            "Banker" => $pb['b'],
            "Result" => $bjl->Result($pb['p'], $pb['b']),
            "HtmlCard" => array(
                "Player" => $bjl->HtmlCard($pb['p']),
                "Banker" => $bjl->HtmlCard($pb['b'])
            )
        );
        $data['next'] = array(
            'awardTime' => $cur['next_awardTime'],
            'awardTimeInterval' => (strtotime($cur['next_awardTime']) - time()) * 1000,
            'delayTimeInterval' => 5,
            'periodNumber' => $cur['next_periodNumber']
        );
        $data['current'] = array(
            'awardNumbers' => $codes,
            'awardTime' => $cur['awardTime'],
            'periodNumber' => $cur['periodNumber']
        );
        exit(json_encode($data));
        break;
    case 'GetBac1HistData':
        db_query("select * from fn_open where type=10 order by term desc limit 25 ");
        $data = array();
        $bjl = new Bjl();
        while ($row = db_fetch_array()) {
            $code = $row['code'];
            $pb = $bjl->getPB($code);
            $data["" . $row['term']] = array(
                "Player" => $pb['p'],
                "Banker" => $pb['b'],
                "Result" => $bjl->Result($pb['p'], $pb['b']),
                "HtmlCard" => array(
                    "Player" => $bjl->HtmlCard($pb['p']),
                    "Banker" => $bjl->HtmlCard($pb['b'])
                )
            );
        }
        exit(json_encode($data));
        break;
    default:
        break;
}