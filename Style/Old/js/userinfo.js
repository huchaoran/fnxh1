
function clearAllCookie() {
    var keys = document.cookie.match(/[^ =;]+(?=\=)/g);
    if (keys) {
        for (var i = keys.length; i--;)
            document.cookie = keys[i] + '=0;path=/Templates/Old;expires=' + new Date(0).toUTCString()
    }
}
function autoWidth(e) {
    var vw = document.body.clientWidth;
    var scale = vw / 500;
    var height = 630 / 980 * vw;
    $(e).css({
        transform: `scale(${scale})`,
        transformOrigin: "top center",
        opacity: 1,
    });
    $('#iframe_sizeID').css({
        height: height + 'px',
        pointerEvents: "none"
    })
}
function initIframeHeight(height) {
    var userAgent = navigator.userAgent;
    var iframe = parent.document.getElementById("ifarms");
    // var iframe = document.getElementById("ifarms");
    var subdoc = iframe.contentDocument || iframe.contentWindow.document;
    var subbody = subdoc.body;
    var realHeight;
    //谷歌浏览器特殊处理
    if (userAgent.indexOf("Chrome") > -1) {
        realHeight = subdoc.documentElement.scrollHeight;
    } else {
        realHeight = subbody.scrollHeight;
    }
    if (realHeight < height) {
        $(iframe).height(height);
    } else {
        $(iframe).height(realHeight);
    }
}

function SetWinHeight(obj) {
    var win = obj;
    if (document.getElementById) {
        if (win && !window.opera) {
            if (win.contentDocument && win.contentDocument.body.offsetHeight) {
                if (win.contentDocument.body.offsetHeight < 30) {
                    win.height = 30; //设置最小高度
                } else {
                    win.height = win.contentDocument.body.offsetHeight;
                }
            } else if (win.Document && win.Document.body.scrollHeight)
                win.height = win.Document.body.scrollHeight;
        }
    }
}
function showMask() {
    $("#mask").css("height", $(document).height());
    $("#mask").css("width", $(document).width());
    $("#mask").show();
}

function hideMask() {
    $("#mask").hide();
}