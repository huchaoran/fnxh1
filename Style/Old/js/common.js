Promise.prototype.complete=function(callback){
	let p = this.constructor
	return this.then(
	  value =>p.resolve(callback()).then(()=>value),
	  reson => p.reject(callback()).then(()=>{throw reson})
	)
}

export  let requestAjaxSyn=(url,data={},params={},successfn,completefn)=>{
	
	   $.ajax({
		   url:url,
		   data:data,
		   type:params.type,
		   async: false,
		   success:function(res){
			   successfn(res)
		   },
		   error:function(res){
			   reject(res)
		   },
		   dataType:params.dataType,
		   complete:function(){
		      completefn()
		   }
	   })	
	
}
export  let requestAjax=(url,data={},params={})=>{
	return  new Promise((resolve,reject)=>{
	   $.ajax({
		   url:url,
		   data:data,
		   type:params.type,
		   success:function(res){
			   resolve(res)
		   },
		   error:function(res){
			   reject(res)
		   },
		   dataType:params.dataType
	   })	
	}).catch((e)=>{
		console.log('请求异常11',e)
	})
}
// 时间格式化
export  let disposeDate = (nowtime,nexttime)=>{
	       //          var nowtime = new Date(),  //获取当前时间
	    			// endtime = new Date("2020/8/8");  //定义结束时间
	    			// console.log(nowtime.getTime());
	    			// console.log(endtime.getTime());
	    			// var lefttime = endtime.getTime() - nowtime.getTime() //距离结束时间的毫秒数
	    			var lefttime = nexttime - nowtime
					var leftd = Math.floor(lefttime/(1000*60*60*24))  //计算天数
	    			var lefth = Math.floor(lefttime/(1000*60*60)%24)  //计算小时数
	    			var leftm = Math.floor(lefttime/(1000*60)%60)  //计算分钟数
	    			var lefts = Math.floor(lefttime/1000%60);  //计算秒数
	    		// return leftd + "天" + lefth + "时" + leftm + "分" + lefts + '秒'
				return (1000*60*leftm)+(lefts*1000);  //返回倒计时的字符串
} 
 export  let testdisposeDate = (nowtime)=>{
 	       //          var nowtime = new Date(),  //获取当前时间
 	    			// endtime = new Date("2020/8/8");  //定义结束时间
 	    			// console.log(nowtime.getTime());
 	    			// console.log(endtime.getTime());
 	    			// var lefttime = endtime.getTime() - nowtime.getTime() //距离结束时间的毫秒数
 	    			
 					var leftd = Math.floor(nowtime/(1000*60*60*24))  //计算天数
 	    			var lefth = Math.floor(nowtime/(1000*60*60)%24)  //计算小时数
 	    			var leftm = Math.floor(nowtime/(1000*60)%60)  //计算分钟数
 	    			var lefts = Math.floor(nowtime/1000%60);  //计算秒数
					if(lefts < 10){
						lefts = "0"+lefts;
					}
					// console.log('-------------left的类型---------',lefts)
 	    		// return leftd + "天" + lefth + "时" + leftm + "分" + lefts + '秒'
 				return leftm+":"+lefts;  //返回倒计时的字符串
 }  
// 正则获取url参数
export let getParameter = (name)=>{
	// http://www.ruidione.com/?room=1&g=pk10&agent=8e5d35bf1414296e03c7846ebab7af6
	let reg =  new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	let r = window.location.search.substr(1).match(reg)
	 
	if(r != null){
		return r[2]
	}
	return null;
}
// 判断封盘时间差，从而确认时间差

// export  let timeDifference = (fengpan,currenttime)=>{
// 	       //          var nowtime = new Date(),  //获取当前时间
// 	    			// endtime = new Date("2020/8/8");  //定义结束时间
// 	    			// console.log(nowtime.getTime());
// 	    			// console.log(endtime.getTime());
// 	    			// var lefttime = endtime.getTime() - nowtime.getTime() //距离结束时间的毫秒数
// 	    			var isTrue = currenttime - fengpan
// 					if(isTrue ){
// 						// 使用封盘时间
// 						var leftm = Math.floor(fengpan/(1000*60)%60)  //计算分钟数
// 						var lefts = Math.floor(fengpan/1000%60);
// 						return (1000*60*leftm)+(lefts*1000);
// 					}else{
						
// 					}
// 				return (1000*60*leftm)+(lefts*1000);  //返回倒计时的字符串
// }

// export default {requestAjax,disposeDate}