var sendtime = 0;
var id = 1;
var chatTimer = ''
$(function () {
	FirstGetContent();
	$('.sendemaill').click(function () { // 重点是这里，从这里向服务器端发送数据
	    // console.log('-------发送信息-------')
		var msgtxt = $('#Message').val();
		send_msg(msgtxt);
	});
	// $('.rightdiv').on('scroll',function(){
		//    console.log('----------rightdiv滚动-----------')
	// })

	// getUserInfo();
	// setInterval(function(){ getUserInfo() }, 3000);
	setInterval(function(){ getUserInfo() }, 3000);
});

function send(){
	// console.log('------chat发送信息-----')
	var msgtxt = $('#Message').val();
	// console.log('-----------用户发送信息---------------',msgtxt)
	send_msg(msgtxt);
}
// user发送信息
function send_msg(msg){
	var msgtxt = msg;
	var str = "";
	var date = new Date().format("hh:mm:ss");
	var time = new Date().getTime();

	if (time - sendtime < 2000) { 
		// zy.tips('距离上次发送时间过短,请稍后再试!'); 
		vant.Toast('距离上次发送时间过短,请稍后再试!');
		return; 
	}
	if (msgtxt == "") {
		// zy.tips("不能发送空消息!");
		vant.Toast('不能发送空消息!');
	} else {
		$.ajax({
			url: '/Application/ajax_chat.php?type=send',
			type: 'post',
			async:false,
			data: { content: msgtxt },
			dataType: 'json',
			success: function (data) {
				if (data.success) {
					sendtime = new Date().getTime();
					str = '<div class="saidright">' +
						'<img src="' + info['headimg'] + '">' +
						'<div class="tousaidl">' +
						'<span class="tousaid2">' + info['nickname'] + '</span>&nbsp;&nbsp;' +
						'<span class="tousaid1">' + date + '</span>' +
						'</div>' +
						'<div class="ts">' +
						'<b></b>' +
						'<span class="neirongsaidl" style="">' + msgtxt + '</span>' +
						'</div>' +
						'</div>';
						// $('.rightdiv').prepend(str);
					$('.rightdiv').append(str);
					// console.log('-----rightdiv------------',$('.rightdiv'))
					$('.rightdiv')[0].lastElementChild.scrollIntoView()
					// $('.rightdiv').scrollTop = $('.rightdiv').scrollHeight;
					$('#Message').val('');
				} else {
					// zy.tips(data.msg);
					vant.Toast(data.msg);
				}
			},
			error: function () { }
		});
	}
}

function getUserInfo(){
	$.ajax({
		url:'/Application/ajax_getuserinfo.php',
		type: 'get',
		cache:false,
		async:false,
		dataType:'json',
		success:function(data){
			if(data.success){
				$('.balance').html(data.price);
				$('.online').html(data.online);
			}else{
				alert('登录过期,请重新登录！');
				//window.location.href="http://" + location.host + "/?room=" + info['roomid'];
				window.location.href="http://" + location.host + "/qr.php?room=" + info['roomid'];
			}
		},
		error:function(){}
	});
}

function FirstGetContent() {
	$.ajax({
		url: '/Application/ajax_chat.php?type=first',
		type: 'get',
		dataType: 'json',
		async:false,
		success: function (data) {
			addMessage(data);
			WelcomMsg(welcome, welHeadimg);
		},
		error: function () { },
		complete:function(){
			$('#right_page_loading')[0].remove();
		}
	});
	$('#messageLoading').remove();
	setInterval(updateContent, 2000);
}

function updateContent() {
	$.ajax({
		url: '/Application/ajax_chat.php?type=update&id=' + id,
		type: 'get',
		async:false,
		dataType: 'json',
		success: function (data) {
			// console.log('--------客服返回的数据----------',data)
			if(data.length ===0 ){
				// console.log('---客服没数据返回-----')
				// console.log('---客服没数据返回-----')
			}else{
				// console.log('=========客服有数据返回==========')
				// console.log('=========客服有数据返回==========')
				addMessage(data);
			}
			
		},
		error: function () { }
	});
}
// 机器人发送信息
function addMessage(data) {
	if (data == null || data.length < 0) {
		return;
	}
	//S1代理  S2待定  S3机器人  S4全局公告
	var str = "";
	for (i = 0; i < data.length; i++) {
		if (parseInt(data[i].id) > id) {
			id = data[i].id;
		}
		var type = data[i].type;
		if (type == 'S3') {  //白色
			str += '<div class="saidleft">' +
				'<img src="' + data[i].headimg + '">' +
				'<div class="tousaid">' +
				'<span class="tousaid1">' + data[i].nickname + '</span>&nbsp;&nbsp;' +
				'<span class="tousaid2">' + data[i].addtime + '</span>' +
				'</div>' +
				'<div class="tsf">' +
				'<b></b>' +
				'<span class="neirongsaid" style="">' + data[i].content + '</span>' +
				'</div>' +
				'</div>';
		// } else if (type == 'S3') {  //黄色
		} else if (type.substr(0, 1) == 'U') {  //黄色
			var headimg = data[i].headimg == "" ? "/Style/images/robot.png" : data[i].headimg;
			str += '<div class="saidright">' +
				'<img src="' + headimg + '">' +
				'<div class="tousaidl">' +
				'<span class="tousaid2">' + data[i].addtime + '</span>&nbsp;&nbsp;' +
				'<span class="tousaid1">' + data[i].nickname + '</span>' +
				'</div>' +
				'<div class="ts">' +
				'<b></b>' +
				'<span class="neirongsaidl" style="">' + data[i].content + '</span>' +
				'</div>' +
				'</div>';
		} else if (type == 'S1') {  //绿色
			var headimg = data[i].headimg == "" ? "/Style/images/Sys.png" : data[i].headimg;
			str += '<div class="saidleft">' +
				'<img src="' + headimg + '">' +
				'<div class="tousaid">' +
				'<span class="tousaid1">' + data[i].addtime + '</span>&nbsp;&nbsp;' +
				'<span class="tousaid2">' + data[i].nickname + '</span>' +
				'</div>' +
				'<div class="tsf tsf1">' +
				'<b style="border-color:transparent #98E165 transparent transparent;"></b>' +
				'<span class="neirongsaid" style="background-color:#98E165;">' + data[i].content + '</span>' +
				'</div>' +
				'</div>';
		}
	}
	
	
	// console.log('scrollHeight--------------',$(this).children("rightdiv:last-child").val()
	$('.rightdiv').append(str);
	// console.log('-----rightdiv------------',$('.rightdiv')[0])
	$('.rightdiv')[0].lastElementChild.scrollIntoView()
	// $('.rightdiv').prepend(str);
	// $('.rightdiv').ready(function(){
		// console.log('渲染完毕',$('#fooder_tab_id'))
	// })
	// console.log('scrollHeight--------------',$('.rightdiv')[0].childNodes.lastElementChild)
}
// 进来时机器人提示语
function WelcomMsg(data, welHeadimg) {
	if (data == null || data.length < 0) {
		return;
	}
	var str = "";
	if (welHeadimg == '') {
		welHeadimg = "/Style/images/Sys.png";
	}
	for (i = 0; i < data.length; i++) {
		sendtime = new Date().format("hh:mm:ss");
		str += '<div class="saidleft">' +
			'<img src="' + welHeadimg + '">' +
			'<div class="tousaid">' +
			'<span class="tousaid1">' + sendtime + '</span>&nbsp;&nbsp;' +
			'<span class="tousaid2">管理员</span>' +
			'</div>' +
			'<div class="tsf tsf1">' +
			'<b style="border-color:transparent #98e165 transparent transparent;"></b>' +
			'<span class="neirongsaid" style="background-color:#98E165;">' + data[i] + '</span>' +
			'</div>' +
			'</div>';
	}
	$('.rightdiv').append(str);
	$('.rightdiv')[0].lastElementChild.scrollIntoView()
	// $('.rightdiv').scrollTop = $('#fooder_tab_id').scrollHeight;
}

Date.prototype.format = function (format) {
	var o = {
		"M+": this.getMonth() + 1, //month
		"d+": this.getDate(),    //day
		"h+": this.getHours(),   //hour
		"m+": this.getMinutes(), //minute
		"s+": this.getSeconds(), //second
		"q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
		"S": this.getMilliseconds() //millisecond
	}
	if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
		(this.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (var k in o) if (new RegExp("(" + k + ")").test(format))
		format = format.replace(RegExp.$1,
			RegExp.$1.length == 1 ? o[k] :
				("00" + o[k]).substr(("" + o[k]).length));
	return format;
}
$(document).ready(
function(){
	// setInterval(function(){
	// 	// console.log('渲染完毕----------------:',$('.rightdiv')[0].lastElementChild)
	// 	console.log('渲染完毕:',$('.rightdiv')[0].lastElementChild.scrollHeight)
		// $('.rightdiv').scrollTop =$('.rightdiv')[0].lastElementChild.scrollHeight 
	// 	// console.log('渲染结果',$('.rightdiv').scrollTop)
	// },10)
	// console.log('rightdiv-----------',	$('.rightdiv'))
	// 实现方法一  轮询查到最后一个
	// setInterval(function(){
		// console.log($('.rightdiv')[0].lastElementChild)
		// $('.rightdiv')[0].lastElementChild.scrollIntoView()
		// $('.rightdiv').scrollTop($('.rightdiv')[0].lastElementChild.offsetHeight)+" px" 
		// $('.rightdiv').animate({ scrollTop: "+=200" }, 10);
		// $(".rightdiv").scrollTop(50)+" px"
	// },2000)
	
	
})
// $(document).ready(
   // $('.rightdiv').on('scroll',function(){
	  //  console.log('----------rightdiv滚动-----------')
   // })
// )