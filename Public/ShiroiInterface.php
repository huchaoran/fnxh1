<?php
/**
 * @author shiroi
 * @e-mail 707305003@qq.com
 * @site https://shiroi.top
 * @create 2020/7/22
 */
include './ShiroiTrait.php';

class ShiroiInterface
{
    use ShiroiTrait;

    //引入公共

    /**
     * 上分下分管理接口
     * @method POST|GET
     * @param datetime time (时间类型 day|week|year)
     * @param int userid (用户id)
     * @param int type (上下分类型 上分/下分)
     * @param int page (页数)
     * @param int limit (条数)
     * @return mixed
     */
    public function upAndDown()
    {
        //条件拼接
        if ($this->getTimeType != 'all') {
            $this->whereBetweenTime = "time < '{$this->today}'";
            $this->whereBetweenTime .= " AND time >= '{$this->endTime}'";
        }
        $where['userid'] = $this->request['userid'];
        if (isset($this->request['game']) && !empty($this->request['type'])) $where['game'] = $this->request['game'];
        if (isset($this->request['type']) && !empty($this->request['type'])) $where['type'] = $this->request['type'];
        //数据查询
        $data = $this->shiroiDb
            ->table('fn_upmark')
            ->where($where)
            ->where($this->whereBetweenTime)
            ->order('id desc');
        $sql = $data->sql;
        $data = $data->limit("{$this->startLimit},{$this->limit}")->select();
        $count = $this->shiroiDb->query(str_replace('*', 'count(*) as count', $sql));
        return ['data' => $data, 'count' => reset($count)['count']];
    }

    /**
     * 获取游戏最新一期的信息
     * @method POST|GET
     * @param int type (游戏类型)  'pk10'=>1 'xyft'=>2 'cqssc'=>3 'xy28'=>4
     * 'jnd28'=>5 'jsmt'=>6 'jssc'=>7 'jsssc'=>8 'kuai3'=>9
     * 'bjl'=>10 'gd11x5'=>11 'jssm'=>12 'lhc'=>13 'jslhc'=>14
     * @return mixed
     */
    public function gameMsg()
    {
        $game_type = isset($this->request['type']) && !empty($this->request['type']) ? $this->request['type'] : 1;
        $getGame= array_search($game_type,$this->game);
        $data = $this->shiroiDb
            ->table('fn_open')
            ->where(['type' => $game_type])
            ->order('term desc')
            ->get();
        $getLottery = (new ShiroiDb())->table($this->fengpan[$getGame])->where(['roomid' => $this->request['roomid']])->get();
        $data = array_merge(self::handleGame($data), [
            'fengpan' => $getLottery['fengtime'],
            'now_time' => time(),
            'gameopen' => $getLottery['gameopen']
        ]);
        $data['distance_fengpan_time'] = intval($data['next_time_str'] - $data['fengpan']);
        return $data;
    }

    /**
     * 获取用户积分/总盈亏/总流水
     * @param string userid (用户id)
     * @param string roomid (房间id)
     * @return array
     */
    public function getMoneyInfo()
    {
        $usermoney = array();
        $zongliushui = array();
        $zongyinkui = array();
        $type = $this->request['type']?$this->request['type']:'';

        if($type=='usermoney') return (new ShiroiDb())->table('fn_user')->where(['userid'=>$this->request['userid']])->value('money');
        //总盈亏
        foreach ($this->table as $k=>$v) {
            $zongliushui[] = (new ShiroiDb())->table($v)
                ->field("sum(`money`) as money",false)
                ->where(['userid'=>$this->request['userid'],'roomid'=>$this->request['roomid']])
                ->where("(`status` > 0 or `status` < 0)")->sql;
        }
        $zongliushui = (new ShiroiDb())->query(implode(' union all ', $zongliushui));
        $zongliushui = array_sum(array_column($zongliushui,'money'));
        //总流水
        foreach ($this->table as $k=>$v) {
            $zongyinkui[] = (new ShiroiDb())->table($v)
                ->field("sum(`status`) as money",false)
                ->where(['userid'=>$this->request['userid'],'roomid'=>$this->request['roomid']])
                ->where('`status` > 0')->sql;
        }
        $zongyinkui = (new ShiroiDb())->query(implode(' union all ', $zongyinkui));
        $zongyinkui = array_sum(array_column($zongyinkui,'money'));
        $zongyinkui = $zongyinkui - $zongliushui;
        if($type){
            return $$type;
        }else{
            return [
                'usermoney' => (new ShiroiDb())->table('fn_user')->where(['userid'=>$this->request['userid']])->value('money'),
                'zongyinkui' => $zongyinkui,
                'zongliushui' => $zongliushui
            ];
        }
    }

    /**
     * 获取当天该游戏的所有信息
     * @method POST|GET
     * @param int type (游戏类型)
     * @return mixed
     */
    public function gameMsgList()
    {
        $game_type = isset($this->request['type']) && !empty($this->request['type']) ? $this->request['type'] : 1;
        //条件拼接
//        $whereStr = "time BETWEEN '{$this->todayStart}' AND '{$this->todayEnd}'";
        $data = $this->shiroiDb
            ->table('fn_open')
            ->where(['type' => $game_type])
//            ->where($whereStr)
            ->order('term desc');
        $sql = $data->sql;
        $data = $data->limit("{$this->startLimit},{$this->limit}")->select();
        $count = $this->shiroiDb->query(str_replace('*', 'count(*) as count', $sql));
        foreach ($data as $k => $v) $data[$k] = self::handleGame($v);
        return ['data' => $data, 'count' => reset($count)['count']];
    }

    /**
     * 投注记录查询
     * @method POST|GET
     * @param datetime time (时间类型 day|week|year)
     * @param int userid (用户id)
     * @param int page (页数)
     * @param int limit (条数)
     * @return mixed
     */
    public function bettingRecord()
    {
        //获取参数
        $game = isset($this->request['game']) ? $this->request['game'] : '';
        //条件拼接
        if ($this->getTimeType != 'all') {
            $this->whereBetweenTime = "addtime < '{$this->today}'";
            $this->whereBetweenTime .= " AND addtime >= '{$this->endTime}'";
        }
        $data = self::getAllOrder($game, "$this->whereBetweenTime and userid='{$this->request['userid']}'");
        $count = self::getAllOrder($game, "$this->whereBetweenTime and userid='{$this->request['userid']}'", '*', 'count');
        foreach ($data as $k => $v) {
            $son = (new ShiroiDb())->table('fn_open')->where(['term' => $v['term']], 1)->get();
            $data[$k]['term_arr'] = self::handleGame($son);
        }
        return ['data' => $data, 'count' => reset($count)['count']];
    }

    /**
     * 投注记录查询
     * @method POST|GET
     * @param string type (返回类型 list|count)
     * @param int userid (用户id)
     * @param int roomid (房间号)
     * @param string game (游戏名)
     * @param int page (页数)
     * @param int limit (条数)
     * @return mixed
     */
    public function profit()
    {
        $type = isset($this->request['type']) && !empty($this->request['type']) ? $this->request['type'] : 'list';//返回的数据类型列表形式或者是统计形式
        $where = [
            'userid' => $this->request['userid'],
            'roomid' => $this->request['roomid']
        ];
        if ($type == 'count') {
            $dataInfo = self::getAllOrder($this->request['game'],
                $where,
                "term,addtime,`status`,id,SIGN(`status`) AS order_status",
                'select',
                'order_status in (1,-1)'
            );
        } else {
            $dataInfo = self::getAllOrder($this->request['game'], $where);
            $count = self::getAllOrder($this->request['game'], $where, '*', 'count');
        }
        if ($type == 'count') $dataInfo = array_sum(array_column($dataInfo, 'status'));
        //按照期数计算
        if ($type == 'list') {
            $arr = [];
            foreach ($dataInfo as $k => $v) {
//                if($v['status'] == '已撤单') $v['status'] = 0;
                $arr[$v['term']][] = $v;
            }
            $dataInfo = ['data' => $arr, 'count' => reset($count)['count']];
        }
        return $dataInfo;
    }

    public function profit1()
    {
        $type = isset($this->request['type']) && !empty($this->request['type']) ? $this->request['type'] : 'list';//返回的数据类型列表形式或者是统计形式
        $where = [
            'userid' => $this->request['userid'],
            'roomid' => $this->request['roomid']
        ];
        if ($type == 'count') {
            $dataInfo = self::getAllOrder($this->request['game'],
                $where,
                "term,addtime,`status`,id,SIGN(`status`) AS order_status",
                'select',
                'order_status in (1,-1)'
            );
        } else {
            $dataInfo = self::getAllOrder($this->request['game'], $where);
            $count = self::getAllOrder($this->request['game'], $where, '*', 'count');
        }
        if ($type == 'count') $dataInfo = array_sum(array_column($dataInfo, 'status'));
        //按照期数计算
        if ($type == 'list') {
            $dataInfo = ['data' => $dataInfo, 'count' => reset($count)['count']];
        }
        return $dataInfo;
    }

    /**
     * 长龙数据处理
     * @method POST|GET
     * @param int type (游戏类型)
     * @return array
     */
    public function longDragon()
    {
        //获取前20期数据
        $game_type = isset($this->request['type']) && !empty($this->request['type']) ? $this->request['type'] : 1;
        $data = $this->shiroiDb
            ->field('code')
            ->table('fn_open')
            ->where(['type' => $game_type])
            ->order('term desc')
            ->limit(20)
            ->select();
        foreach ($data as $k => $v) $data[$k]['arr_code'] = explode(',', $v['code']);
        $term = array_column($data, 'arr_code');
        $minci = ['冠军' => [], '亚军' => [], '第三名' => [], '第四名' => [], '第五名' => [], '第六名' => [], '第七名' => [], '第八名' => [], '第九名' => [], '第十名' => []];
        $mincinum = ['冠军' => 0, '亚军' => 1, '第三名' => 2, '第四名' => 3, '第五名' => 4, '第六名' => 5, '第七名' => 6, '第八名' => 7, '第九名' => 8, '第十名' => 9];
        $xiao_arr = [1, 2, 3, 4, 5];//小区间
        $da_arr = [6, 7, 8, 9, 10];//大区间
        $dan = '单';
        $shuang = '双';
        $da = '大';
        $xiao = '小';
        $long = '龙';
        $hu = '虎';
        $longhu_arr = [];
        $arr = [];//存放值
        //数据处理
        for ($i = 0; $i < count($term); $i++) {
            $init = 0;
            foreach ($minci as $k => $v) {
                $minci[$k][] = $term[$i][$init];
                $init++;
            }
        }
        //判断单双操作
        foreach ($minci as $k => $v) {
            $jishu = 0;//基数出现的次数不中断
            $oushu = 0;//偶数出现的次数不中断
            for ($i = 0; $i < count($v); $i++) {
                if ($v[$i] % 2 == 0) {//偶数区间
                    if ($jishu > 0) break;
                    $oushu++;
                } else {//基数区间
                    if ($oushu > 0) break;
                    $jishu++;
                }
            }
            if ($jishu > 2) $arr[] = [$k . $dan, $jishu];
            if ($oushu > 2) $arr[] = [$k . $shuang, $oushu];
        }
        //判断大小操作
        foreach ($minci as $k => $v) {
            $xiao_init = 0;//小出现的次数不中断
            $da_init = 0;//大出现的次数不中断
            for ($i = 0; $i < count($v); $i++) {
                if (in_array($v[$i], $xiao_arr)) {//小区间
                    if ($da_init > 0) break;
                    $xiao_init++;
                }
                if (in_array($v[$i], $da_arr)) {//大区间
                    if ($xiao_init > 0) break;
                    $da_init++;
                }
            }
            if ($xiao_init > 2) $arr[] = [$k . $xiao, $xiao_init];
            if ($da_init > 2) $arr[] = [$k . $da, $da_init];
        }
        //判断龙虎操作
        foreach ($term as $k => $v) {
            for ($i = 0; $i < count($v) / 2; $i++) $longhu_arr[$k][$i] = $v[$i] > $v[count($v) - ($i + 1)] ? $long : $hu;
        }
        //纵向切割
        $tmp = [];
        foreach ($longhu_arr as $k => $v) {
            for ($i = 0; $i < count($v); $i++) {
                $key = array_search($i, $mincinum);
                $tmp[$key] .= $v[$i];
            }
        }
        //横向切割
        foreach ($tmp as $k => $v) {
            $long_init = 0;
            $hu_init = 0;
            foreach (mb_str_split($v) as $ks => $vs) {
                if ($vs == $long) {
                    if ($hu_init > 0) break;
                    $long_init++;
                } else {
                    if ($long_init > 0) break;
                    $hu_init++;
                }
            }
            if ($long_init > 2) $arr[] = [$k . $long, $long_init];
            if ($hu_init > 2) $arr[] = [$k . $hu, $hu_init];
        }
        return $arr;
    }

    /**
     * 预测 (内部实现)
     * @method POST|GET
     * @param int type
     * @return mixed|string[]
     */
    public function prediction()
    {
        $getNewData = self::gameMsg();//最新一期
        $nextTime = $getNewData['next_time_str'];//获取下一个时间段的区间范围
        $arr = ['冠军', '亚军', '第三名', '第四名', '第五名', '第六名', '第七名', '第八名', '第九名', '第十名'];
        $last_arr = '冠亚和';
        if($this->request['game'] == 'cqssc') {
            $arr = ['万位','千位','百位','十位','个位'];
            $last_arr = '龙虎和';
        }
        $num = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];//定义号码范围
        $num1 = $num;
        $both = ['单', '双'];
        $having = ['大', '小'];
        $dragon = ['虎', '龙'];
        $path = "{$_SERVER['DOCUMENT_ROOT']}/prediction_file/";
        if (!is_dir($path)) mkdir($path);
        $filepath = "{$path}{$nextTime}.txt";
        shuffle($num1);
        if (!file_exists($filepath)) {
            foreach ($arr as $k => $v) {
                shuffle($num1);
                $arr[$k] = [
                    $v,
                    implode(',', array_slice($num1, 0, 5)),
                    self::xunhuan($num[array_rand($num, 1)], $both),
                    self::xunhuan($num[array_rand($num, 1)], $having)
                ];
            }
            $arr1 = [$last_arr];
            $arr2 = [
                implode(',', array_slice($num1, 0, 5)),
                self::xunhuan($num[array_rand($num, 1)], $both),
                self::xunhuan($num[array_rand($num, 1)], $having)
            ];
            if($this->request['game'] == 'cqssc') {
                $arr2 = [
                    self::xunhuan($num[array_rand($num, 1)], $dragon)
                ];
            }
            array_push($arr, array_merge($arr1,$arr2));
//            }
            file_put_contents($filepath, json_encode($arr, true));
            return $arr;
        } else {
            return json_decode(file_get_contents($filepath));
        }
    }

    /**
     * 预测（采集数据）
     * @method POST|GET
     * @param int term (期数)
     * @return mixed
     */
    public function requestHttp()
    {
        $term = isset($this->request['term']) && !empty($this->request['term']) ? '.*?' . $this->request['term'] . '.*?' : '.*?';//期数
        $game = isset($this->request['game']) && !empty($this->request['game']) ? $this->request['game'] : 'pk10';
        $getUrlData = file_get_contents("https://m.1396r.com/{$game}/betgame");//采集的网站
        preg_match('/<section class=\"app_index_Scope.*?<\/section>/is', $getUrlData, $getall);//采集父层标签
        preg_match('/<div class=\"rp_reference\">' . $term . '(<table>.*?<\/table>).*?<\/div>/is', $getall[0], $getson);//获取指定期数的标签
//        preg_match_all('/<div class=\"rp_reference\">.*?748059期.*?(<table>.*?<\/table>).*?<\/div>/is',$getall[0],$getson);//所有子标签
        return self::tdToArray($getson[1]);//最新一期
    }

    /**
     * 文件上传类
     * @method POST
     * @param int userid (用户id)
     * @param string path (存放的文件夹->项目下)
     * @return string
     */
    public function filePost()
    {
        $userid = $this->request['userid'];//userid
        $file_name = isset($this->request['path']) && !empty($this->request['path']) ? '/' . $this->request['path'] : '/user_file';//文件夹名称
        $path = $_SERVER['DOCUMENT_ROOT'] . $file_name;//文件存放路径
        if (!is_dir($path)) mkdir($path);//文件夹不存在则新建
        if ($userid) {
            foreach ($this->file as $k => $v) {
                $suf = '.' . explode('.', $_FILES["file"]["name"])[1];
                $upload_file = $path . "/" . "{$userid}{$suf}";
                move_uploaded_file($_FILES["file"]["tmp_name"], $upload_file);
                if (file_exists($upload_file)) return $file_name . '/' . $userid . $suf;
            }
        }
        return 'fail';
    }

    /**
     * 消息管理
     * @method POST|GET
     * @param string userid (用户id)
     * @param string type (已读|未读)
     * @param string user_group (S1|U2)
     * @return mixed
     */
    public function custom()
    {
        //类型 已读/未读
        $type = isset($this->request['type']) && !empty($this->request['type']) ? $this->request['type'] : '';
        //用户群体
        $user_group = isset($this->request['user_group']) ? $this->request['user_group'] : '';
        $table = $this->shiroiDb->table('fn_custom')->where(['userid' => $this->request['userid']]);
        if ($type) $table = $table->where(['status' => $type]);
        if ($user_group) $table = $table->where(['type' => $user_group]);
        return $table
            ->order('addtime desc')
            ->limit("{$this->startLimit},{$this->limit}")
            ->select();
    }

    /**
     * 修改客户发送信息，设置为已读或未读
     * @param string type (已读|未读)
     * @param string userid (用户id)
     * @param int id (消息id)
     * @return mixed
     */
    public function editCustomMsg()
    {
        $type = isset($this->request['type']) && !empty($this->request['type']) ? $this->request['type'] : '已读';
        return $this->shiroiDb->table('fn_custom', 'update')
            ->where(['userid' => $this->request['userid'], 'id' => $this->request['id']])
            ->update(['status' => $type]);
    }

    /**
     * 用户信息
     * @method POST|GET
     * @param string userid (用户id)
     * @return mixed
     */
    public function userInfo()
    {
        return $this->shiroiDb
            ->table('fn_user')
            ->where(['userid' => $this->request['userid']])
            ->get();
    }

    /**
     * 修改用户信息
     * @method POST|GET
     * @param string userid (用户id)
     * @param string username (用户名)
     * @param string phone (手机)
     * @param string oldpass (旧密码)
     * @param string newpass (新密码)
     * @param string headimg (头像)
     * @return string
     */
    public function upDateUserInfo()
    {
        $request = $this->request;//GET/POST所有参数
        $update = [];
        if ($request['userid']) {
            $table = $this->shiroiDb->table('fn_user')->where(['userid' => $request['userid']]);
            $sql = $table->sql;
            $userinfo = $table->get();
            $userid = $this->request['userid'];
            if ($userinfo) {
                if ($request['oldpass']) {
                    if ($request['oldpass'] == $userinfo['pass']) {
                        if ($request['newpass']) $request['pass'] = $request['newpass'];//密码
                    } else {
                        to_json([], $this->failCode[201], 201);
                        die;
                    }
                }
                unset($request['f'], $request['userid'], $request['oldpass'], $request['newpass']);
                $this->shiroiDb
                    ->table('fn_user', 'update')
                    ->where(['userid' => $userid], 1)
                    ->update($request);
                $userinfo = $this->shiroiDb->query($sql);
                return reset($userinfo);
            }
        }
        return 'fail';

    }

    /**
     * 收款方式
     * @method POST|GET
     * @param string userid (用户id)
     * @param string wechat (微信收款码)
     * @param string alipay (支付宝收款码)
     * @param string card_username (银行卡持卡人姓名)
     * @param string card_number (银行卡号码)
     * @param string card_type (银行卡类型)
     * @param string card_bank_address (开户行地址)
     * @return mixed
     */
    public function payInfo()
    {
        unset($this->request['f']);//删除回调方法
        $userinfo = $this->shiroiDb->table('fn_pay')->where(['userid' => $this->request['userid']])->get();//用户信息
        $userid = $this->request['userid'];//用户id
        if ($userinfo) {//更新
            unset($this->request['userid']);//删除userid
            $this->shiroiDb->table('fn_pay', 'update')->where(['userid' => $userid], 1)->update($this->request);
        } else {//新增
            $this->shiroiDb->table('fn_pay', 'insert')->insert($this->request);
        }
        return $this->shiroiDb->table('fn_pay')->where(['userid' => $userid], 1)->get();
    }

    /**
     * 获取房间
     * @method POST|GET
     * @param string username
     * @return array|mixed
     */
    public function getUserRoom()
    {
        $roomadmin = $this->request['username'];
        $array = [];
        if ($roomadmin) {
            $array = $this->shiroiDb->table('fn_room')->where(['roomadmin' => $roomadmin])->select();
        }
        return $array;
    }

    /**
     * 获取房间的所有用户
     * @param int roomid (房间号)
     * @param int limit (条数)
     * @param int page (页数)
     * @return mixed
     */
    public function getRoomUser()
    {
        $roomid = $this->request['roomid'];
        $data = $this->shiroiDb
            ->table('fn_user')
            ->field('id,username,headimg,money,roomscore,roomid')
            ->where(['roomid' => $roomid])
            ->limit("{$this->startLimit},{$this->limit}")
            ->select();
        $count = $this->shiroiDb->table('fn_user')->where(['roomid' => $roomid], 1)->count();
        return ['count' => $count, 'data' => $data];
    }

    /**
     * 查看用户账变记录
     * @param string startTime (开始区间)
     * @param string endTime (结束区间)
     * @param string userid (用户id)
     * @return mixed
     */
    public function getAccountChange()
    {
//        dd($this->request);
        $query = (new ShiroiDb())->table('fn_account_change');
        $count = (new ShiroiDb())->table('fn_account_change');
        if ($this->request['startTime'] && $this->request['endTime']) {
            $startTime = "{$this->request['startTime']} 00:00:00";
            $endTime = "{$this->request['endTime']} 23:59:59";
            $where = "addtime > '{$startTime}'";
            $where .= " AND addtime <= '{$endTime}'";
            $query = $query->where($where, 1);
            $count = $count->where($where, 1);
        }
        $where = [];
        if($this->request['game']) $where['game'] = $this->request['game'];
        $where['userid'] = $this->request['userid'];
        return [
            'count'=>$count->where($where)->count(),
            'data'=>$query->where($where)->order('addtime desc')->limit("{$this->startLimit},{$this->limit}")->select(),
        ];
    }

    /**
     * 竞猜报表
     * @param string game (游戏名)
     * @param string startTime (开始时间 格式:2020-08-10)
     * @param string endTime (结束时间 格式:2020-08-11)
     * @param string userid (用户id)
     * @param string roomid (房间id)
     * @return array
     */
    public function getReport()
    {
        $game = $this->request['game'] ?: '';
        $all_order = [];
        if ($game) {
            $table = $this->table[$game] ? [$game => $this->table[$game]] : '';
        } else {
            $table = $this->table;
        }
        $upAndDownArray = self::getUpDown();//获取用户上下分金额
        foreach ($table as $k => $v) {
            //流水区间(取money)
            $type = ['money', 'status'];
            $liushuiData = [];
            for ($i = 0; $i < count($type); $i++) {
                $liushui = (new ShiroiDb())->field("sum(`{$type[$i]}`) as {$type[$i]}", false)->table($v)->where([
                    'roomid' => $this->request['roomid'],
                    'userid' => $this->request['userid']
                ]);
                if ($type[$i] == 'money') {
                    $liushui = $liushui->where("(status > 0 or status < 0)");
                } else {
                    $liushui = $liushui->where("status > 0");
                }
                if (isset($this->request['startTime']) && isset($this->request['endTime'])) {
                    $where = "(`addtime` between '{$this->request['startTime']}' and '{$this->request['endTime']}')";
                    $liushui = $liushui->where($where);
                }
                $getInfo = $this->shiroiDb->query($liushui->sql);
                $liushuiData[$type[$i]] = round(reset($getInfo)[$type[$i]], 2);
                $all_order[$k] = $liushuiData;
            }
            $all_order[$k]['yk'] = (float)bcdiv($all_order[$k]['status'],$all_order[$k]['money'],2);//每个游戏的盈亏
            //获取游戏总结果
            $all_order[$k]['gameresult'] = self::getGameResult($k)[$k];
            //获取玩家的总结果数
            $all_order[$k]['playerresult'] = self::getPlayerResult($k)[$k];
            //获取玩家注单
            $all_order[$k]['orderinfo'] = self::getOrderInfo($k)[$k];
            //获取返点合计
            $all_order[$k]['backwater'] = self::getBackWater($k)[$k];

        }
        $upAndDownArray['userbane'] = (new ShiroiDb())->table('fn_user')->where(['userid' => $this->request['userid']])->value('username');
        $upAndDownArray['time'] = str_replace('-', '/', date('Y-m-d H:i',strtotime($this->request['startTime']))) . ' - ' . str_replace('-', '/', date('Y-m-d H:i',strtotime($this->request['endTime'])));
        $upAndDownArray['注额'] = round(array_sum(array_column($all_order, 'money')), 2);//流水统计
        $upAndDownArray['盈亏'] = round(array_sum(array_column($all_order, 'yk')), 2);//盈亏统计
        $upAndDownArray['游戏总结果'] = array_sum(array_column($all_order, 'gameresult'));//游戏总结果统计
        $upAndDownArray['玩家总结果'] = array_sum(array_column($all_order, 'playerresult'));//玩家总结果统计
        $upAndDownArray['总注单'] = array_sum(array_column($all_order, 'orderinfo'));//总注单统计
        $upAndDownArray['返点合计'] = array_sum(array_column($all_order, 'backwater'));//返点合计统计
        $upAndDownArray['game'] = $all_order;
        return $upAndDownArray;
    }

    /**
     * 获取返点合计(反水|退水)
     * @param string $game (游戏名)
     * @param string $type (类型)
     * @param string startTime (开始时间 格式:2020-08-10)
     * @param string endTime (结束时间 格式:2020-08-11)
     * @param string userid (用户id)
     * @param string roomid (房间id)
     * @return array
     */
    public function getBackWater($game = '', $type = '系统退水')
    {
        $game = $game ? $game : $this->request['game'];
        if ($game) {
            $game = $this->game[$game] ? [$game => $this->game[$game]] : '';
        } else {
            $game = $this->game;
        }
        $gameData = [];
        foreach ($game as $k => $v) {
            $getBackWater = (new ShiroiDb())->field("sum(money)", false)->table('fn_marklog')->where([
                'userid' => $this->request['userid'],
                'roomid' => $this->request['roomid'],
                'content' => $type,
                'game' => $k
            ]);
            if (isset($this->request['startTime']) && isset($this->request['endTime'])) {
                $where = "(`addtime` between '{$this->request['startTime']}' and '{$this->request['endTime']}')";
                $getBackWater = $getBackWater->where($where);
            }
            $getBackWater = $getBackWater->get();
            $gameData[$k] = (int)reset($getBackWater);//单个游戏的返点
        }
        return $gameData;
    }

    /**
     * 获取改用户的上下分金额数
     * @param string roomid (房间号)
     * @param string userid (用户id)
     * @param string startTime (开始时间 格式:2020-08-10)
     * @param string endTime (结束时间 格式:2020-08-11)
     * @return array
     */
    public function getUpDown()
    {
        $upAndDown = ['上分', '下分'];//查询上下分已处理
        $upAndDownArray = [];
        foreach ($upAndDown as $v) {
            $sf = (new ShiroiDb())->field("sum(`money`) as money", false)->table('fn_upmark')->where([
                'roomid' => $this->request['roomid'],
                'userid' => $this->request['userid'],
                'type' => $v,
                'status' => '已处理'
            ]);//条件查询拼接
            if (isset($this->request['startTime']) && isset($this->request['endTime'])) {
                $where = "(`time` between '{$this->request['startTime']}' and '{$this->request['endTime']}')";
                $sf = $sf->where($where);
            }
            $getInfo = $this->shiroiDb->query($sf->sql);
            $upAndDownArray[$v] = round(reset($getInfo)['money'], 2);//生成sql语句
        }
        return $upAndDownArray;
    }

    /**
     * 注单(用户下注了多少单)
     * @param string userid (用户id)
     * @param string roomid (房间id)
     * @param string startTime (开始时间 格式:2020-08-10)
     * @param string endTime (结束时间 格式:2020-08-11)
     * @param string game (游戏名)
     * @return array
     */
    public function getOrderInfo($game = '')
    {
        $game = $game ? $game : $this->request['game'];
        $gameData = [];
        if ($game) {
            $table = $this->table[$game] ? [$game => $this->table[$game]] : '';
        } else {
            $table = $this->table;
        }
        foreach ($table as $k => $v) {
            $playerResult = (new ShiroiDb())->table($v)
                ->field("count(*)", false)
                ->where([
                    'userid' => $this->request['userid'],
                    'roomid' => $this->request['roomid']
                ]);
            if (isset($this->request['startTime']) && isset($this->request['endTime'])) {
                $where = "(`addtime` between '{$this->request['startTime']}' and '{$this->request['endTime']}')";
                $playerResult = $playerResult->where($where);
            }
            $playerResult = $playerResult->get();
            $gameData[$k] = reset($playerResult);
        }
        return $gameData;
    }

    /**
     * 玩家结果数(玩家投注的次数)(一期下多注结果为一)
     * @param string userid (用户id)
     * @param string game (游戏名)
     * @param string startTime (开始时间 格式:2020-08-10)
     * @param string endTime (结束时间 格式:2020-08-11)
     * @return array
     */
    public function getPlayerResult($game = '')
    {
        $game = $game ? $game : $this->request['game'];
        $gameData = [];
        if ($game) {
            $table = $this->table[$game] ? [$game => $this->table[$game]] : '';
        } else {
            $table = $this->table;
        }
        foreach ($table as $k => $v) {
            $sonSql = (new ShiroiDb())->table($v)
                ->field("count(*)", false)
                ->where([
                    'userid' => $this->request['userid'],
                    'roomid' => $this->request['roomid']
                ]);
            if (isset($this->request['startTime']) && isset($this->request['endTime'])) {
                $where = "(`addtime` between '{$this->request['startTime']}' and '{$this->request['endTime']}')";
                $sonSql = $sonSql->where($where);
            }
            $sonSql = $sonSql->group('term')->sql;
            $playerResult = (new ShiroiDb())->field("count(*)", false)->table("({$sonSql}) as t")->get();
            $gameData[$k] = reset($playerResult);
        }
        return $gameData;
    }

    /**
     * 游戏结果数(游戏的总场次)
     * @param string $game
     * @param string startTime (开始时间 格式:2020-08-10)
     * @param string endTime (结束时间 格式:2020-08-11)
     * @return array
     */
    public function getGameResult($game = '')
    {
        $game = $game ? $game : $this->request['game'];
        if ($game) {
            $game = $this->game[$game] ? [$game => $this->game[$game]] : '';
        } else {
            $game = $this->game;
        }
        $gameData = [];
        foreach ($game as $k => $v) {
            $gameResult = (new ShiroiDb())->table('fn_open')
                ->field("count(*) as count", false)
                ->where(['type' => $v]);
            if (isset($this->request['startTime']) && isset($this->request['endTime'])) {
                $where = "(`time` between '{$this->request['startTime']}' and '{$this->request['endTime']}')";
                $gameResult = $gameResult->where($where);
            }
            $gameResult = $gameResult->get();
            $gameData[$k] = reset($gameResult);//单个游戏的总结果
        }
        return $gameData;
    }

    /**
     * 根据游戏获取订单数据
     * @param string $game
     * @param string $where
     * @param string $field
     * @param string $type (查询类型 select count)
     * @param string $having
     * @return mixed
     */
    private function getAllOrder($game = '', $where = '', $field = '*', $type = 'select', $having = '')
    {
        $all_order = [];
        if ($game) {
            $table = ['game' => $this->table[$game]];
        } else {
            $table = $this->table;
        }
        foreach ($table as $v) {
            $sql = (new ShiroiDb())->field($field, false)->table($v)->where($where);
            if($this->request['startTime'] && $this->request['endTime']) {
                $whereTime = "(`addtime` between '{$this->request['startTime']}' and '{$this->request['endTime']}')";
                $sql = $sql->where($whereTime);
            }
            if ($having) $sql = $sql->having($having);
            $all_order[] = $sql->sql;
        }
        $order_sql = implode(' union all ', $all_order);
        if ($type == 'select') {
            $sql = "{$order_sql} order by addtime desc limit {$this->startLimit},{$this->limit}";
        } else {
            $sql = (new ShiroiDb())->field("count(*) as count", false)->table("($order_sql)")->sql;
            $sql .= ' as count';
        }
        return (new ShiroiDb())->query($sql);
    }

    /**
     * html table转换成数组
     * @param $table
     * @return mixed
     */
    private function tdToArray($table)
    {
        set_time_limit(0);
        $table = preg_replace("'<table[^>]*?>'si", "", $table);
        $table = preg_replace("'<tr[^>]*?>'si", "", $table);
        $table = preg_replace("'<td[^>]*?>'si", "", $table);
        $table = str_replace("</tr>", "{tr}", $table);
        $table = str_replace("</td>", "{td}", $table);
        //去掉 HTML 标记
        $table = preg_replace("'<[/!]*?[^<>]*?>'si", "", $table);
        //去掉空白字符
        $table = preg_replace("'([rn])[s]+'", "", $table);
        $table = str_replace(" ", "", $table);
        $table = str_replace(" ", "", $table);
        $table = explode('{tr}', $table);
        array_pop($table);
        foreach ($table as $key => $tr) {
            // 自己可添加对应的替换
            $td = explode('{td}', str_replace(array("\r\n", "\n", "\r", "\t"), '', $tr));
            array_pop($td);
            $td_array[] = $td;
        }
        return $td_array;
    }

    /**
     * 处理开奖逻辑操作
     * @param $data
     * @return mixed
     */
    private function handleGame($data)
    {
        $game = array_search($this->request['type'],$this->game);
        $da = '大';
        $xiao = '小';
        $dan = '单';
        $shuang = '双';
        $hu = '虎';
        $long = '龙';
        $string = '';
        if (!empty($data)) {
            $data['code_arr'] = explode(',', $data['code']);
            $data['time_str'] = timeToStr($data['time']);
            $data['next_time_str'] = timeToStr($data['next_time']);
            $code_arr = $data['code_arr'];
            $code_arr = array_splice($code_arr, 0, 2);
            $data['k1'] = array_sum($code_arr);
            for ($i = 0; $i < count($data['code_arr']) / 2; $i++) {
                if ($i == 0) $data['k2'] = $data['code_arr'][$i] > $data['code_arr'][count($data['code_arr']) - ($i + 1)] ? $da : $xiao;
                $string .= intval($data['code_arr'][$i]) > intval($data['code_arr'][count($data['code_arr']) - ($i + 1)]) ? $long : $hu;
            }
            $data['k3'] = $data['k1'] % 2 == 0 ? $shuang : $dan;
            $data['k4'] = mb_str_split($string);
            if($game=='cqssc'){
                $getinfo = $this->getazxy5(1);
                if($getinfo)
                $data['k1'] = $getinfo['k1'];
                $data['k2'] = $getinfo['k2'];
                $data['k3'] = $getinfo['k3'];
                $data['k4'] = $getinfo['k4'];
            }
        }
        return $data;
    }

    /**
     * 澳洲幸运5历史开奖
     * @param int $type
     * @return array|mixed
     */
    public function getazxy5($type=0)
    {
        $type = $this->request['type']?$this->request['type']:$type;
        $getResult = json_decode(file_get_contents('https://www.52cp.cn/henei5/api_tool/history2?lottery=azxy5&type=1&period=10'),true);
        $getResult = $getResult['result'];
        $data = [];
        if(!empty($getResult)){
            foreach ($getResult as $k=>$v) {
                foreach ($v as $ks=>$vs) {
                    preg_match('/^num./',$ks,$num);
                    if($num) $data[$k]['code_arr'][] = $vs;
                    if($ks=='expect') $data[$k][$ks] = $vs;
                    if($ks=='sum_val') $data[$k]['k1'] = $vs;
                    if($ks=='sum_dx') $data[$k]['k2'] = $vs;
                    if($ks=='sum_ds') $data[$k]['k3'] = $vs;
                    if($ks=='lh') $data[$k]['k4'] = $vs;
                    $data[$k]['code'] = implode(',',$data[$k]['code_arr']);
                }
            }
            if(!$type) {
                return $data;
            }else{
                return $data[$type-1];
            }
        }
    }

    /**
     * 递归算法
     * @param $target
     * @param array $arr
     * @return mixed
     */
    private function xunhuan($target, $arr = [])
    {
        for ($i = 0; $i < count($arr); $i++) {
            if ($target == 0) return $arr[$i];//跳出循环
            $target--;
        }
        return self::xunhuan($target, $arr);
    }

    /**
     * 投注核对结果
     * @param string game (游戏名)
     * @return string
     */
    public function getCheckResult()
    {
        //获取下期期数
        $terminfo = (new ShiroiDb())->table('fn_open')->where(['type'=>$this->gameType])->order('term desc')->get();
        $term = $terminfo['next_term'];
        $rediskey = "{$this->request['game']}_overtime";
        $str = "{$term}期有效投注核对<br>";
        $all_user_order = self::handleUserOrder($term);
        foreach ($all_user_order as $userid => $data) {
            $str .= "<div>";
            //用户信息
            $userinfo = (new ShiroiDb())->table('fn_user')->where(['userid' => $userid])->get();
            $str .= "[<a style='color: dodgerblue;'>{$userinfo['username']}</a>]积分:{$userinfo['money']}<br>";
            foreach ($data as $k=>$v) {
                $str .= "$k";
                $tmp = [];
                $tmpstr = "";
                foreach ($v as $ks=>$vs) {
                    $tmp[reset($vs)] += next($vs);
                }
                foreach($tmp as $ks=>$vs) {
                    if($tmpstr) {
                        $tmpstr .= "&nbsp;{$ks}/{$vs}";
                    }else{
                        $tmpstr .= "{$ks}/{$vs}";
                    }
                };
                $str .= "[$tmpstr]";
            }
            $str .="</div>";
        };
        //判断redis key是否存在
        $get = $this->shiroiRedis->get($rediskey);
        if(!$get){
            //设置redis key失效期
            $expire = strtotime($terminfo['next_time']) - time();
            $this->shiroiRedis->setex($rediskey,$expire,'随便什么都行');
            //发送消息
            if($all_user_order) {
                self::insertChat($str,$this->request['game'],$this->request['roomid']);
            }
            return 'success';
        }
        return  'fail';
    }

    /**
     * 统计订单
     * @param $term (投注下一期的所有投注数据)
     * @return array
     */
    private function handleUserOrder($term)
    {
        $arr = [];
        $merge_arr = [];
        $minci = [1 => '冠军', 2 => '亚军', 3 => '第三名', 4 =>'第四名', 5 => '第五名', 6 => '第六名', 7 => '第七名', 8 => '第八名', 9 => '第九名', 10 => '第十名'];
        $all_user_order = (new ShiroiDb())->table($this->order)->where(['term' => $term])->select();
        foreach ($all_user_order as $v) $arr[$v['userid']][] = $v;//将用户的下注信息整合成一起
        //数据处理
        foreach ($arr as $k=>$v) {
            foreach ($v as $ks=>$vs) {
                $merge_arr[$k][$minci[intval($vs['mingci'])]][] = [$vs['content'],$vs['money']];
            }
        }
        return $merge_arr;
    }

    /**
     * 在线人数统计
     * @param int roomid (房间id)
     * @param string game (游戏名)
     * @return string
     */
    public function getLotteryTotal()
    {
        //统计在线人数
        $time = time()-300;
        $roomid = $this->request['roomid'];
        $rediskey = "{$this->request['game']}_total";
        //获取下期期数
        $terminfo = (new ShiroiDb())->table('fn_open')->where(['type'=>$this->gameType])->order('term desc')->get();
        $str = '';
        $where = "`roomid` = '{$roomid}'";
        $where .= " and `statustime` > '{$time}'";
        //房间名
        $roomname = (new ShiroiDb())->table('fn_room')->where(['roomid'=>$roomid])->value('roomname');
        $str .= "★☆[{$roomname}]☆★<br>";
        //在线用户
        $userinfo = (new ShiroiDb())->table('fn_user')->where($where,false)->select();
        $count = count($userinfo);
        $money = array_sum(array_column($userinfo,'money'));
        $str .= "在线{$count}人  总分：{$money}<br>";
        $str .= "==================<br>";
        foreach ($userinfo as $k=>$v) {
            $str .= "[{$v['username']}] {$v['money']}<br>";
        }
        $get = $this->shiroiRedis->get($rediskey);
        if(!$get){
            //设置redis key失效期
            $expire = strtotime($terminfo['next_time']) - time();
            $this->shiroiRedis->setex($rediskey,$expire,'随便什么都行');
            //发送消息
            if($userinfo) self::insertChat($str,$this->request['game'],$roomid);
            return 'success';
        }
        return 'faill';
    }

    /**
     * 开奖结果
     * @param $term (开奖期数)
     * @return string
     */
    public function getLotteryResult($term='')
    {
        $term = $term?$term:$this->request['term'];
        $str = "<div>";
        $arr = [];
        $minci = [1 => '冠军', 2 => '亚军', 3 => '第三名', 4 =>'第四名', 5 => '第五名', 6 => '第六名', 7 => '第七名', 8 => '第八名', 9 => '第九名', 10 => '第十名'];
        //开奖结果
        $oldcode = (new ShiroiDb())->table('fn_open')->where(['term'=>$term])->value('code');
        $codeArr = explode(',',$oldcode);//期数
        foreach ($codeArr as $k=>$v) $codeArr[$k] = intval($v);//转整数
        $newcode = implode(' ',$codeArr);//期数
//        $str .= "==========================<br>";
//        $str .= "第{$term}期开奖号：<br>";
//        $str .= "{$newcode}<br>";
//        $str .= "==========================<br>";
//        $str .= "以下为本期中奖名单：<br>";
        //用户信息
        $orderinfo = (new ShiroiDb())->table($this->order)
            ->field("userid,username,term,mingci,content,sum(money) AS money,addtime,sum(`status`) as `status`,roomid",false)
            ->where(['term' => $term])->group("mingci,content")->select();
        if(!$orderinfo) return '';
        foreach ($orderinfo as $k=>$v) $arr[$v['userid']][] = $v;
        foreach ($arr as $k=>$v) {
            foreach ($v as $ks=>$vs) {
//                if(intval($vs['status'])>0){
                    if($ks==0) $str .= "[<a style='color: red;' href='javascript:;'>".$vs['username']."</a>]";
                    $str .= "&nbsp;".$minci[intval($vs['mingci'])].$vs['content'].'/'.$vs['money'].'=';
                    if($vs['status']>0) {
                        $str .= "<a style='color: red;' href='javascript:;'>{$vs['status']}</a>";
                    }else {
                        $str .= "<a style='color: green;' href='javascript:;'>{$vs['status']}</a>";
                    }
//                }
            }
        }
        $str .="</div>";
        return  $str;
    }

    /**
     * 发送管理员消息
     * @param string $content
     * @param string $game
     * @param int $roomid
     * @return mixed
     */
    private function insertChat($content = '',$game='',$roomid=1)
    {
        $userid = 'system';
        $username = '播报员';
        $headimg = get_query_val('fn_setting', 'setting_robotsimg', array('roomid' => $roomid));
        $addtime = date('H:i:s');
        $type = 'S3';
        $data = compact('userid','username','headimg','addtime','type','content','game','roomid');
        if($content && $game) {
            $this->shiroiDb->table('fn_chat', 'insert')
                ->insert($data);
        }
    }
}

/**
 * 获取操作方法
 * @param string f 方法名
 */
if (isset($_GET['f']) && !empty($_GET['f'])) {
    try {
        $function = $_GET['f'];
        $ShiroiInterface = new ShiroiInterface();
        to_json($ShiroiInterface->$function());
    } catch (Exception $e) {
        $e->getMessage();
    }
}

/**
 * json格式化
 * @param $data
 * @param string $message
 * @param int $status
 */
function to_json($data, $message = 'success', $status = 200)
{
    $arr['status'] = $status;
    $arr['message'] = $message;
    $arr['data'] = $data;
    echo json_encode($arr, true);
}

/**
 * @param $time (时间 格式为:1997-01-10 00:00:00| 1997-01-10)
 * @param string $type (要转换的时间格式 timestamp|datetime)
 * @param int $day (天数 往时间后面增加或者减少天数)
 * @param string $mode (计算方式 add|sub)
 * @return false|int|string
 * @example  举例：timeToStr('1997-01-10 00:00:00','datetime',0,'sub')
 */
function timeToStr($time, $type = 'timestamp', $day = 0, $mode = 'add')
{
    if (is_int($time)) {
        $strToTime = $time;
    } else {
        $strToTime = strtotime($time);
    }
    $rule = $mode == 'add' ? '+' : '-';
    if ($day > 0) $strToTime = strtotime("{$rule} {$day} day", $strToTime);
    return $type == 'timestamp' ? $strToTime : date('Y-m-d H:i:s', $strToTime);
}