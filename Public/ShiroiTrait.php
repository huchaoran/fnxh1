<?php
/**
 * @author shiroi
 * @e-mail 707305003@qq.com
 * @site https://shiroi.top
 * @create 2020/7/23
 */
include_once './ShiroiDb.php';
include_once './ShiroiRedis.php';

trait ShiroiTrait
{
    public $shiroiDb;//数据库链接
    public $shiroiRedis;//redis链接
    public $request,$get,$post,$file;//获取传递的参数
    protected $today, $todayStart, $todayEnd;//当前时间 和 当天的00：00 和 当天23：59
    protected $limit, $page, $startLimit;//条数 页数 开始页数
    private $getTimeType,//时间类型
        $endTime,//起始时间（往后减时间）
        $whereBetweenTime;//时间范围字符串
    private $failCode;//定义错误编码
    private $table = [
        'jssc' => 'fn_jsscorder',
//        'cqssc' => 'fn_sscorder',
//        'pk10' => 'fn_order',
//        'xyft' => 'fn_flyorder',
        'jssm' => 'fn_smorder'
    ];//表
    private $game = [
        'jssc' => 7,
//        'cqssc' => 3,
//        'pk10' => 1,
//        'xyft' => 2,
        'jssm' => 12
    ];
    private $fengpan = [
//        'pk10' => 'fn_lottery1',
//        'cqssc' => 'fn_lottery3',
        'jssm' => 'fn_lottery12',
        'jssc' => 'fn_lottery7',
//        'xyft' => 'fn_lottery2',
    ];
    private $order,$gameType,$fOrder;
    /**
     * ShiroiInterface constructor.
     */
    public function __construct()
    {
        //获取传递的参数
        $this->request = $_REQUEST;
        //GET
        $this->get = $_GET;
        //POST
        $this->post = $_POST;
        //文件上传
        $this->file = $_FILES;
        //引入配置文件
        require_once 'config.php';
        try {
            //替换参数
            if(isset($db)){
                $db['username'] = $db['user'];
                $db['password'] = $db['pass'];
                $db['dbname'] = $db['name'];
                $this->shiroiDb = new ShiroiDb($db);//连接数据库
            }
            if(isset($rdb)){
                $port= $rdb['port'];
                $host = $rdb['host'];
                $this->shiroiRedis = new ShiroiRedis(compact($port,$host),$rdb['pass']);//连接redis
            }
        } catch(\Exception $e) {}
        //参数定义
        $this->today = date("Y-m-d H:i:s", time());
        $this->todayStart = date("Y-m-d 00:00:00", strtotime(date("Y-m-d"), time()));
        $this->todayEnd = date("Y-m-d 23:59:59", strtotime(date("Y-m-d"), time()));
        $this->limit = isset($this->request['limit']) && !empty($this->request['limit']) ? $this->request['limit'] : 10;
        $this->page = isset($this->request['page']) && !empty($this->request['page']) ? $this->request['page'] : 1;
        $this->startLimit = ($this->page - 1) * $this->limit;
        //时间条件
        $this->getTimeType = isset($this->request['time']) && !empty($this->request['time']) ? $this->request['time'] : 'week';
        $this->endTime = date('Y-m-d H:i:s', strtotime("- 1 {$this->getTimeType}"));
        //引入错误编码返回
        $failCode = require_once ('./ShiroiFailType.php');
        $this->failCode = $failCode['code'];
        $this->order = $this->table[$this->request['game']];//获取查询订单
        $this->gameType = $this->game[$this->request['game']];//获取游戏类型
        $this->fOrder = $this->fengpan[$this->request['game']];//获取封盘时间表
        //获取时间区间
        if($this->request['startTime'] && $this->request['endTime']) {
            $this->request['startTime'] = date('Y-m-d H:i:s',strtotime($this->request['startTime']));
            $this->request['endTime'] = date('Y-m-d H:i:s',strtotime($this->request['endTime']));
        }
    }
}