<?php
/**
 * @author shiroi
 * @e-mail 707305003@qq.com
 * @site https://shiroi.top
 * @create 2020/7/21
 */

class ShiroiDb
{
    private $db = [
        'host' => '129.226.170.216',         //设置服务器地址
        'port' => '3306',              //设端口
        'dbname' => 'a3ym',             //设置数据库名
        'username' => 'a3ym',           //设置账号
        'password' => 'a3ym',      //设置密码
        'charset' => 'utf8'
    ];
    public $pdo, $sql, $field = '*', $table, $order, $group, $limit, $where_type = 1,$handle_type='select';

    /**
     * ShiroiDb constructor.
     * @param array $db
     */
    function __construct($db = [])
    {
        self::db_init($db);//重置数据库配置
    }

    /**
     * 查询数据列表(多条)
     * @return mixed
     */
    function select()
    {
//        echo $this->sql;die;
        $stmt = $this->pdo->query($this->sql);
        return $stmt->fetchAll();
    }

    /**
     * 原生sql语句查询
     * @param string $sql
     * @return mixed
     */
    function query($sql = '')
    {
        $stmt = $this->pdo->query($sql);
        return $stmt->fetchAll();
    }

    /**
     * 查询数据列表(单条)
     * @return mixed
     */
    function get()
    {
//        echo $this->sql;die;
        $this->limit(1);
        $stmt = $this->pdo->query($this->sql);
        return $stmt->fetch();
    }

    /**
     * 更新数据库
     * @param array $update
     * @return mixed
     */
    function update(array $update=[])
    {
        $whereStr = "";
        if($update) {
            if (!empty($update)) {
                if (is_string($update)) {
                    $whereStr = $update;
                } else {
                    $i = 0;
                    foreach ($update as $k => $v) {
                        $i++;
                        if ($i > 1) {
                            $whereStr .= ' , ';
                        }
                        $whereStr .= "`{$k}`='{$v}'";
                    }
                }
                $whereStr = " set {$whereStr}";
            }
        }
//        dump($this->sql);
        $this->sql = substr_replace($this->sql,$whereStr,strpos($this->sql,' where'),0);
//        dd($this->sql);
        return $this->pdo->exec($this->sql);
    }

    /**
     * 数据插入
     * @param array $insert
     * @return mixed
     */
    function insert(array $insert = [])
    {
        if($insert){
            $keys = '';
            $values = '';
            $i = 0;
            foreach ($insert as $k=>$v) {
                $k = "`{$k}`";
                if(!is_int($v)) $v = '"'.$v.'"';
                if($i<1){
                    $keys .= $k;
                    $values .= $v;
                }else{
                    $keys .= ",{$k}";
                    $values .= ",{$v}";
                }
                $i++;
            }
            $this->sql .= "($keys) values($values)";
        }
//        dump($this->sql);
        $this->pdo->exec($this->sql);
        return $this->pdo->lastInsertId();
    }

    /**
     * 删除数据
     * @return mixed
     */
    function delete()
    {
        return $this->pdo->exec($this->sql);
    }

    /**
     * 查询该条数据某一值
     * @param string $value
     * @return mixed
     */
    function value($value='')
    {
        $this->limit(1);
        $stmt = $this->pdo->query($this->sql)->fetch();
        if(count($stmt)>0) return $stmt[$value];
    }

    /**
     * 查询数据返回条数
     * @return mixed
     */
    function count()
    {
        $stmt = $this->pdo->query($this->sql);
        return $stmt->rowCount();
    }

    function table($table,$handle_type='select')
    {
        $this->table = $table;
        $this->handle_type = $handle_type;
        switch ($this->handle_type) {
            case 'select':
                $this->sql = "select {$this->field} from {$this->table}";
                break;
            case 'update':
                $this->sql = "update {$this->table}";
                break;
            case 'insert':
                $this->sql = "insert into {$this->table}";
                break;
            case 'delete':
                $this->sql = "delete from {$this->table}";
        }
        return $this;
    }

    function field($field,$type=true,$str='')
    {
        if($type){
            foreach (explode(',',$field) as $k=>$v) {
                if($k>0) $str .= ',';
                if($v) $str .= "`{$v}`";
            }
        }else{
            $str .= $field;
        }
        $this->field = $str;
        $this->sql = "select {$this->field} from {$this->table}";
        return $this;
    }

    function where($where,$init=0)
    {
        $whereStr = "";
        if (!empty($where)) {
            if (is_string($where)) {
                $whereStr = $where;
            } else {
                $i = 0;
                foreach ($where as $k => $v) {
                    $i++;
                    if ($i > 1) {
                        $whereStr .= ' and ';
                    }
                    $whereStr .= "`{$k}`='{$v}'";
                }
            }
            $whereStr = ltrim($whereStr, '');
            $whereStr = ltrim($whereStr, 'where');
            if ($this->where_type <= 1 || $init != 0) {
                $whereStr = " where {$whereStr}";
            }else{
                $whereStr = " and {$whereStr}";
            }
        }
        $this->sql .= $whereStr;
        if($this->handle_type=='select') $this->where_type++;
        return $this;
    }

    function order($order)
    {
        $this->order = $order;
        $this->sql .= ' order by ' . $order;
        return $this;
    }

    function group($group)
    {
        $this->group = $group;
        $this->sql .= ' group by ' . $group;
        return $this;
    }

    function limit($limit)
    {
        $this->sql .= " limit {$limit}";
        return $this;
    }

    function having($having)
    {
        $this->sql .= " having {$having}";
        return $this;
    }

    /**
     * 数据库连接参数配置
     * @param $db
     */
    private function db_init($db)
    {
        $this->db['host'] = isset($db['host']) && !empty($db['host']) ? $db['host'] : $this->db['host'];
        $this->db['port'] = isset($db['port']) && !empty($db['port']) ? $db['port'] : $this->db['port'];
        $this->db['dbname'] = isset($db['dbname']) && !empty($db['dbname']) ? $db['dbname'] : $this->db['dbname'];
        $this->db['username'] = isset($db['username']) && !empty($db['username']) ? $db['username'] : $this->db['username'];
        $this->db['password'] = isset($db['password']) && !empty($db['password']) ? $db['password'] : $this->db['password'];
        $this->db['charset'] = isset($db['charset']) && !empty($db['charset']) ? $db['charset'] : $this->db['charset'];
        $this->db['dsn'] = "mysql:host={$this->db['host']};dbname={$this->db['dbname']};port={$this->db['port']};charset={$this->db['charset']}";
        self::db_status();
    }

    /**
     * 返回数据库连接状态
     */
    private function db_status()
    {
        //连接
        $options = array(
//            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, //默认是PDO::ERRMODE_SILENT, 0, (忽略错误模式)
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, // 默认是PDO::FETCH_BOTH, 4
        );
        try {
            $this->pdo = new PDO($this->db['dsn'], $this->db['username'], $this->db['password'], $options);
        } catch (PDOException $e) {
            errorc__("数据库连接失败:[" . sql_write_log(array('数据库链接出错', $e->getMessage(),$e->getMessage())) . "]");
        }
    }
}
