### [v2.9.4](https://github.com/youzan/vant/compare/v2.9.3...v2.9.4)

`2020-07-29`

**Bug Fixes**

- Popup: incorrect lock scroll in some cases [#6892](https://github.com/youzan/vant/issues/6892)
- Stepper: double tap to scroll in safari [#6882](https://github.com/youzan/vant/issues/6882)

**Document**

- changelog: 2.9.3 [96f3e9](https://github.com/youzan/vant/commit/96f3e90ff3fa93d2f47795584ec2f2165e593eab)

**Feature**

- ShareSheet: add className option [#6886](https://github.com/youzan/vant/issues/6886)
- Sku: modify default min year of sku date picker [#6879](https://github.com/youzan/vant/issues/6879)
