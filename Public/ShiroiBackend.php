<?php
/**
 * @author shiroi
 * @e-mail 707305003@qq.com
 * @site https://shiroi.top
 * @create 2020/7/23
 */
include './ShiroiTrait.php';

class ShiroiBackend
{
    use ShiroiTrait;//引入公共

    /**
     * 聊天设置
     * @method POST|GET
     * @return mixed
     */
    public function chat_setting()
    {
        return $this->shiroiDb
            ->table('fn_config')
            ->where(['key' => 'chat'])
            ->value('value');
    }

    /**
     * 聊天设置列表
     * @method POST|GET
     * @param array $arr
     * @return mixed
     */
    public function chat_setting_list($arr = [])
    {
        $data = $this->shiroiDb
            ->field('id,key,value,beforevalue,game,status,type')
            ->table('fn_config')
            ->where(['game' => $this->request['game']])
            ->select();
        foreach ($data as $k=>$v) $arr[$v['type']][] = $v;//数据处理
        return $arr;
    }

    /**
     * 修改消息配置
     * @method POST|GET
     * @param int id (消息id)
     * @param int status (状态 1=>已开启  2=>未开启)
     * @param string value (内容)
     * @return mixed
     */
    public function modifyConfig()
    {
        return $this->shiroiDb
            ->table('fn_config','update')
            ->where(['id'=>$this->request['id']])
            ->update($this->post);
    }
}

/**
 * 获取操作方法
 * @param string f 方法名
 */
if (isset($_GET['f']) && !empty($_GET['f'])) {
    try {
        $function = $_GET['f'];
        $ShiroiInterface = new ShiroiBackend();
        to_json($ShiroiInterface->$function());
    }catch (Exception $e){
        $e->getMessage();
    }
}

/**
 * json格式化
 * @param $data
 * @param string $message
 * @param int $status
 */
function to_json($data, $message = 'success', $status = 200)
{
    $arr['status'] = $status;
    $arr['message'] = $message;
    $arr['data'] = $data;
    echo json_encode($arr, true);
}