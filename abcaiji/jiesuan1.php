<?php
include '../Public/ShiroiDb.php';

$config = require_once('../Public/ShiroiConfig.php');
$keyword = $config['keyword'];
$table = 'fn_order';

function 用户_上分($Userid, $Money, $room, $game, $term, $content)
{
    update_query('fn_user', array('money' => '+=' . $Money), array('userid' => $Userid, 'roomid' => $room));
    insert_query("fn_marklog", array("userid" => $Userid, 'type' => '上分', 'content' => $term . '期' . $game . '中奖派彩' . $content, 'money' => $Money, 'roomid' => $room, 'addtime' => 'now()'));
}

//TODO 澳洲幸运10
function SM_jiesuan()
{
    $table = 'fn_smorder';
    select_query($table, '*', array("status" => "未结算"));
    while ($con = db_fetch_array()) {
        $cons[] = $con;
    }
    foreach ($cons as $con) {
        $id = $con['id'];
        $roomid = $con['roomid'];
        $user = $con['userid'];
        $term = $con['term'];
        $zym_1 = $con['mingci'];
        $zym_8 = $con['content'];
        $zym_7 = $con['money'];
        $opencode = get_query_val('fn_open', 'code', "`term` = '$term' and `type` = '12'");
        $opencode = str_replace('10', '0', $opencode);
        if ($opencode == "") continue;
        $code = explode(',', $opencode);
        if ($zym_1 == '1') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery12', 'da', "`roomid` = '$roomid'");
                if ((int)$code[0] > 5 || $code[0] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery12', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[0] < 6 && $code[0] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery12', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery12', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery12', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0 && (int)$code[0] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery12', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0 && (int)$code[0] > 5 || (int)$code[0] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery12', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0 && (int)$code[0] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery12', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0 && (int)$code[0] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery12', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[0] > (int)$code[9] && $code[9] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[0] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery12', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[0] < (int)$code[9] && $code[0] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[9] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[0]) {
                $peilv = get_query_val('fn_lottery12', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '2') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery12', 'da', "`roomid` = '$roomid'");
                if ((int)$code[1] > 5 || $code[1] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery12', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[1] < 6 && $code[1] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery12', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery12', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery12', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0 && (int)$code[1] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery12', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0 && (int)$code[1] > 5 || (int)$code[1] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery12', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0 && (int)$code[1] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery12', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0 && (int)$code[1] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery12', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[1] > (int)$code[8] && $code[8] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[1] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery12', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[1] < (int)$code[8] && $code[1] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[8] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[1]) {
                $peilv = get_query_val('fn_lottery12', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '3') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery12', 'da', "`roomid` = '$roomid'");
                if ((int)$code[2] > 5 || $code[2] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery12', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[2] < 6 && $code[2] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery12', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery12', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery12', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0 && (int)$code[2] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery12', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0 && (int)$code[2] > 5 || (int)$code[2] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery12', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0 && (int)$code[2] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery12', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0 && (int)$code[2] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery12', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[2] > (int)$code[7] && $code[7] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[2] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery12', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[2] < (int)$code[7] && $code[2] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[7] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[2]) {
                $peilv = get_query_val('fn_lottery12', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '4') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery12', 'da', "`roomid` = '$roomid'");
                if ((int)$code[3] > 5 || $code[3] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery12', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[3] < 6 && $code[3] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery12', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery12', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery12', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0 && (int)$code[3] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery12', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0 && (int)$code[3] > 5 || (int)$code[3] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery12', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0 && (int)$code[3] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery12', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0 && (int)$code[3] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery12', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[3] > (int)$code[6] && $code[6] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[3] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery12', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[3] < (int)$code[6] && $code[3] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[6] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[3]) {
                $peilv = get_query_val('fn_lottery12', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '5') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery12', 'da', "`roomid` = '$roomid'");
                if ((int)$code[4] > 5 || $code[4] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery12', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[4] < 6 && $code[4] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery12', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery12', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery12', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0 && (int)$code[4] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery12', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0 && (int)$code[4] > 5 || (int)$code[4] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery12', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0 && (int)$code[4] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery12', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0 && (int)$code[4] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery12', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[4] > (int)$code[5] && $code[5] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[4] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery12', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[4] < (int)$code[5] && $code[4] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[5] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[4]) {
                $peilv = get_query_val('fn_lottery12', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '12') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery12', 'da', "`roomid` = '$roomid'");
                if ((int)$code[5] > 5 || $code[5] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery12', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[5] < 6 && $code[5] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery12', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery12', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery12', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0 && (int)$code[5] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery12', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0 && (int)$code[5] > 5 || (int)$code[5] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery12', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0 && (int)$code[5] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery12', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0 && (int)$code[5] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[5]) {
                $peilv = get_query_val('fn_lottery12', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '7') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery12', 'da', "`roomid` = '$roomid'");
                if ((int)$code[6] > 5 || $code[6] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery12', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[6] < 6 && $code[6] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery12', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery12', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery12', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0 && (int)$code[6] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery12', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0 && (int)$code[6] > 5 || (int)$code[6] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery12', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0 && (int)$code[6] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery12', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0 && (int)$code[6] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[6]) {
                $peilv = get_query_val('fn_lottery12', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '8') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery12', 'da', "`roomid` = '$roomid'");
                if ((int)$code[7] > 5 || $code[7] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery12', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[7] < 6 && $code[7] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery12', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery12', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery12', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0 && (int)$code[7] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery12', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0 && (int)$code[7] > 5 || (int)$code[7] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery12', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0 && (int)$code[7] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery12', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0 && (int)$code[7] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[7]) {
                $peilv = get_query_val('fn_lottery12', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '9') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery12', 'da', "`roomid` = '$roomid'");
                if ((int)$code[8] > 5 || $code[8] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery12', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[8] < 6 && $code[8] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery12', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery12', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery12', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0 && (int)$code[8] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery12', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0 && (int)$code[8] > 5 || (int)$code[8] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery12', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0 && (int)$code[8] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery12', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0 && (int)$code[8] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[8]) {
                $peilv = get_query_val('fn_lottery12', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '0') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery12', 'da', "`roomid` = '$roomid'");
                if ((int)$code[9] > 5 || $code[9] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery12', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[9] < 6 && $code[9] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery12', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery12', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery12', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0 && (int)$code[9] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery12', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0 && (int)$code[9] > 5 || (int)$code[9] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery12', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0 && (int)$code[9] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery12', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0 && (int)$code[9] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[9]) {
                $peilv = get_query_val('fn_lottery12', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '和') {
            if ($code[0] == "0" || $code[1] == "0") {
                $hz = (int)$code[0] + (int)$code[1] + 10;
            } else {
                $hz = (int)$code[0] + (int)$code[1];
            }
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery12', 'heda', "`roomid` = '$roomid'");
                if ($hz > 11) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery12', 'hexiao', "`roomid` = '$roomid'");
                if ($hz < 12) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery12', 'hedan', "`roomid` = '$roomid'");
                if ($hz % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery12', 'heshuang', "`roomid` = '$roomid'");
                if ($hz % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ((int)$zym_8 == $hz) {
                if ($hz == 3 || $hz == 4 || $hz == 18 || $hz == 19) {
                    $peilv = get_query_val('fn_lottery12', 'he341819', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 5 || $hz == 6 || $hz == 16 || $hz == 17) {
                    $peilv = get_query_val('fn_lottery12', 'he561617', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 7 || $hz == 8 || $hz == 14 || $hz == 15) {
                    $peilv = get_query_val('fn_lottery12', 'he781415', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 9 || $hz == 10 || $hz == 12 || $hz == 13) {
                    $peilv = get_query_val('fn_lottery12', 'he9101213', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 11) {
                    $peilv = get_query_val('fn_lottery12', 'he11', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '香港赛马', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
    }
}

//TODO 幸运飞艇
function MLAFT_jiesuan()
{
    $table = 'fn_flyorder';
    select_query($table, '*', array("status" => "未结算"));
    while ($con = db_fetch_array()) {
        $cons[] = $con;
    }
    foreach ($cons as $con) {
        echo $con['id'];
        $id = $con['id'];
        $roomid = $con['roomid'];
        $user = $con['userid'];
        $term = $con['term'];
        $zym_1 = $con['mingci'];
        $zym_8 = $con['content'];
        $zym_7 = $con['money'];
        $opencode = get_query_val('fn_open', 'code', "`term` = '$term' and `type` = '2'");
        $opencode = str_replace('10', '0', $opencode);
        if ($opencode == "") continue;
        $code = explode(',', $opencode);
        if ($zym_1 == '1') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery2', 'da', "`roomid` = '$roomid'");
                if ((int)$code[0] > 5 || $code[0] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery2', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[0] < 6 && $code[0] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery2', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery2', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery2', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0 && (int)$code[0] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery2', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0 && (int)$code[0] > 5 || (int)$code[0] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery2', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0 && (int)$code[0] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery2', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0 && (int)$code[0] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery2', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[0] > (int)$code[9] && $code[9] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[0] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery2', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[0] < (int)$code[9] && $code[0] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[9] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[0]) {
                $peilv = get_query_val('fn_lottery2', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '2') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery2', 'da', "`roomid` = '$roomid'");
                if ((int)$code[1] > 5 || $code[1] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery2', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[1] < 6 && $code[1] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery2', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery2', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery2', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0 && (int)$code[1] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery2', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0 && (int)$code[1] > 5 || (int)$code[1] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery2', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0 && (int)$code[1] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery2', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0 && (int)$code[1] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery2', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[1] > (int)$code[8] && $code[8] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[1] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery2', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[1] < (int)$code[8] && $code[1] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[8] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[1]) {
                $peilv = get_query_val('fn_lottery2', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '3') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery2', 'da', "`roomid` = '$roomid'");
                if ((int)$code[2] > 5 || $code[2] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery2', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[2] < 6 && $code[2] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery2', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery2', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery2', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0 && (int)$code[2] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery2', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0 && (int)$code[2] > 5 || (int)$code[2] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery2', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0 && (int)$code[2] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery2', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0 && (int)$code[2] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery2', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[2] > (int)$code[7] && $code[7] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[2] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery2', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[2] < (int)$code[7] && $code[2] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[7] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[2]) {
                $peilv = get_query_val('fn_lottery2', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '4') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery2', 'da', "`roomid` = '$roomid'");
                if ((int)$code[3] > 5 || $code[3] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery2', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[3] < 6 && $code[3] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery2', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery2', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery2', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0 && (int)$code[3] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery2', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0 && (int)$code[3] > 5 || (int)$code[3] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery2', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0 && (int)$code[3] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery2', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0 && (int)$code[3] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery2', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[3] > (int)$code[6] && $code[6] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[3] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery2', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[3] < (int)$code[6] && $code[3] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[6] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[3]) {
                $peilv = get_query_val('fn_lottery2', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '5') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery2', 'da', "`roomid` = '$roomid'");
                if ((int)$code[4] > 5 || $code[4] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery2', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[4] < 6 && $code[4] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery2', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery2', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery2', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0 && (int)$code[4] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery2', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0 && (int)$code[4] > 5 || (int)$code[4] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery2', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0 && (int)$code[4] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery2', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0 && (int)$code[4] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery2', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[4] > (int)$code[5] && $code[5] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[4] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery2', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[4] < (int)$code[5] && $code[4] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[5] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[4]) {
                $peilv = get_query_val('fn_lottery2', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '6') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery2', 'da', "`roomid` = '$roomid'");
                if ((int)$code[5] > 5 || $code[5] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery2', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[5] < 6 && $code[5] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery2', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery2', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery2', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0 && (int)$code[5] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery2', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0 && (int)$code[5] > 5 || (int)$code[5] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery2', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0 && (int)$code[5] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery2', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0 && (int)$code[5] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[5]) {
                $peilv = get_query_val('fn_lottery2', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '7') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery2', 'da', "`roomid` = '$roomid'");
                if ((int)$code[6] > 5 || $code[6] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery2', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[6] < 6 && $code[6] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery2', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery2', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery2', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0 && (int)$code[6] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery2', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0 && (int)$code[6] > 5 || (int)$code[6] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery2', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0 && (int)$code[6] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery2', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0 && (int)$code[6] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[6]) {
                $peilv = get_query_val('fn_lottery2', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '8') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery2', 'da', "`roomid` = '$roomid'");
                if ((int)$code[7] > 5 || $code[7] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery2', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[7] < 6 && $code[7] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery2', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery2', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery2', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0 && (int)$code[7] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery2', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0 && (int)$code[7] > 5 || (int)$code[7] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery2', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0 && (int)$code[7] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery2', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0 && (int)$code[7] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[7]) {
                $peilv = get_query_val('fn_lottery2', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '9') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery2', 'da', "`roomid` = '$roomid'");
                if ((int)$code[8] > 5 || $code[8] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery2', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[8] < 6 && $code[8] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery2', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery2', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery2', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0 && (int)$code[8] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery2', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0 && (int)$code[8] > 5 || (int)$code[8] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery2', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0 && (int)$code[8] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery2', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0 && (int)$code[8] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[8]) {
                $peilv = get_query_val('fn_lottery2', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '0') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery2', 'da', "`roomid` = '$roomid'");
                if ((int)$code[9] > 5 || $code[9] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery2', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[9] < 6 && $code[9] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery2', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery2', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery2', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0 && (int)$code[9] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery2', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0 && (int)$code[9] > 5 || (int)$code[9] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery2', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0 && (int)$code[9] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery2', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0 && (int)$code[9] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[9]) {
                $peilv = get_query_val('fn_lottery2', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '和') {
            if ($code[0] == "0" || $code[1] == "0") {
                $hz = (int)$code[0] + (int)$code[1] + 10;
            } else {
                $hz = (int)$code[0] + (int)$code[1];
            }
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery2', 'heda', "`roomid` = '$roomid'");
                if ($hz > 11) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery2', 'hexiao', "`roomid` = '$roomid'");
                if ($hz < 12) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery2', 'hedan', "`roomid` = '$roomid'");
                if ($hz % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery2', 'heshuang', "`roomid` = '$roomid'");
                if ($hz % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ((int)$zym_8 == $hz) {
                if ($hz == 3 || $hz == 4 || $hz == 18 || $hz == 19) {
                    $peilv = get_query_val('fn_lottery2', 'he341819', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 5 || $hz == 6 || $hz == 16 || $hz == 17) {
                    $peilv = get_query_val('fn_lottery2', 'he561617', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 7 || $hz == 8 || $hz == 14 || $hz == 15) {
                    $peilv = get_query_val('fn_lottery2', 'he781415', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 9 || $hz == 10 || $hz == 12 || $hz == 13) {
                    $peilv = get_query_val('fn_lottery2', 'he9101213', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 11) {
                    $peilv = get_query_val('fn_lottery2', 'he11', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '幸运飞艇', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
    }
}

//TODO 澳洲幸运5
function SSC_jiesuan()
{
    $table = 'fn_sscorder';
    select_query($table, '*', array("status" => "未结算"));
    while ($con = db_fetch_array()) {
        $cons[] = $con;
    }
    foreach ($cons as $con) {
        $id = $con['id'];
        $roomid = $con['roomid'];
        $user = $con['userid'];
        $term = $con['term'];
        $zym_1 = $con['mingci'];
        $zym_8 = $con['content'];
        $zym_7 = $con['money'];
        $game = '澳洲幸运5';
        $opencode = get_query_val('fn_open', 'code', "`term` = '$term' and `type` = '3'");
        if ($opencode == "") continue;
        $codes = explode(',', $opencode);
        if (count($codes) != 5) {
            echo 'ERROR!';
            exit();
        } else {
            $zym_2 = array('豹子' => false, '半顺' => false, '顺子' => false, '对子' => false, '杂六' => false);
            $q3 = array((int)$codes[0], (int)$codes[1], (int)$codes[2]);
            sort($q3);
            $zym_3 = array('豹子' => false, '半顺' => false, '顺子' => false, '对子' => false, '杂六' => false);
            $z3 = array((int)$codes[1], (int)$codes[2], (int)$codes[3]);
            sort($z3);
            $zym_4 = array('豹子' => false, '半顺' => false, '顺子' => false, '对子' => false, '杂六' => false);
            $h3 = array((int)$codes[2], (int)$codes[3], (int)$codes[4]);
            sort($h3);
            if ($codes[0] == $codes[1] && $codes[1] == $codes[2]) {
                $zym_2['豹子'] = true;
            } elseif ($codes[0] == $codes[1] || $codes[0] == $codes[2] || $codes[1] == $codes[2]) {
                $zym_2['对子'] = true;
            } elseif (($q3[0] + 1 == $q3[1] && $q3[1] + 1 == $q3[2]) || ($q3[0] == '0' && $q3[1] == '8' && $q3[2] == '9') || ($q3[0] == '0' && $q3[1] == '1' && $q3[2] == '9')) {
                $zym_2['顺子'] = true;
            } elseif (($q3[0] + 1 == $q3[1] || $q3[1] + 1 == $q3[2]) || ($q3[0] == '0' && $q3[2] == '9')) {
                $zym_2['半顺'] = true;
            } else {
                $zym_2['杂六'] = true;
            }
            if ($codes[1] == $codes[2] && $codes[2] == $codes[3]) {
                $zym_3['豹子'] = true;
            } elseif ($codes[1] == $codes[2] || $codes[1] == $codes[3] || $codes[2] == $codes[3]) {
                $zym_3['对子'] = true;
            } elseif (($z3[0] + 1 == $z3[1] && $z3[1] + 1 == $z3[2]) || ($z3[0] == '0' && $z3[1] == '8' && $z3[2] == '9') || ($z3[0] == '0' && $z3[1] == '1' && $z3[2] == '9')) {
                $zym_3['顺子'] = true;
            } elseif (($z3[0] + 1 == $z3[1] || $z3[1] + 1 == $z3[2]) || ($z3[0] == '0' && $z3[2] == '9')) {
                $zym_3['半顺'] = true;
            } else {
                $zym_3['杂六'] = true;
            }
            if ($codes[2] == $codes[3] && $codes[3] == $codes[4]) {
                $zym_4['豹子'] = true;
            } elseif ($codes[2] == $codes[3] || $codes[2] == $codes[4] || $codes[3] == $codes[4]) {
                $zym_4['对子'] = true;
            } elseif (($h3[0] + 1 == $h3[1] && $h3[1] + 1 == $h3[2]) || ($h3[0] == '0' && $h3[1] == '8' && $h3[2] == '9') || ($h3[0] == '0' && $h3[1] == '1' && $h3[2] == '9')) {
                $zym_4['顺子'] = true;
            } elseif (($h3[0] + 1 == $h3[1] || $h3[1] + 1 == $h3[2]) || ($h3[0] == '0' && $h3[2] == '9')) {
                $zym_4['半顺'] = true;
            } else {
                $zym_4['杂六'] = true;
            }
            var_dump($zym_4);
            $zong = (int)$codes[0] + (int)$codes[1] + (int)$codes[2] + (int)$codes[3] + (int)$codes[4];
        }
        if ($zym_1 == '总') {
            if ($zym_8 == '大' && $zong > 22) {
                $peilv = get_query_val('fn_lottery3', 'zongda', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '小' && $zong < 23) {
                $peilv = get_query_val('fn_lottery3', 'zongxiao', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '单' && $zong % 2 != 0) {
                $peilv = get_query_val('fn_lottery3', 'zongdan', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '双' && $zong % 2 == 0) {
                $peilv = get_query_val('fn_lottery3', 'zongshuang', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '龙' && $codes[0] > $codes[4]) {
                $peilv = get_query_val('fn_lottery3', '`long`', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '虎' && $codes[0] < $codes[4]) {
                $peilv = get_query_val('fn_lottery3', 'hu', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '和' && $codes[0] == $codes[4]) {
                $peilv = get_query_val('fn_lottery3', 'he', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
            continue;
        }
        if ($zym_1 == '前三') {
            if ($zym_8 == '豹子' && $zym_2['豹子'] == true) {
                $peilv = get_query_val('fn_lottery3', 'q_baozi', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '顺子' && $zym_2['顺子'] == true) {
                $peilv = get_query_val('fn_lottery3', 'q_shunzi', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '对子' && $zym_2['对子'] == true) {
                $peilv = get_query_val('fn_lottery3', 'q_duizi', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '半顺' && $zym_2['半顺'] == true) {
                $peilv = get_query_val('fn_lottery3', 'q_banshun', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '杂六' && $zym_2['杂六'] == true) {
                $peilv = get_query_val('fn_lottery3', 'q_zaliu', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
            continue;
        }
        if ($zym_1 == '中三') {
            if ($zym_8 == '豹子' && $zym_3['豹子'] == true) {
                $peilv = get_query_val('fn_lottery3', 'z_baozi', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '顺子' && $zym_3['顺子'] == true) {
                $peilv = get_query_val('fn_lottery3', 'z_shunzi', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '对子' && $zym_3['对子'] == true) {
                $peilv = get_query_val('fn_lottery3', 'z_duizi', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '半顺' && $zym_3['半顺'] == true) {
                $peilv = get_query_val('fn_lottery3', 'z_banshun', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '杂六' && $zym_3['杂六'] == true) {
                $peilv = get_query_val('fn_lottery3', 'z_zaliu', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
            continue;
        }
        if ($zym_1 == '后三') {
            if ($zym_8 == '豹子' && $zym_4['豹子'] == true) {
                $peilv = get_query_val('fn_lottery3', 'h_baozi', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '顺子' && $zym_4['顺子'] == true) {
                $peilv = get_query_val('fn_lottery3', 'h_shunzi', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '对子' && $zym_4['对子'] == true) {
                $peilv = get_query_val('fn_lottery3', 'h_duizi', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '半顺' && $zym_4['半顺'] == true) {
                $peilv = get_query_val('fn_lottery3', 'h_banshun', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '杂六' && $zym_4['杂六'] == true) {
                $peilv = get_query_val('fn_lottery3', 'h_zaliu', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
            continue;
        }
        if ((int)$zym_1 > 0) {
            $count = (int)$zym_1 - 1;
            if ($zym_8 == '大' && (int)$codes[$count] > 4) {
                $peilv = get_query_val('fn_lottery3', 'da', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '小' && (int)$codes[$count] < 5) {
                $peilv = get_query_val('fn_lottery3', 'xiao', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '单' && (int)$codes[$count] % 2 != 0) {
                $peilv = get_query_val('fn_lottery3', 'dan', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == '双' && (int)$codes[$count] % 2 == 0) {
                $peilv = get_query_val('fn_lottery3', 'shuang', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } elseif ($zym_8 == $codes[$count]) {
                $peilv = get_query_val('fn_lottery3', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, $game, $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
            continue;
        }
    }
}

//TODO PK10
function PK10_jiesuan()
{
    $table = 'fn_order';
    select_query($table, '*', array("status" => "未结算"));
    while ($con = db_fetch_array()) {
        $cons[] = $con;
    }
    foreach ($cons as $con) {
        echo $con['id'];
        $id = $con['id'];
        $roomid = $con['roomid'];
        $user = $con['userid'];
        $term = $con['term'];
        $zym_1 = $con['mingci'];
        $zym_8 = $con['content'];
        $zym_7 = $con['money'];
        $opencode = get_query_val('fn_open', 'code', "`term` = '$term' and `type` = '1'");
        $opencode = str_replace('10', '0', $opencode);
        if ($opencode == "") continue;
        $code = explode(',', $opencode);
        if ($zym_1 == '1') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery1', 'da', "`roomid` = '$roomid'");
                if ((int)$code[0] > 5 || $code[0] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery1', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[0] < 6 && $code[0] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery1', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery1', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery1', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0 && (int)$code[0] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery1', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0 && (int)$code[0] > 5 || (int)$code[0] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery1', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0 && (int)$code[0] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery1', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0 && (int)$code[0] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery1', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[0] > (int)$code[9] && $code[9] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[0] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery1', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[0] < (int)$code[9] && $code[0] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[9] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[0]) {
                $peilv = get_query_val('fn_lottery1', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '2') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery1', 'da', "`roomid` = '$roomid'");
                if ((int)$code[1] > 5 || $code[1] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery1', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[1] < 6 && $code[1] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery1', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery1', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery1', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0 && (int)$code[1] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery1', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0 && (int)$code[1] > 5 || (int)$code[1] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery1', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0 && (int)$code[1] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery1', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0 && (int)$code[1] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery1', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[1] > (int)$code[8] && $code[8] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[1] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery1', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[1] < (int)$code[8] && $code[1] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[8] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[1]) {
                $peilv = get_query_val('fn_lottery1', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '3') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery1', 'da', "`roomid` = '$roomid'");
                if ((int)$code[2] > 5 || $code[2] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery1', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[2] < 6 && $code[2] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery1', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery1', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery1', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0 && (int)$code[2] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery1', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0 && (int)$code[2] > 5 || (int)$code[2] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery1', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0 && (int)$code[2] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery1', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0 && (int)$code[2] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery1', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[2] > (int)$code[7] && $code[7] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[2] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery1', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[2] < (int)$code[7] && $code[2] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[7] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[2]) {
                $peilv = get_query_val('fn_lottery1', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '4') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery1', 'da', "`roomid` = '$roomid'");
                if ((int)$code[3] > 5 || $code[3] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery1', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[3] < 6 && $code[3] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery1', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery1', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery1', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0 && (int)$code[3] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery1', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0 && (int)$code[3] > 5 || (int)$code[3] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery1', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0 && (int)$code[3] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery1', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0 && (int)$code[3] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery1', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[3] > (int)$code[6] && $code[6] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[3] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery1', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[3] < (int)$code[6] && $code[3] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[6] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[3]) {
                $peilv = get_query_val('fn_lottery1', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '5') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery1', 'da', "`roomid` = '$roomid'");
                if ((int)$code[4] > 5 || $code[4] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery1', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[4] < 6 && $code[4] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery1', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery1', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery1', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0 && (int)$code[4] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery1', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0 && (int)$code[4] > 5 || (int)$code[4] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery1', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0 && (int)$code[4] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery1', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0 && (int)$code[4] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery1', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[4] > (int)$code[5] && $code[5] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[4] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery1', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[4] < (int)$code[5] && $code[4] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[5] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[4]) {
                $peilv = get_query_val('fn_lottery1', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '6') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery1', 'da', "`roomid` = '$roomid'");
                if ((int)$code[5] > 5 || $code[5] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery1', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[5] < 6 && $code[5] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery1', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery1', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery1', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0 && (int)$code[5] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery1', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0 && (int)$code[5] > 5 || (int)$code[5] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery1', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0 && (int)$code[5] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery1', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0 && (int)$code[5] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[5]) {
                $peilv = get_query_val('fn_lottery1', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '7') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery1', 'da', "`roomid` = '$roomid'");
                if ((int)$code[6] > 5 || $code[6] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery1', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[6] < 6 && $code[6] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery1', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery1', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery1', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0 && (int)$code[6] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery1', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0 && (int)$code[6] > 5 || (int)$code[6] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery1', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0 && (int)$code[6] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery1', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0 && (int)$code[6] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[6]) {
                $peilv = get_query_val('fn_lottery1', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '8') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery1', 'da', "`roomid` = '$roomid'");
                if ((int)$code[7] > 5 || $code[7] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery1', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[7] < 6 && $code[7] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery1', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery1', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery1', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0 && (int)$code[7] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery1', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0 && (int)$code[7] > 5 || (int)$code[7] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery1', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0 && (int)$code[7] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery1', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0 && (int)$code[7] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[7]) {
                $peilv = get_query_val('fn_lottery1', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '9') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery1', 'da', "`roomid` = '$roomid'");
                if ((int)$code[8] > 5 || $code[8] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery1', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[8] < 6 && $code[8] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery1', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery1', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery1', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0 && (int)$code[8] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery1', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0 && (int)$code[8] > 5 || (int)$code[8] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery1', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0 && (int)$code[8] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery1', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0 && (int)$code[8] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[8]) {
                $peilv = get_query_val('fn_lottery1', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '0') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery1', 'da', "`roomid` = '$roomid'");
                if ((int)$code[9] > 5 || $code[9] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery1', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[9] < 6 && $code[9] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery1', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery1', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery1', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0 && (int)$code[9] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery1', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0 && (int)$code[9] > 5 || (int)$code[9] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery1', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0 && (int)$code[9] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery1', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0 && (int)$code[9] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[9]) {
                $peilv = get_query_val('fn_lottery1', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '和') {
            if ($code[0] == "0" || $code[1] == "0") {
                $hz = (int)$code[0] + (int)$code[1] + 10;
            } else {
                $hz = (int)$code[0] + (int)$code[1];
            }
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery1', 'heda', "`roomid` = '$roomid'");
                if ($hz > 11) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery1', 'hexiao', "`roomid` = '$roomid'");
                if ($hz < 12) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery1', 'hedan', "`roomid` = '$roomid'");
                if ($hz % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery1', 'heshuang', "`roomid` = '$roomid'");
                if ($hz % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ((int)$zym_8 == $hz) {
                if ($hz == 3 || $hz == 4 || $hz == 18 || $hz == 19) {
                    $peilv = get_query_val('fn_lottery1', 'he341819', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 5 || $hz == 6 || $hz == 16 || $hz == 17) {
                    $peilv = get_query_val('fn_lottery1', 'he561617', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 7 || $hz == 8 || $hz == 14 || $hz == 15) {
                    $peilv = get_query_val('fn_lottery1', 'he781415', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 9 || $hz == 10 || $hz == 12 || $hz == 13) {
                    $peilv = get_query_val('fn_lottery1', 'he9101213', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 11) {
                    $peilv = get_query_val('fn_lottery1', 'he11', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '北京赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
    }
}

//TODO 极速赛车
function JSSC_jiesuan()
{
    $table = 'fn_jsscorder';
    select_query($table, '*', array("status" => "未结算"));
    while ($con = db_fetch_array()) {
        $cons[] = $con;
    }
    foreach ($cons as $con) {
        //echo $con['id'];
        $id = $con['id'];
        $roomid = $con['roomid'];
        $user = $con['userid'];
        $term = $con['term'];
        $zym_1 = $con['mingci'];
        $zym_8 = $con['content'];
        $zym_7 = $con['money'];
        $opencode = get_query_val('fn_open', 'code', "`term` = '$term' and `type` = '7'");
        $opencode = str_replace('10', '0', $opencode);
        if ($opencode == "") continue;
        $code = explode(',', $opencode);
        if ($zym_1 == '1') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery7', 'da', "`roomid` = '$roomid'");
                if ((int)$code[0] > 5 || $code[0] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery7', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[0] < 6 && $code[0] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery7', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery7', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery7', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0 && (int)$code[0] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery7', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0 && (int)$code[0] > 5 || (int)$code[0] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery7', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 == 0 && (int)$code[0] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery7', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[0] % 2 != 0 && (int)$code[0] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery7', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[0] > (int)$code[9] && $code[9] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[0] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery7', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[0] < (int)$code[9] && $code[0] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[9] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[0]) {
                $peilv = get_query_val('fn_lottery7', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '2') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery7', 'da', "`roomid` = '$roomid'");
                if ((int)$code[1] > 5 || $code[1] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery7', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[1] < 6 && $code[1] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery7', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery7', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery7', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0 && (int)$code[1] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery7', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0 && (int)$code[1] > 5 || (int)$code[1] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery7', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 == 0 && (int)$code[1] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery7', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[1] % 2 != 0 && (int)$code[1] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery7', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[1] > (int)$code[8] && $code[8] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[1] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery7', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[1] < (int)$code[8] && $code[1] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[8] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[1]) {
                $peilv = get_query_val('fn_lottery7', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '3') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery7', 'da', "`roomid` = '$roomid'");
                if ((int)$code[2] > 5 || $code[2] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery7', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[2] < 6 && $code[2] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery7', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery7', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery7', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0 && (int)$code[2] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery7', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0 && (int)$code[2] > 5 || (int)$code[2] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery7', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 == 0 && (int)$code[2] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery7', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[2] % 2 != 0 && (int)$code[2] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery7', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[2] > (int)$code[7] && $code[7] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[2] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery7', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[2] < (int)$code[7] && $code[2] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[7] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[2]) {
                $peilv = get_query_val('fn_lottery7', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '4') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery7', 'da', "`roomid` = '$roomid'");
                if ((int)$code[3] > 5 || $code[3] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery7', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[3] < 6 && $code[3] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery7', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery7', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery7', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0 && (int)$code[3] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery7', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0 && (int)$code[3] > 5 || (int)$code[3] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery7', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 == 0 && (int)$code[3] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery7', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[3] % 2 != 0 && (int)$code[3] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery7', '`long`', "`roomid` = '$roomid'");
                if ((int)$code[3] > (int)$code[6] && $code[6] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[3] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery7', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[3] < (int)$code[6] && $code[3] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[6] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[3]) {
                $peilv = get_query_val('fn_lottery7', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '5') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery7', 'da', "`roomid` = '$roomid'");
                if ((int)$code[4] > 5 || $code[4] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery7', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[4] < 6 && $code[4] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery7', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery7', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery7', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0 && (int)$code[4] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery7', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0 && (int)$code[4] > 5 || (int)$code[4] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery7', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 == 0 && (int)$code[4] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery7', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[4] % 2 != 0 && (int)$code[4] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '龙') {
                $peilv = get_query_val('fn_lottery7', '`long`', "`roomid` = '1'");
                if ((int)$code[4] > (int)$code[5] && $code[5] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[4] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '虎') {
                $peilv = get_query_val('fn_lottery7', 'hu', "`roomid` = '$roomid'");
                if ((int)$code[4] < (int)$code[5] && $code[4] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($code[5] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[4]) {
                $peilv = get_query_val('fn_lottery7', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '6') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery7', 'da', "`roomid` = '$roomid'");
                if ((int)$code[5] > 5 || $code[5] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery7', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[5] < 6 && $code[5] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery7', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery7', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery7', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0 && (int)$code[5] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery7', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0 && (int)$code[5] > 5 || (int)$code[5] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery7', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 == 0 && (int)$code[5] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery7', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[5] % 2 != 0 && (int)$code[5] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[5]) {
                $peilv = get_query_val('fn_lottery7', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '7') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery7', 'da', "`roomid` = '$roomid'");
                if ((int)$code[6] > 5 || $code[6] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery7', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[6] < 6 && $code[6] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery7', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery7', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery7', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0 && (int)$code[6] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery7', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0 && (int)$code[6] > 5 || (int)$code[6] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery7', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 == 0 && (int)$code[6] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery7', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[6] % 2 != 0 && (int)$code[6] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[6]) {
                $peilv = get_query_val('fn_lottery7', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '8') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery7', 'da', "`roomid` = '$roomid'");
                if ((int)$code[7] > 5 || $code[7] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery7', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[7] < 6 && $code[7] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery7', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery7', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery7', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0 && (int)$code[7] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery7', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0 && (int)$code[7] > 5 || (int)$code[7] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery7', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 == 0 && (int)$code[7] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery7', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[7] % 2 != 0 && (int)$code[7] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[7]) {
                $peilv = get_query_val('fn_lottery7', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '9') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery7', 'da', "`roomid` = '$roomid'");
                if ((int)$code[8] > 5 || $code[8] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery7', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[8] < 6 && $code[8] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery7', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery7', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery7', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0 && (int)$code[8] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery7', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0 && (int)$code[8] > 5 || (int)$code[8] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery7', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 == 0 && (int)$code[8] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery7', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[8] % 2 != 0 && (int)$code[8] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[8]) {
                $peilv = get_query_val('fn_lottery7', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '0') {
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery7', 'da', "`roomid` = '$roomid'");
                if ((int)$code[9] > 5 || $code[9] == '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery7', 'xiao', "`roomid` = '$roomid'");
                if ((int)$code[9] < 6 && $code[9] != '0') {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery7', 'dan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery7', 'shuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大单') {
                $peilv = get_query_val('fn_lottery7', 'dadan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0 && (int)$code[9] > 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '大双') {
                $peilv = get_query_val('fn_lottery7', 'dashuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0 && (int)$code[9] > 5 || (int)$code[9] == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小双') {
                $peilv = get_query_val('fn_lottery7', 'xiaoshuang', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 == 0 && (int)$code[9] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小单') {
                $peilv = get_query_val('fn_lottery7', 'xiaodan', "`roomid` = '$roomid'");
                if ((int)$code[9] % 2 != 0 && (int)$code[9] < 5) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == $code[9]) {
                $peilv = get_query_val('fn_lottery7', 'tema', "`roomid` = '$roomid'");
                $zym_11 = $peilv * (int)$zym_7;
                用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
        if ($zym_1 == '和') {
            if ($code[0] == "0" || $code[1] == "0") {
                $hz = (int)$code[0] + (int)$code[1] + 10;
            } else {
                $hz = (int)$code[0] + (int)$code[1];
            }
            if ($zym_8 == '大') {
                $peilv = get_query_val('fn_lottery7', 'heda', "`roomid` = '$roomid'");
                if ($hz > 11) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '小') {
                $peilv = get_query_val('fn_lottery7', 'hexiao', "`roomid` = '$roomid'");
                if ($hz < 12) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '单') {
                $peilv = get_query_val('fn_lottery7', 'hedan', "`roomid` = '$roomid'");
                if ($hz % 2 != 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ($zym_8 == '双') {
                $peilv = get_query_val('fn_lottery7', 'heshuang', "`roomid` = '$roomid'");
                if ($hz % 2 == 0) {
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } else {
                    $zym_11 = '-' . $zym_7;
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } elseif ((int)$zym_8 == $hz) {
                if ($hz == 3 || $hz == 4 || $hz == 18 || $hz == 19) {
                    $peilv = get_query_val('fn_lottery7', 'he341819', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 5 || $hz == 6 || $hz == 16 || $hz == 17) {
                    $peilv = get_query_val('fn_lottery7', 'he561617', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 7 || $hz == 8 || $hz == 14 || $hz == 15) {
                    $peilv = get_query_val('fn_lottery7', 'he781415', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 9 || $hz == 10 || $hz == 12 || $hz == 13) {
                    $peilv = get_query_val('fn_lottery7', 'he9101213', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                } elseif ($hz == 11) {
                    $peilv = get_query_val('fn_lottery7', 'he11', "`roomid` = '$roomid'");
                    $zym_11 = $peilv * (int)$zym_7;
                    用户_上分($user, $zym_11, $roomid, '极速赛车', $term, $zym_1 . '/' . $zym_8 . '/' . $zym_7);
                    update_query($table, array("status" => $zym_11), array('id' => $id));
                    continue;
                }
            } else {
                $zym_11 = '-' . $zym_7;
                update_query($table, array("status" => $zym_11), array('id' => $id));
                continue;
            }
        }
    }
}

/**
 * @param $game (游戏)
 * @param $term (下一期ID)
 */
function kaichat($game, $term)
{
    $haomachuan = '';
    //TODO 结算开奖并入库播报员，前端定时器获取入库数据，并显示
    if ($game == 'bjpk10') {//TODO 北京pk10
        $haoma = get_query_val('fn_open', 'code', "`type` = '1' order by `term` desc limit 1");
        $fenge = explode(",", $haoma);
        foreach ($fenge as $val) {
            $haomachuan .= "<span class='pk_" . (int)$val . "'>" . (int)$val . "</span>";
        }
        select_query('fn_lottery1', '*', array('gameopen' => 'true'));
        while ($con = db_fetch_array()) {
            $cons[] = $con;
        }
        foreach ($cons as $con) {

            $kjqh = get_query_val('fn_open', 'term', "`type` = '1' order by `term` desc limit 1");
            管理员喊话("第 $kjqh 期&nbsp;开&nbsp;奖&nbsp;号&nbsp;码<br><br>$haomachuan<br><br> $term 期已经开启,请下注!", $con['roomid'], 'pk10');
            //TODO 发奖操作(fn_chat表)
            管理员提示开奖($con['roomid'], 'pk10', $kjqh);
            echo "bjpk10喊话-" . $con['roomid'] . '..<br>';
        }
    } elseif ($game == 'xyft') {//TODO 幸运飞艇
        $haoma = get_query_val('fn_open', 'code', "`type` = '2' order by `term` desc limit 1");
        $fenge = explode(",", $haoma);
        $haomachuan = '';
        foreach ($fenge as $val) {
            $haomachuan .= "<span class='pk_" . (int)$val . "'>" . (int)$val . "</span>";
        }
        select_query('fn_lottery2', '*', array('gameopen' => 'true'));
        while ($con = db_fetch_array()) {
            $cons[] = $con;
        }
        foreach ($cons as $con) {

            $kjqh = get_query_val('fn_open', 'term', "`type` = '2' order by `term` desc limit 1");
            管理员喊话("第 $kjqh 期&nbsp;开&nbsp;奖&nbsp;号&nbsp;码<br><br>$haomachuan<br><br> $term 期已经开启,请下注!", $con['roomid'], 'xyft');
            //TODO 发奖操作(fn_chat表)
            管理员提示开奖($con['roomid'], 'xyft', $kjqh);
            echo "mlaft喊话-" . $con['roomid'] . '..<br>';
        }
    } elseif ($game == 'cqssc') {//TODO 澳洲幸运5
        $haoma = get_query_val('fn_open', 'code', "`type` = '3' order by `term` desc limit 1");
        $fenge = explode(",", $haoma);
        foreach ($fenge as $val) {
            $haomachuan .= "<span class='pk_" . (int)$val . "'>" . (int)$val . "</span>";
        }
        select_query('fn_lottery3', '*', array('gameopen' => 'true'));
        while ($con = db_fetch_array()) {
            $cons[] = $con;
        }
        foreach ($cons as $con) {

            $kjqh = get_query_val('fn_open', 'term', "`type` = '3' order by `term` desc limit 1");
            管理员喊话("第 $kjqh 期&nbsp;开&nbsp;奖&nbsp;号&nbsp;码<br><br>$haomachuan<br><br> $term 期已经开启,请下注!", $con['roomid'], 'cqssc');
            //TODO 发奖操作(fn_chat表)
            管理员提示开奖($con['roomid'], 'cqssc', $kjqh);
            echo "mlaft喊话-" . $con['roomid'] . '..<br>';
        }
    } elseif ($game == 'jssc') {//TODO 极速赛车
        $haoma = get_query_val('fn_open', 'code', "`type` = '7' order by `term` desc limit 1");
        $fenge = explode(",", $haoma);
        foreach ($fenge as $val) {
            $haomachuan .= "<span class='pk_" . (int)$val . "'>" . (int)$val . "</span>";
        }
        select_query('fn_lottery7', '*', array('gameopen' => 'true'));
        while ($con = db_fetch_array()) {
            $cons[] = $con;
        }
        foreach ($cons as $con) {

            $kjqh = get_query_val('fn_open', 'term', "`type` = '7' order by `term` desc limit 1");
            管理员喊话("第 $kjqh 期&nbsp;开&nbsp;奖&nbsp;号&nbsp;码<br><br>$haomachuan<br><br> $term 期已经开启,请下注!", $con['roomid'], 'jssc');
            //TODO 发奖操作(fn_chat表)
            管理员提示开奖($con['roomid'], 'jssc', $kjqh);
            echo "jssc喊话-" . $con['roomid'] . '..<br>';
        }
    } elseif ($game == 'jssm') {//TODO 澳洲幸运10
        $haoma = get_query_val('fn_open', 'code', "`type` = '12' order by `term` desc limit 1");
        $fenge = explode(",", $haoma);
        foreach ($fenge as $val) {
            $haomachuan .= "<span class='pk_" . (int)$val . "'>" . (int)$val . "</span>";
        }

        select_query('fn_lottery12', '*', array('gameopen' => 'true'));
        while ($con = db_fetch_array()) {
            $cons[] = $con;
        }
        foreach ($cons as $con) {

            $kjqh = get_query_val('fn_open', 'term', "`type` = '12' order by `term` desc limit 1");
            管理员喊话("第 $kjqh 期&nbsp;开&nbsp;奖&nbsp;号&nbsp;码<br><br>$haomachuan<br><br> $term 期已经开启,请下注!", $con['roomid'], 'jssm');
            //TODO 发奖操作(fn_chat表)
            管理员提示开奖($con['roomid'], 'jssm', $kjqh);
            echo "jssm喊话-" . $con['roomid'] . '..<br>';
        }
    }
    //TODO 发奖操作(fn_chat表)
//    管理员提示开奖($con['roomid'], $game, $kjqh);

}

function 管理员喊话($Content, $roomid, $game)
{
    $headimg = get_query_val('fn_setting', 'setting_robotsimg', array('roomid' => $roomid));
    insert_query("fn_chat", array("username" => "播报员", "headimg" => $headimg, 'content' => $Content, 'game' => $game, 'addtime' => date('H:i:s'), 'type' => 'S3', 'userid' => 'system', 'roomid' => $roomid));
}

function 导入配置($key, $roomid, $game)
{
    global $keyword;//游戏名
    //todo 管理员喊话
    $info = (new ShiroiDb())->table('fn_config')->where("`key` like '$key%'")->where(['status'=>1,'game'=>$game])->get();
    if($info) {
        preg_match_all("/\[(.*?)\]/is",$info['value'],$allReplaceValue);
        $allReplaceValue = next($allReplaceValue);
        foreach ($allReplaceValue as $k=>$v) {
            $info['value'] = str_replace('['.$v.']',$keyword[$v],$info['value']);
        }
        管理员喊话($info['value'], $roomid, $game);
    }
}

function 文本_逐字分割($str, $split_len = 1)
{
    if (!preg_match('/^[0-9]+$/', $split_len) || $split_len < 1) return FALSE;
    $len = mb_strlen($str, 'UTF-8');
    if ($len <= $split_len) return array($str);
    preg_match_all("/.{" . $split_len . '}|[^x00]{1,' . $split_len . '}$/us', $str, $ar);
    return $ar[0];
}

function 管理员提示开奖($roomid, $game, $team)
{
    echo $game;
    global $keyword;//游戏名
    $str = '';
    switch ($game) {
        case "jssc":
            $table = "fn_jsscorder";
            break;
        case "cqssc":
            $table = "fn_sscorder";
            break;
        case "pk10":
            $table = "fn_order";
            break;
        case "xyft":
            $table = "fn_flyorder";
            break;
        case "jssm":
            $table = "fn_smorder";
            break;

    }
    $all_user_order = lottery_result($game,$team);
    $keyword['期号'] = $team;
    if(!empty($all_user_order)){//当前参与用户提示
        $str .= $all_user_order;
        $keyword['中奖'] = $str;
        导入配置('奖后消息1',$roomid,$game);
    }else{//当前未参与用户提示
        导入配置('奖后消息4',$roomid,$game);
    }
//    insert_query("fn_chat", array("username" => "播报员", "headimg" => $headimg, 'content' => $str, 'game' => $game, 'addtime' => date('H:i:s'), 'type' => 'S3', 'userid' => 'system', 'roomid' => $roomid));
}

//开奖结果
function lottery_result($game,$team)
{
    $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
    $httpHost =  $http_type . $_SERVER['SERVER_NAME'] . "/Public/ShiroiInterface.php?f=getLotteryResult&term={$team}&game={$game}";
    $json = json_decode(file_get_contents($httpHost),true);
    return isset($json['data'])?$json['data']:'';
}
?>