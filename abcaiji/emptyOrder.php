<?php
//清空所有账户的流水
include_once('../Public/ShiroiDb.php');
include_once('../Public/ShiroiRedis.php');
//清空订单
$thisDay = date('Y-m-d 00:00:00');
$nextDay = date('Y-m-d 00:00:00',strtotime("+1 day"));
$time = date('Y-m-d H:i:s');
$emptyTime = date('Y-m-d 06:00:00');//清空时间-开始
$emptyTime1 = date('Y-m-d 06:30:00');//清空时间-结束
$expire = strtotime($nextDay) - strtotime($time);//获取redis失效期
$redisKey = date('Y-m-d').'_emptyOrder';
$redis = (new ShiroiRedis())->get($redisKey);
if(!$redis){
    if(strtotime($time)>strtotime($emptyTime) && strtotime($time)<strtotime($emptyTime1)) {
        (new ShiroiRedis())->setex($redisKey,$expire,$redisKey);
        (new ShiroiDb())->query('truncate fn_jsscorder');//极速赛车订单表
        (new ShiroiDb())->query('truncate fn_smorder');//澳洲幸运10订单表
        (new ShiroiDb())->query('truncate fn_res_open');//账户记录表
        (new ShiroiDb())->query('truncate fn_open');//账户记录表
        (new ShiroiDb())->query('truncate fn_sopen');//账户记录表
        (new ShiroiDb())->query('truncate fn_marklog');//反水表
        (new ShiroiDb())->query('truncate fn_chat');//聊天表
    }
}