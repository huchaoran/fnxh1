<link rel="stylesheet" href="plugins/iCheck/all.css">
<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link href="plugins/room/css/bootstrap.min.css" rel="stylesheet">
<link href="plugins/room/css/layui.css" rel="stylesheet"/>
<link href="plugins/room/css/main.css" rel="stylesheet"/>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            北京赛车
            <small>游戏设置</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 仪表盘</a></li>
            <li><a href="#"> 游戏设置</a></li>
            <li class="active">北京赛车</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content" style="background-color: #f3f3f4;">
        <div style="width: 100%;display: flex">
            <!--聊天-->
            <div class="chatArea">
                <!--标签-->
                <div class="chat_label">
                    <!--切换-->
                    <div class="chat_label_t">
                        <p class="chat_label_active" onclick="changeChatLabel(0)">游戏</p>
                        <p onclick="changeChatLabel(1)" id="label_service">客服<span id="label_service_warn"></span></p>
                    </div>
                    <!--列表-->
                    <div class="chat_label_b">
                        <div class="chat_label_room chat_active">
                            <div class="chat_label_active" onclick="changeChatGame('8')" index="8" id="gameChatLabel_8">
                                <div>
                                    <img src="http://img.1e7q.com/201904022035537274591"
                                         style="width: 100%;height: 100%"/>
                                </div>
                                <p>极速赛车</p>
                            </div>
                        </div>
                        <div class="chat_label_service">

                        </div>
                    </div>
                </div>
                <!--内容-->
                <div class="chat_chat_content" style="background-color: #F5F5F5;width: 550px">
                    <!--游戏-->
                    <div class="chat_active">
                        <div class="chat_div chat_div_game">
                            <div id="gameChatContent_8">

                            </div>
                        </div>
                        <!--输入框-->
                        <div class="chat_input">
                            <textarea id="email_content" class="chat_textarea chat_textarea_game"></textarea>
                            <div class="chatSureDiv">
                                <button type="button" onclick="sendChat();" class="chatSure">发 送</button>
                            </div>
                        </div>
                    </div>
                    <!--客服-->
                    <div>
                        <div class="chat_div chat_div_service">
                            <!--聊天内容-->
                        </div>
                        <!--输入框-->
                        <div class="chat_input">
                            <textarea id="service_content" class="chat_textarea chat_textarea_service"></textarea>
                            <div class="chatSureDiv">
                                <button type="button" onclick="sendServiceChat();" class="chatSure">发 送</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--统计-->
            <div class="total">
                <div class="dataStatis">
                    <!--在线人数-->
                    <div id="countPeopleDiv">
                        <p style="margin-bottom: 20px;font-size: 20px">在线人数：<span id="countPeople">0</span></p>
                    </div>
                </div>
                <div class="dataStatis">
                    <!--用户余额-->
                    <div id="userMoneyDiv">
                        <div class="">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">全部</span>
                                <h5>用户余额</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 id="userMoney"></h1>
                                <div class="text-primary pull-right"><i class="fa fa-bolt"></i>
                                </div>
                                <small>积分</small>
                            </div>
                        </div>
                    </div>
                    <!--今日流水-->
                    <div id="moneyDiv">
                        <div class="">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">全部</span>
                                <h5>今日流水</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 id="money"></h1>
                                <div class="text-success pull-right"><i class="fa fa-bolt"></i>
                                </div>
                                <small>积分</small>
                            </div>
                        </div>
                    </div>
                    <!--今日盈亏-->
                    <div id="profitDiv">
                        <div class="">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">全部</span>
                                <h5>今日盈亏</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 id="profit"></h1>
                                <div class="text-info pull-right"><i class="fa fa-bolt"></i>
                                </div>
                                <small>积分</small>
                            </div>
                        </div>
                    </div>
                    <!--今日回水-->
                    <div id="backDiv">
                        <div class="">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">全部</span>
                                <h5>今日回水</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 id="back"></h1>
                                <div class="text-info pull-right"><i class="fa fa-bolt"></i>
                                </div>
                                <small>积分</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-wrapper">
                    <div style="margin-top: 50px">
                        <table class="fl-table">
                            <thead>
                            <tr id="game_8">
                                <th style="background-color: #c9c6c6">极速赛车</th>
                                <th style="background-color: #c9c6c6"><span id="game_8_nowNumber">31579174</span>期</th>
                                <th style="background-color: #c9c6c6">本期流水：<span id="game_8_nowMoney">0</span></th>
                                <th style="background-color: #c9c6c6">今日流水：<span id="game_8_todayMoney">0</span></th>
                                <th style="background-color: #c9c6c6">今日盈亏：<span id="game_8_todayProfit">0</span></th>
                                <th style="background-color: #c9c6c6;color: red;font-size: 16px" id="game_8_time"></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div>
                        <table class="fl-table">
                            <thead>
                            <tr>
                                <th>会员编号</th>
                                <th>用户名</th>
                                <th>昵称</th>
                                <th>余额</th>
                                <th>上期盈亏</th>
                                <th>竞猜总分</th>
                                <th>竞猜详情</th>
                            </tr>
                            </thead>
                            <tbody id="game_8_member">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
        </div>

        <div style="height: 200px"></div>
    </section>
    <!-- /.content -->
</div>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip({
            container: 'body',
            html: true,
        });
        $("li.dropdown").hover(function () {
            $(this).addClass("open");
        }, function () {
            $(this).removeClass("open");
        });
    });

    //更换聊天标签
    function changeChatLabel(index) {
        $('.chat_label_t').children('p').removeClass('chat_label_active');
        $('.chat_label_t').children('p').eq(index).addClass('chat_label_active');
        $('.chat_label_b').children('div').removeClass('chat_active');
        $('.chat_label_b').children('div').eq(index).addClass('chat_active');
        $('.chat_chat_content').children('div').removeClass('chat_active');
        $('.chat_chat_content').children('div').eq(index).addClass('chat_active');
        if (index == 1) {
            $('#label_service_warn').removeClass('label_service_warn');
            var chat_div = $('.chat_chat_content').children('div').eq(index).children('div').eq(0).children('div').eq(0);
            if (chat_div.length > 0) {
                chat_div.scrollTop(chat_div[0].scrollHeight);
            }
        }
    }


</script>