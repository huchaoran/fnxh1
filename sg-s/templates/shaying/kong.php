<html>
<head>
    <meta charset="utf-8"/>
    <title>在线控制器</title>
    <style>
        .caaa{
            float: left;
            width:200px;
            height:200px;
            padding: 0;
            margin: 0;
        }
        #timeinfo1,#timeinfo2 {
            font-size: 14px;
            color: #C60
        }
    </style>
</head>
<body>
    <div class="caaa">
        <iframe id="api_jssm" src="api_jssm.php" style="width:200px;height:65px;" frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="no" allowtransparency="yes"></iframe>
        <input type=button name=button value="刷新" onClick="document.getElementById('api_jssm').contentWindow.location.reload(true)"> <span id="timeinfo1" href="javascript:;"></span>
    </div>
    <div class="caaa">
        <iframe id="api_jssc" src="api_jssc.php" style="width:200px;height:65px;" frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="no" allowtransparency="yes"></iframe>
        <input type=button name=button value="刷新" onClick="document.getElementById('api_jssc').contentWindow.location.reload(true)"> <span id="timeinfo2" href="javascript:;"></span>
    </div>
</body>
</html>
<script type="text/javascript">
    var jssmtime = 3,jssctime = 3,time1,time2;
    var timeinfo1 = document.getElementById('timeinfo1');
    var timeinfo2 = document.getElementById('timeinfo2');
    var chatIframeTimer1,chatIframeTimer2;

    //澳洲幸运10的定时器
    function timeInfo1SetTime() {
        if(time1>0){
            timeinfo1.innerText = time1 + "秒后自动获取!"
            time1--;
        }else{
            sleep(function () {//睡眠操作
                time1 = jssmtime;
                //刷新iframe
                document.getElementById('api_jssm').contentWindow.location.reload(true);
                chatIframeStop1();
            },0);

        }
    }
    //清除定时器
    function chatIframeStop1() {
        if (chatIframeTimer1) {
            window.clearTimeout(chatIframeTimer1);
        }
    }

    //极速赛车的定时器
    function timeInfo2SetTime() {
        if(time2>0){
            timeinfo2.innerText = time2 + "秒后自动获取!"
            time2--;
        }else{
            sleep(function () {//睡眠操作
                time2 = jssctime;
                //刷新iframe
                document.getElementById('api_jssc').contentWindow.location.reload(true);
                chatIframeStop2();
            },0);
        }
    }
    //清除定时器
    function chatIframeStop2() {
        if (chatIframeTimer2) {
            window.clearTimeout(chatIframeTimer2);
        }
    }

    //页面渲染完成执行操作
    window.onload=function(){
        //澳洲幸运10的定时器
        chatIframeTimer1 = setInterval(function () {
            setInterval(timeInfo1SetTime, 1000);
        }, jssmtime * 1000);//1000为1秒钟
        //极速赛车的定时器
        chatIframeTimer2 = setInterval(function () {
            setInterval(timeInfo2SetTime, 1000);
        }, jssctime * 1000);//1000为1秒钟
    }

    //睡眠操作
    function sleep(callback, time) {
        if (typeof callback == "function") {
            setTimeout(callback, time);
        }
    }
</script>