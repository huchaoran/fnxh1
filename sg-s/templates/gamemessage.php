<link rel="stylesheet" href="plugins/iCheck/all.css">
<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<style>
    .message {
        color: #006fc4;
    }

    .messageListHide {
        display: none;
    }
    .hand{
        cursor:pointer;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            北京赛车
            <small>游戏设置</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 仪表盘</a></li>
            <li><a href="#"> 游戏设置</a></li>
            <li class="active">北京赛车</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <ul class="nav nav-tabs">
            <li class="hrefMessage active"><a>操作消息</a></li>
            <li class="hrefMessage message"><a href="javascript:;">奖前消息</a></li>
            <li class="hrefMessage message"><a href="javascript:;">奖后消息</a></li>
        </ul>
        <div class="messageList">

            <div class="wrap js-check-wrap">
                <div style="margin-top: 20px;width: 700px;margin-left: 100px;height: 450px;overflow: auto">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th width="20"></th>
                            <th width="120">消息名称</th>
                            <th>消息内容</th>
                            <th>状态</th>
                        </tr>
                        </thead>
                        <tbody id="caozuoxiaoxihtml"></tbody>
                        <script id="caozuoxiaoxi" type="text/html">
                            <%  for(let i in data) {%>
                                <tr id="html_<%=data[i].id%>">
                                    <th>
                                        <label style="display: flex;align-items: center">
                                            <input value="<%=data[i].id%>" onclick="getLabel('<%=data[i].id%>')" name="radio" type="radio" style="width: 15px;height: 15px"/>
                                        </label>
                                    </th>
                                    <td><%=data[i].key%></td>
                                    <td>
                                <textarea class="form-control" style="width: 400px;height: 100px" disabled><%=data[i].value%></textarea>
                                    </td>
                                    <td>
                                        <%if(data[i].status==1){%>
                                            <span class="label label-primary hand" onclick="statusChange('<%=data[i].id%>')">开启</span>
                                        <%}else{%>
                                            <span class="label label-warning hand" onclick="statusChange('<%=data[i].id%>')">关闭</span>
                                        <%}%>
                                    </td>
                                </tr>
                            <%}%>
                        </script>
                    </table>
                </div>
            </div>

            <div class="wrap js-check-wrap messageListHide">
                <div style="margin-top: 20px;width: 700px;margin-left: 100px;height: 450px;overflow: auto">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="20"></th>
                                <th width="120">消息名称</th>
                                <th>消息内容</th>
                                <th>状态</th>
                            </tr>
                        </thead>
                        <tbody id="jiangqianxiaoxihtml"></tbody>
                        <script id="jiangqianxiaoxi" type="text/html">
                            <%  for(let i in data) {%>
                                <tr id="html_<%=data[i].id%>">
                                    <th>
                                        <label style="display: flex;align-items: center">
                                            <input value="<%=data[i].id%>" onclick="getLabel('<%=data[i].id%>')" name="radio" type="radio" style="width: 15px;height: 15px"/>
                                        </label>
                                    </th>
                                    <td><%=data[i].key%></td>
                                    <td>
                                        <textarea class="form-control" style="width: 400px;height: 100px" disabled><%=data[i].value%></textarea>
                                    </td>
                                    <td>
                                        <%if(data[i].status==1){%>
                                        <span class="label label-primary hand" onclick="statusChange('<%=data[i].id%>')">开启</span>
                                        <%}else{%>
                                        <span class="label label-warning hand" onclick="statusChange('<%=data[i].id%>')">关闭</span>
                                        <%}%>
                                    </td>
                                </tr>
                            <%}%>
                        </script>
                    </table>
                </div>
            </div>


            <div class="wrap js-check-wrap messageListHide">
                <div style="margin-top: 20px;width: 700px;margin-left: 100px;height: 450px;overflow: auto">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th width="20"></th>
                            <th width="120">消息名称</th>
                            <th>消息内容</th>
                            <th>状态</th>
                        </tr>
                        </thead>
                        <tbody id="jianghouxiaoxihtml"></tbody>
                        <script id="jianghouxiaoxi" type="text/html">
                            <%  for(let i in data) {%>
                                <tr>
                                    <th>
                                        <label style="display: flex;align-items: center">
                                            <input value="<%=data[i].id%>" onclick="getLabel('<%=data[i].id%>')" name="radio" type="radio" style="width: 15px;height: 15px"/>
                                        </label>
                                    </th>
                                    <td><%=data[i].key%></td>
                                    <td>
                                        <textarea class="form-control" style="width: 400px;height: 100px" disabled><%=data[i].value%></textarea>
                                    </td>
                                    <td>
                                        <%if(data[i].status==1){%>
                                        <span class="label label-primary hand" onclick="statusChange('<%=data[i].id%>')">开启</span>
                                        <%}else{%>
                                        <span class="label label-warning hand" onclick="statusChange('<%=data[i].id%>')">关闭</span>
                                        <%}%>
                                    </td>
                                </tr>
                            <%}%>
                        </script>
                    </table>
                </div>
            </div>
        </div>
        <div style="margin-top: 50px;margin-left: 100px;">
            <div style="display: flex;align-items: center">
                消息名称：<input type="text" class="form-control" id="name" style="width: 200px" disabled>
            </div>
            <div style="display: flex;margin-top: 20px">
                <div style="display: flex;align-items: center;">
                    消息内容：<textarea class="form-control" id="content"
                                   style="width: 400px;height: 100px"></textarea>
                </div>
            </div>
            <div style="display: flex;align-items: center;margin-top: 20px">
                开启消息：<input type="checkbox" id="status"/>
            </div>
            <div style="display: flex;margin-top: 20px">
                <input type="button" class="btn btn-primary" id="sure" style="width: 100px" value="确认修改"/>
                <input type="button" class="btn btn-danger" id="restore" style="width: 100px;margin-left: 20px"
                       value="还原默认"/>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<script src="plugins/iCheck/icheck.min.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
<script src="plugins/bootstrap-wysihtml5/locale/zh-CN.js"></script>
<script src="plugins/template/template.js"></script>
<script>
    //选项卡
    $('.nav-tabs .hrefMessage').on('click', function () {
        var index = $(".nav-tabs .hrefMessage").index(this);
        $(this).addClass('active').removeAttr("message");
        $(this).siblings().removeClass("active").addClass('message');
        $('.messageList .js-check-wrap').eq(index).removeClass('messageListHide').siblings().addClass('messageListHide');
    });

    //ajax请求数据
    function ajaxRequest(url, type = 'GET', data = {}, datatype = "json") {
        return new Promise((back, erro) => {
            $.ajax({
                type: type,
                url: url,
                dataType: datatype,
                cache: false,
                async: false,
                data: data,
                success: function (data) {
                    back(data);
                },
                error: function (data) {
                    erro(data)
                }
            });
        })
    }

    //定义类型
    let data = {
        f: 'chat_setting_list',
        game: "<?=$_GET['g']?>"
    };
    var alldata = {};
    var init = {};//默认空设置
    //调用接口
    ajaxRequest('/Public/ShiroiBackend.php', 'GET', data).then((result) => {
        //数据渲染
        let {data, message, status} = result;
        alldata = data.操作消息.concat(data.奖前消息,data.奖后消息);
        if (status == 200) {
            $('#caozuoxiaoxihtml').html(template($('#caozuoxiaoxi').html(),{
                data:data.操作消息
            }));
            $('#jiangqianxiaoxihtml').html(template($('#jiangqianxiaoxi').html(),{
                data:data.奖前消息
            }));
            $('#jianghouxiaoxihtml').html(template($('#jianghouxiaoxi').html(),{
                data:data.奖后消息
            }));
        }
    });

    //选择video事件
    function getLabel(e){
        for (let i in alldata) {
            if(parseInt(alldata[i].id)==e){
                init = alldata[i];
                $('#name').val(alldata[i].key);
                $('#content').val(alldata[i].value);
                if(alldata[i].status==1){
                    $("#status").prop("checked", true);
                }else{
                    $("#status").prop("checked", false);
                }
            }
        }
    }

    //确认修改
    $('#sure').on('click',function () {
        let array = {
            status:$('#status').is(':checked')?1:2,
            value:$('#content').val()
        };
        ajaxRequest('/Public/ShiroiBackend.php?f=modifyConfig&id='+init.id, 'POST', array).then((result)=>{
            window.location.reload();
        });
    });

    //修改状态
    function statusChange(e){
        let son;
        for(let i in alldata) if(alldata[i].id==e) son = alldata[i];
        let status = son.status==1?2:1;
        ajaxRequest('/Public/ShiroiBackend.php?f=modifyConfig&id='+son.id, 'POST', {status:status}).then((result)=>{
            window.location.reload();
        });
    }

    //还原默认
    $('#restore').on('click',function () {
        $('#content').val(init.beforevalue);
    });
</script>