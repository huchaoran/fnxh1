<?php
//include_once("/Public/config.php");
include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
if($_POST){
	if(empty($_POST['user'])||empty($_POST['pass'])){
		$json['status']=2;
		$json['msg']='请输入完整信息！';
		echo json_encode($json);exit;		
	}
  
   $userinfo =select_query("fn_user", '*', array('user'=>$_POST['user']));
  
	$userinfo = db_fetch_array();
  
	$time = $_GET['time'] == "" ? 1 : (int)$_GET['time'];
	$user = $_POST['user'];
	$pass = $_POST['pass'];
	$pass1 = $_POST['pass1'];
	$headimgurl = $_POST['headimgurl'];
	$agent = $_COOKIE['agent'] ?$_COOKIE['agent']:'null';
	// $agent = $_POST['pass1'] ?$_POST['pass1']:'null';
	
	// $agent = 'null';
	$userid = md5(uniqid());
	 $_SESSION['roomid'] =  $_SESSION['roomid']? $_SESSION['roomid']:1;
  
	if(!$userinfo){
		
		if(get_query_val("fn_user", "user", array("user"=>$user))){
			$json['status']=2;
			$json['msg']='用户名已存在！';
			echo json_encode($json);exit;
		}
		if(insert_query("fn_user",array('user' => $user,'pass' => $pass,'roomid' => $_SESSION['roomid'],'userid'=>$userid,'headimg'=>$headimgurl,'username'=>$user,'agent'=>$agent,'isagent'=>'false','statustime'=>$time,'jia'=>'false'))){
			$json['status']=1;
			$json['msg']=$_SESSION['roomid'];
			// $url = $_GET['url']; 

			//Header("Location:/"); 
			 echo json_encode($json);exit;	
		}else{
			$json['status']=2;
			$json['msg']='注册失败，请联系管理员！';
			echo json_encode($json);exit;	
		}
	}else{
		$json['status']=2;
		$json['msg']='网络繁忙！';
		echo  json_encode($json);exit;
		//echo  "<b style='color:#fff'>{$json['msg']}</b>";
		
	}


	
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php echo $sitename ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" />
		<!-- <link rel="Stylesheet" type="text/css" href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" /> -->
		<link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="/NewUI/css/register.css" />
	</head>
	<body>
		
		<div id="register">
			<!-- <template> -->
				<div class="register_header">
					<a href="javascript:history.go(-1)">
					    <img  src="/NewUI/images/chat/ic_back.png"/>
					</a>
					<span >注册</span>
				</div>
			<!-- </template> -->
			    
			    <div class="form">
					<form id="formID" >
					<!-- <div   @click="loginEvent">登录</div> -->
					  <div  style="display:inline-block;position:relative;width: 100%;">
					    <div  style="position:absolute;right:2px;top:21%;width:10px;height: 10px; margin-top:-7px ; cursor:pointer;display:none;" class="input_clear">
					    		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					    					×
					    		</button>
					     </div>
					    <input  type="text" data-pure-clear-button class="form-control_form" id="usernameID" placeholder="请输入账号" >
					  </div>
					  <div  style="display:inline-block;position:relative;width: 100%;">
						   <div  style="position:absolute;right:2px;top:21%;width:10px;height: 10px; margin-top:-7px ; cursor:pointer;display:none;" class="input_clear">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
											×
								</button>
						   </div>
					       <input type="password" class="form-control_form" id="passwordID"  placeholder="请输入密码">
					  
					  </div>
					 
					  <div  style="display:inline-block;position:relative;width: 100%;">  
					  <div  style="position:absolute;right:2px;top:21%;width:10px;height: 10px; margin-top:-7px ; cursor:pointer;display:none;" class="input_clear">
					  								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					  											×
					  								</button>
					  </div>
					    <input type="password" class="form-control_form" id="conpasswordID"  placeholder="请确认密码">
					  </div>
					 
					  <button type="submit"  class="btn form-conteol_button btn-lg btn-block" @click="loginEvent">登录</button>
					</form>
					
				</div>
				
				
		</div>
	</body>
	
	<script src="/Style/Old/js/hotcss.js"></script>
	<!-- <script src="/Templates/home/res/jquery.min.js?v=2.1.4"></script> -->
	<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js"></script>
	<!-- <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script> -->
	<!-- <script src="//cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
	
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="/Templates/home/res/ajaxForm.js"></script>
	<script src="/Public/vedor/vant/lib/vant.min.js"></script>
	<!-- <script src="/Templates/home/res/layer.js"></script> -->
	
	<script src="https://cdn.bootcdn.net/ajax/libs/layer/1.8.5/layer.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("input").focus(function(){
			    $(this).parent().children(".input_clear").show();  
			});  
			$("input").blur(function(){  
				if($(this).val()=='')  
				{  
					$(this).parent().children(".input_clear").hide();  
				}  
			});  
			$(".input_clear").click(function(){  
				$(this).parent().find('input').val('');  
				$(this).hide();  
			}); 
		})
		$(function(){
			$('#formID').ajaxForm({
				beforeSubmit: checkForm,
				success: complete, 
				dataType: 'json'
			});
			function CheckStr(str){
			   var myReg = "[@/'\"#!$%&^*]+";
			     var reg = new RegExp(myReg);
			    if(reg.test(str)){
			    	return true; 
				} else {
					 return false; 
				}
			}
			function checkForm(){
				if( '' == $.trim($('#usernameID').val())){
					// layer.alert('用户名不能为空', {icon: 5}, function(index){
					// layer.close(index);
					alert('用户名不能为空')
					$('#usernameID').focus(); 
					// });
					return false;
				}
				// console.log(CheckStr($('#username').val()));
				if (CheckStr($('#usernameID').val())) {
					// layer.alert('用户名不能有特殊字符', {icon: 5}, function(index){
					// layer.close(index);
					alert('用户名不能有特殊字符')
					$('#usernameID').focus(); 
					// });
					return false;
				}
			
				if ($('#passwordID').val().length < 6 && $('#passwordID').val() !='') {
					console.log("密码长度",typeof($('#passwordID').val()) )
					// layer.alert('密码长度不能低于6位', {icon: 5}, function(index){
					// layer.close(index);
					alert('密码长度不能低于6位')
					$('#passwordID').focus(); 
					// });
					return false;
				}
			
				if( '' == $.trim($('#passwordID').val())){
					// layer.alert('密码不能为空', {icon: 5}, function(index){
					// layer.close(index);
					alert('密码不能为空')
					$('#passwordID').focus(); 
					// });
					return false;
				}
				if( '' == $.trim($('#conpasswordID').val())){
					// layer.alert('密码不能为空', {icon: 5}, function(index){
					// layer.close(index);
					alert('确认密码不能为空')
					$('#conpasswordID').focus(); 
					// });
					return false;
				}
			
				if ($('#passwordID').val() != $('#conpasswordID').val()) {
					// layer.alert('两次输入的密码不一致', {icon: 5}, function(index){
					// layer.close(index);
					alert('两次输入的密码不一致')
					$('#passwordID').focus(); 
					// });
					return false;
				}
			}
			function complete(data){
				if(data.status==1){
					$('.btn').attr('disabled','disabled');
					layer.msg(data.msg, function(index){
						layer.close(index);
						window.location.href=data.url;
					});
				}else{
					layer.msg(data.msg);
					$('#password').val('').focus();
					return false;	
				}
			}
			
		})
			
	</script> 
	<script type="module">
		
		var app = new Vue({
			el:'#register',
			data:{
				username: '',
				password: '',
				confirmpassword:''
			},
			methods:{
				// onSubmit(values) {
				// 	  console.log('submit', values);
				// },
				getfocus(e){
					// console.log('----方法中获取光标------')
					console.log('-----获取焦点-------', e.currentTarget.parentElement.querySelector('.input_clear'))
					let input = e.currentTarget.parentElement.querySelector('.input_clear')
					input.style.display = 'block'
				},
				loginEvent(){
					
					var user =$('#usernameID').val() 
					var password = $('#passwordID').val()
					var repassword = $('#conpasswordID').val()
					 var headimgurl ="/Templates/home/res/face/3.png"
					console.log('------登录操作--------',user,password,repassword)
					$.ajax({
					          type:'post',
					          url:"/Templates/home/register.php",
					          data:'user='+user+'&pass='+password+'&pass1='+repassword+'&headimgurl='+headimgurl,
					          dataType:'json',
					          success:function(data){
					            //var data =eval("+data+");
					            // alert(data);
					              if(data.status==1){
					                alert('注册成功！');	
														//location.href='/?room='+data.msg;	
					                location.href='/Templates/home/login.php';
									return false
					          	 }
								 alert(data.msg)
								 // console.log('-------------',data)
					          },
							  error:function(error){
								  console.log('error',error)
							  }
					});
					
					
				}
				
			}
			
		})
	</script>

</html>