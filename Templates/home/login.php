<?php
include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
if($_POST){
	if(empty($_POST['user'])||empty($_POST['pass'])){
		$json['status']=2;
		$json['msg']='请输入完整信息！';
		echo json_encode($json);exit;		
	}
	select_query("fn_user", '*', array('user'=>$_POST['user'], 'pass'=>$_POST['pass']));
	$userinfo = db_fetch_array();
  //var_dump($userinfo);exit;
    //用户账号密码正确
    if($userinfo){
	    //安全码判断
        $security_num = get_query_val('fn_setting','security_num',array(
            'roomid'=>$userinfo['roomid']
        ));
        if(trim($security_num) == $_POST['security_num']){
            $_SESSION['userid'] = $userinfo['userid'];
            $_SESSION['username'] = $userinfo['username'];
            $_SESSION['headimg'] = $userinfo['headimg'];
            $_SESSION['roomid'] = $userinfo['roomid'];
            $json['status']=1;
            $json['msg']=$userinfo['roomid'];
            // Header("Location:/");
        }else{
            $json['status']=2;
            $json['msg']='安全码错误！';
        }
	}else{
		$json['status']=2;
		$json['msg']='帐号或密码错误！';
	}
    echo json_encode($json);exit;
}
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="favicon.ico" />
<title>登录页</title>
<script src="/Templates/home/js/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="/Templates/home/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="/Templates/home/css/demo.css" />
<!--必要样式-->
<link rel="stylesheet" type="text/css" href="/Templates/home/css/component.css" />
<link rel="stylesheet" type="text/css" href="/Templates/home/css/toastr.css">
<script type="text/javascript" src="/Templates/home/js/toastr.min.js"></script>
<!--[if IE]>
    <script src="/Templates/home/js/html5.js"></script>
    <![endif]-->
</head>
<body data-genuitec-lp-enabled="false" data-genuitec-file-id="wc1-7" data-genuitec-path="/game-http-client/WebRoot/login.html">
<div class="container demo-1" data-genuitec-lp-enabled="false" data-genuitec-file-id="wc1-7" data-genuitec-path="/game-http-client/WebRoot/login.html">
    <div class="content">
        <div id="large-header" class="large-header">
            <canvas id="demo-canvas"></canvas>
            <div class="logo_box"
					style="background-color: white;border-radius: 30px">
                <h3 style="color: black">欢迎你</h3>
                <form id="form" action="" name="f" method="post">
                    <div class="input_outer"> <span class="u_user"></span>
                        <input name="user" id="user" class="text" style="color: black !important" type="text" placeholder="请输入账户">
                        </br>
                        <span
								style="color: red;display: none">账号不能为空</span> </div>
                    <div class="input_outer"> <span class="us_uer"></span>
                        <input name="pass" id="pass" class="text" style="color:black !important; position:absolute; z-index:100;" value="" type="password" placeholder="请输入密码">
                    </div>
                    <div class="hidden">
                      <input id="security_num" type="hidden" name="security_num">
                    </div>
                    <div class="mb2"> <a class="act-but submit" href="javascript:;" style="color: #FFFFFF">登录</a> </br>
                      <a class="act-right" id="a_login" href="/Templates/home/register.php">☞前往注册 </a ></div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="codeLayer">
  <div class="codeForm">
    <div class="rows">
      <div class="row">
        <img src="/NewUI/images/login/ic_pop_server_title.png" alt="">
      </div>
      <div class="row">
        <input id="saveCode" type="password" placeholder="请输入安全码">
      </div>
      <div class="row">
        <span class="codeTips">安全码为6位数字</span>
      </div>
      <div class="row">
        <div id="putCode">确定</div>
      </div>
    </div>
  </div>
</div>
<!-- /container --> 
<script src="/Templates/home/js/tweenlite.min.js"></script> 
<script src="/Templates/home/js/easepack.min.js"></script> 
<script src="/Templates/home/js/raf.js"></script> 
<script src="/Templates/home/js/demo-1.js"></script> 
<script src="/Templates/home/js/core.js"></script> 
<script>
var reg = /^[0-9]{6}$/;
$('#putCode').click(function(){
  let c = $("#saveCode").val();
  // if(!reg.test(c)){
  //   alert('安全码格式错误！');	
  //   return false;
  // }
  $('#security_num').val(c);
  $('.codeLayer').hide();
})

$('.submit').click(function(){
  var security_num = $('#security_num').val();
  // if(!reg.test(security_num)){
  //   alert('请输入安全码');
  //   return false;
  // }
  var user = $('#user').val();
  var pass = $('#pass').val();
  $.ajax({
          type:'post',
          url:"/Templates/home/login.php",
          data: `user=${user}&pass=${pass}&security_num=${security_num}`,
          dataType:'json',
          success:function(data){
            console.log(data);
              if(data.status==1){
                location.href='/index.php?room='+data.msg;	
            }else{
              alert(data.msg);
            }
          }
      });
})
</script>
</body>
</html>