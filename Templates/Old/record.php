<?php include_once(dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php echo $sitename ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="/NewUI/css/mui.min.css">
		<link href="/NewUI/css/common.css" rel="stylesheet"/>
		<link href="/NewUI/css/index.css" rel="stylesheet"/>
		<link href="/NewUI/font/iconfont.css" rel="stylesheet" />
		<link href="/NewUI/css/record.css" rel="stylesheet" />
		<!-- vant css -->
		<link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
		
		<script src="/Style/Old/js/jquery.min.js"></script>
			<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<!-- vue vant js-->
		<script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
		<script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
	    <script src="/Style/Old/js/hotcss.js"></script>
		<style>
			
			
			
			
			/* .record_fooder .up_down_record:nth-child(2){
				border-left: 1px solid #fff;
			} */
			/* .record_fooder .dowm_down{
				flex-grow: 1;
				text-align: center;
				border-left: 1px solid #fff;
			} */
		</style>
	</head>
	<body>
		<div id="record">
		  <template>
			  <div class="record_header">
			  	<a href="javascript:history.go(-1)">
			  	    <img  src="/NewUI/images/chat/ic_back.png"/>
			  	</a>
			  	<span>申请记录</span>
			  </div>
			  <!-- 近一周  近一月  全部 -->
			  <div class="tab_date">
			  				   <div class="record_nums" :class="{tab_record_active: date_index === index}"  v-for="(item,index) in recordnums" :key="item" @click="tabEvent(index)">
			  				   	<span>{{item}}</span>
			  				   </div>
			  </div>	
			  <!-- <div class="record_tab" >
			    
			  	
			  	
			  	
			  </div> -->
			  <div class="list">
			  	<van-pull-refresh v-model="isDownLoading" @refresh="onDownRefresh">
			  	  <van-list
			  	    v-model="isUpLoading"
			  	    :finished="finished"
			  	    finished-text="没有更多了"
			  	    @load="onUpLoad"
					:offset="50"
					:immediate-check="false"
					
			  	  >
			  	    <van-cell v-for="(item,index) in upDowns" :key="index"  >
			  		  
			  		   <div class="cell">
			  			   <div class="shangfen">
			  				   <div class="logo" :class="{apply_up_active:item.type==='上分',apply_down_active:item.type==='下分'}" ></div>
			  				   <div class="apply">
			  					   <div class="title">
			  						   <span v-if="item.type ==='上分'"><b>申请上分</b></span>
			  						   <span v-else><b>申请下分</b></span>
			  					   </div>
			  					   <div class="date">
			  						   {{item.time}}
			  					   </div>
			  				   </div>
			  			   </div>
			  			   <div class="examine">
			  				   <div class="num" v-if="item.type ==='上分'"><b>+{{item.money}}</b></div>
			  				   <div class="num" v-else>-{{item.money}}</div>
			  				   <div class="is_status" :class="{is_handle_status:item.status ==='已处理',not_handle_status:item.status ==='未处理'}">{{item.status}}</div>
			  			   </div>
			  		   </div>
			  		</van-cell>	
			  	  </van-list>
			  	</van-pull-refresh>
			  </div>
			  <div class="record_fooder">
			  	<div class="up_down_record"  @click="updownEvent(index)" v-for="(item,index) in updownrecords" :key="item"><span :class="{record_active: tab_index===index }">{{item}}记录</span></div>
			  	<!-- <div class="dowm_down"><span>下分记录</span></div> -->
			  </div>
			  
		  </template>	
			
		</div>
	</body>
	<script type="text/javascript">
	    var info = {
	        'nickname': "<?php echo $_SESSION['username'] ?>",
	        'headimg':"<?php echo $_SESSION['headimg'] ?>",
	        'userid':"<?php echo $_SESSION['userid'] ?>",
	        'roomid':"<?php echo $_SESSION['roomid'] ?>",
	        'game': "<?php echo $_COOKIE['game'];
	            ?>"
	    };
		
	</script>
	<script type="module">
		import {getParameter,requestAjax,disposeDate} from '/Style/Old/js/common.js'
		var app = new Vue({
			el:'#record',
			data:{
				test:'123',
				chatvalue:'',
				roomshow:false,
				updownrecords:['上分','下分'],
				// 上下分选中下标
				tab_index:0,
				// tab选中下标
				date_index:0,
				recordnums:['近一周','近一月','全部'],
				// 上下分记录数据
				updownArr:[],
				// list
				list: [],
			    isDownLoading: false,
			    finished: false,
			    isUpLoading: false,
				// 上下分记录
				upDowns:[],
				// 当前游戏
				game:'',
				// 上下分界面类型  默认上分
				upDwonType:'上分',
				// 时间类型  默认week
				weekTabType:'week',
				// 总条数
				 counts:0,
				 // 页数
				 page:1
			},
			mounted() {
				// this.$nextTick(()=>{
					this.game ="<?=$_SESSION['game'] ?>"
					// console.log('game=========',this.game)
				// })
				console.log('点击上分：',this.upDwonType,this.weekTabType,this.game,this.page)
				// this.getGameRecord(this.game)
				// this.getGameRecord(this.upDwonType,this.weekTabType,this.game,this.page)
				this.onDownRefresh()
			},
			methods:{
				/*
				查询数据
				*/
			   
			   // 获取上下分记录
			   getGameRecord(type='上分',time='week',game,page){
			   	let data = {
			   		f:'upAndDown',
			   		userid:info.userid,
			   		type,
			   		time,
			   		game,
					page,
					limit:10
			   		
			   	}
			   	let params={
			   		'type':'GET',
			   		'dataType':'json'
			   	}
			   	let Arr=[]
			   			// 获取页面的数据
						console.log('----params---',page)
			   			requestAjax('/Public/ShiroiInterface.php',data,params).then((res)=>{
			   				if(res.data){
								this.counts = res.data.count
								console.log('------------this.counts-----',this.counts)
								console.log('-----返回条数-------',res.data.data.length)
								if(this.upDowns.length >= this.counts){
									console.log('超出长度',this.upDowns.length)
									this.finished = true;
									return false
								}
								if(this.page === 1){
									this.upDowns= res.data.data
									console.log('page==1',this.upDowns.length)
									
								}else{
									this.upDowns=this.upDowns.concat(...res.data.data)
								    console.log('page>1',this.upDowns.length)
								}
			   				}
			   				
			   			}).complete(()=>{
							this.isDownLoading = false
							this.isUpLoading = false
						})
			   			
			   			
			   			
			   			// 获取侧滑页面数据
			   		// }
			   	// }
			   },
				// 上下分
				updownEvent(index){
					// console.log('index====',index)
					this.upDowns = []
					if(this.tab_index === index){
						return false
					}
					if(index === 0){
						
						this.upDwonType = '上分'
						this.onDownRefresh()
						// console.log('点击上分：',this.upDwonType,this.weekTabType,this.game)
						// this.getGameRecord(this.upDwonType,this.weekTabType,this.game,this.page)
					}else{
						this.upDwonType = '下分'
						this.onDownRefresh()
						// console.log('点击下分：',this.upDwonType,this.weekTabType,this.game)
						// this.getGameRecord(this.upDwonType,this.weekTabType,this.game,this.page)
					}
					
					this.tab_index = index
				},
				// tabEvent
				tabEvent(index){
					console.log('idnex==',index)
					if(this.date_index === index){
						return false
					}
					this.page = 1;
					this.upDowns = []
					if(index === 0){
						this.weekTabType = 'week'
						this.onDownRefresh()
						// console.log('点击上分：',this.upDwonType,this.weekTabType,this.game)
						// this.getGameRecord(this.upDwonType,this.weekTabType,this.game,this.page)
					}else if(index === 1 ){
						this.weekTabType = 'month'
						this.onDownRefresh()
						// console.log('点击上分：',this.upDwonType,this.weekTabType,this.game)
						// this.getGameRecord(this.upDwonType,this.weekTabType,this.game,this.page)
					}else{
						this.weekTabType = 'all'
						this.onDownRefresh()
						// console.log('点击上分：',this.upDwonType,this.weekTabType,this.game)
						// this.getGameRecord(this.upDwonType,this.weekTabType,this.game,this.page)
						
					}
					this.date_index = index
				},
				// list
				// 上拉加载数据
				 onUpLoad() {
					 // console.log('上拉')
                        this.page++; 
				      // setTimeout(() => {
				              if (this.isDownLoading) {
				                this.upDowns = [];
				                this.isDownLoading = false;
				              }
				      
				              // for (let i = 0; i < 10; i++) {
				              //   this.list.push(this.list.length + 1);
				              // }
							  this.getGameRecord(this.upDwonType,this.weekTabType,this.game,this.page)
				              // this.isUpLoading = false;
				      
				      //         if (this.upDowns.length > this.counts) {
								  // console.log('数据超过总数')
				      //           this.finished = true;
				      //         }
				     // }, 1000);
                    
                 },
				 
					// 下拉刷新
				    onDownRefresh() {
						 // console.log('刷新')
						 this.upDowns = []
						    this.page = 1
						    this.finished = false;  
							// 重新加载数据
							// 将 loading 设置为 true，表示处于加载状态
							this.isUpLoading = true;
							// this.onUpLoad();
							this.getGameRecord(this.upDwonType,this.weekTabType,this.game,this.page)
				    },
			}
			
		})
	</script>
</html>