<?php include_once(dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php echo $sitename ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="/NewUI/css/mui.min.css">
		<link href="/NewUI/css/common.css" rel="stylesheet"/>
		<link href="/NewUI/css/index.css" rel="stylesheet"/>
		<link href="/NewUI/font/iconfont.css" rel="stylesheet" />
		<link href="/NewUI/css/gamerecord.css" rel="stylesheet" />
		<!-- vant css -->
		<link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
		
		<script src="/Style/Old/js/jquery.min.js"></script>
		<!-- vue vant js-->
		<script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
		<script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
		<script src="/Style/Old/js/hotcss.js"></script>
		<style>
			/* .van-nav-bar{
				background-color: #35A8F1;
				
			}
			.van_header{
				color: #fff;
			}
			.van-nav-bar__title{
				color: #fff;
			} */
			
		
		</style>
	</head>
	<body>
		<div id="game_record">
			<template>
				<div class="header">
					<div class="header_back" >
						<a href="javascript:history.go(-1)"><img src="/NewUI/images/public/ic_back.png" /></a>
						
					</div>
					<div class="title"><span>{{itemActive}}</span></div>
					<div class="other_record" @click="otherRecordEvent">
						<span>其他记录</span>
						<img src="/NewUI/images/public/ic_arrow_right.png" />
					</div>
					
					<!-- 其他记录对话框 -->
					<!-- <div class="other_record_select"  v-if="otherRecordShow" >
						<div class="item" v-for="(item,index) in otherRecords" :key="item"> -->
							<!-- <span class="title">{{item}}</span> -->
							<!-- {{item}}
						</div>
					</div> -->
					<van-overlay :style="{background: 'rgb(0, 0, 0, 0)'}" :show="otherRecordShow" @click="otherRecordShow = false" >
						<div class="other_record_select"   >
							<div class="interval">
								<div class="item" v-for="(item,index) in otherRecords" :key="index" @click="itemTabEvent(item)">
									<!-- <span class="title">{{item}}</span> -->
									{{item.title}}
								</div>
							</div>
							
						</div>
					</van-overlay>
				</div>
				<div class="game_record_content">
					
					<van-pull-refresh v-model="isDownLoading" @refresh="onDownRefresh">
					  <van-list
					    v-model="isUpLoading"
					    :finished="isUpfinished"
					    finished-text="没有更多了"
					    @load="onUpLoad"
						:offset="50"
						:immediate-check="false"
					  >
								<div class="item" v-for="item in gamerecords" :key="item.id">
									<div class="logo_fail" v-if="item.status < 0"></div>
									<div class="logo_vatory" v-else-if="item.status > 0"></div>
									<div class="logo_vatory" v-else></div>
									<!-- <div class="logo_no"></div> -->
									<div class="item_right" >
										<!-- nums -->
										<div class="nums" v-if="!item.isResults" > 
											 <div class="arrow"><img src="/NewUI/images/public/ic_bright_right.png" /></div> 
											 <div class="nums_arrow" @click="isNumsResultsEvent(item)">
												<div class="title">
													<span v-if="item.mingci === '1'"><b>冠军【{{item.content}}/{{item.money}}】</b></span>
													<span v-else-if="item.mingci === '2'"><b>亚冠【{{item.content}}/{{item.money}}】</b></span>
													<span v-else><b>第{{item.mingci}}【{{item.content}}/{{item.money}}】</b></span>
													<span>{{item.term}}期</span>
												</div>
												<div class="nums_re">
													<span :class="item.status > 0 ? 'status_green':'status_red'">{{item.status}}</span>
													<span>{{item.addtime}}</span>
													
												</div>
											</div> 
										</div>
										<!-- result -->
										<div class="result" v-else  >
											<div class="back"><img src="/NewUI/images/xuandan/ic_bright_leftt.png" /></div>
											<div class="result_back" @click="NumsResultsEvent(item)">
												<div class="results_nums">
													<div class="number">
														<p class="number_btn ball_pks_ ball_lenght10" :class="{ball_pks1:items==='01',ball_pks2:items==='02',ball_pks3:items==='03',ball_pks4:items==='04',ball_pks5:items==='05',ball_pks6:items==='06',ball_pks7:items==='07',ball_pks8:items==='08',ball_pks9:items==='09',ball_pks10:items==='10'}" v-for="(items,index) in item.term_arr.code_arr" :key="index"></p>
																
													</div>
												</div>
												<div class="result_end">
													<p>亚冠和&nbsp;:&nbsp;<span>{{item.term_arr.k1}}</span><span>{{item.term_arr.k2}}</span><span>{{item.term_arr.k3}}</span></p>
													<p>1-5龙虎&nbsp;:&nbsp;<span v-for="(items,index) in item.term_arr.k4" :key="index">{{items}}</span></p>
													
												</div>
											</div>
										</div>
										
									</div>
									
								</div>
					</van-list>
					</van-pull-refresh>	
				</div>
			</template>
		</div>
	</body>
	<script type="text/javascript">
	    var info = {
	        'nickname': "<?php echo $_SESSION['username'] ?>",
	        'headimg':"<?php echo $_SESSION['headimg'] ?>",
	        'userid':"<?php echo $_SESSION['userid'] ?>",
	        'roomid':"<?php echo $_SESSION['roomid'] ?>",
	        'game': "<?php echo $_COOKIE['game'];
	            ?>"
	    };
	</script>
	<script type="module">
		import {getParameter,requestAjax,disposeDate} from '/Style/Old/js/common.js'
		var app = new Vue({
			el:'#game_record',
			data:{
				test:'123',
				otherRecordShow:false,
				otherRecords:[
					// {
					// 	title:'北京赛车',
					// 	alis:'pk10'
					// },
					// {
					// 	title:'幸运飞艇',
					// 	alis:'xyft'
					// },
					// {
					// 	title:'澳洲幸运5',
					// 	alis:'cqssc'
					// },
					{
						title:'澳洲幸运10',
						alis:'jssm'
					},
					{
						title:'极速赛车',
						alis:'jssc'
					}
					
					],
				itemActive:'北京赛车',
				// isResults:false,
				
				//数据
				gamerecords:[],
				// 当前游戏
				game:'',
				
				// 当前游戏title
				gameTitle:null,
				// 处理上下拉加载判断的
				 
				isDownLoading:false,//下拉刷新
				isUpLoading:false,//上拉加载
				isUpfinished:false ,//加载完成
				counts:0,//当前数据条数	
				page:1,// 当前页数
				
				
				
			},
			mounted(){
				this.game ="<?=$_SESSION['game'] ?>"
				this.gameTitle = decodeURI(getParameter('title'))
				// console.log('this.gameTitle',typeof(this.gameTitle))
				if(this.gameTitle != 'null'){
					this.itemActive = this.gameTitle
					for(let i = 0 ;i< this.otherRecords.length;i++){
						if(this.gameTitle === this.otherRecords[i].title){
							this.game = this.otherRecords[i].alis
							this.onDownRefresh()
							// this.getGameRecord(this.game,this.page)
						}
					}
					
				}else{
					// 当前游戏进去
					// console.log('当前游戏进去')
					for(let i=0;i<this.otherRecords.length;i++){
						if(this.game === this.otherRecords[i].alis ){
							this.itemActive = this.otherRecords[i].title
						}
					}
					this.onDownRefresh()
					// this.getGameRecord(this.game,this.page)
					
				}
			    
				
			},
			methods:{
				isNumsResultsEvent(item){
					// console.log('index-------',index)
					item.isResults = true
				},
				NumsResultsEvent(item){
					// console.log('index-------',index)
					item.isResults = false
				},
				otherRecordEvent(){
					this.otherRecordShow = !this.otherRecordShow
				},
				// tab选中事件
				itemTabEvent(value){
					this.itemActive = value.title
					this.gamerecords = []
					this.game = value.alis
					this.onDownRefresh()

				},
				// 数据获取
				getGameRecord(game,page='1'){
					let data = {
						f:'bettingRecord',
						userid:info.userid,
						time:'week',
						game,
						page,
						limit:10
						
					}
					let params={
						'type':'GET',
						'dataType':'json'
					}

					let Arr=[]

							// 获取页面的数据
							requestAjax('/Public/ShiroiInterface.php',data,params).then((res)=>{
								if(res.data){
									this.counts = res.data.count
						           if (this.gamerecords.length >= this.counts) {
						           	  console.log('数据超过总数')
						               this.isUpfinished = true;
						           }
						
									 for(let i =0;i<res.data.data.length;i++){
										 res.data.data[i].isResults = false
										 // console.log('dasdad')
										   
									 }
									 // 处理数据
									 if (this.page === 1) {
										this.gamerecords=res.data.data
									 } else {
									   this.gamerecords = this.gamerecords.concat(...res.data.data)
									 } 		      
								}		
							}).complete(()=>{
								console.log('请求完成')
								this.isDownLoading = false
								this.isUpLoading = false    
							})
							// 获取侧滑页面数据
						// }
					// }
				},
				// 上拉加载数据
				 onUpLoad(){
					 this.page++ 
					 
					 if(this.isDownLoading){
						 this.gamerecords=[]
						 this.isDownLoading = false
					 }
					 this.getGameRecord(this.game,this.page)	 
				 },
				 //下拉刷新
				 onDownRefresh(){
					 this.gamerecords = []
					 this.page = 1
					 this.isUpfinished = false
					 // 将 loading 设置为 true，表示处于加载状态
					 this.isUpLoading = true;
					 this.getGameRecord(this.game,this.page)
				 }
			}
		})
	</script>
</html>