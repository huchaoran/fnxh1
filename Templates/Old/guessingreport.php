<!DOCTYPE >
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php echo $sitename ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="/NewUI/css/mui.min.css">
		<link href="/NewUI/css/common.css" rel="stylesheet"/>
		<link href="/NewUI/css/index.css" rel="stylesheet"/>
		<link href="/NewUI/font/iconfont.css" rel="stylesheet" />
		<!-- Bootstrap stylesheet -->
		<link rel="stylesheet" type="text/css" href="/Style/Old/css/bootstrap.min.css">
		
		<!-- ClockPicker Stylesheet -->
		<link rel="stylesheet" type="text/css" href="/Style/Old/css/bootstrap-clockpicker.min.css">
		<!-- vant css -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vant@2.9/lib/index.css"/>
	    <link rel="stylesheet" href="/NewUI/css/guessingreport.css"/>
		<script src="/Style/Old/js/jquery.min.js"></script>
		<!-- bootstrap -->
		<script type="text/javascript" src="/Style/Old/js/bootstrap.min.js"></script>
		
	    <!-- vue vant js-->
	    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
	    <script src="https://cdn.jsdelivr.net/npm/vant@2.9/lib/vant.min.js"></script>
	    <!-- ClockPicker script -->
	    <script type="text/javascript" src="/Style/Old/js/bootstrap-clockpicker.js"></script>
	    <script src="/Style/Old/js/hotcss.js"></script>
	</head>
	<style>
		
	</style>
	<body>
	<script type="text/javascript">
	    var info = {
	        'nickname': "<?php echo $_SESSION['username'] ?>",
	        'headimg':"<?php echo $_SESSION['headimg'] ?>",
	        'userid':"<?php echo $_SESSION['userid'] ?>",
	        'roomid':"<?php echo $_SESSION['roomid'] ?>",
	        'game': "<?php echo $_COOKIE['game'];
	            ?>"
	    };
		
	    console.log(info);
	   
	
	</script>	
		<div id="guessingreport">
			<div class="header">
				<a href="javascript:history.go(-1)"><img src="/NewUI/images/public/ic_back.png" /></a>
				<span>竞猜报表</span>
			</div>
			<!-- content -->
			<div class="content">
				
				<div class="room_num"><span>房间号:</span><span>394555</span></div>
                <!-- 最小日期选择 -->
				<div  @click.stop="minSelectedEvent($event)" class="min_date" id="min_date"><span>{{minDate.year}}/{{minDate.mon}}/{{minDate.day}}</span><input  disabled="true" type="text" class="calendarMin" id="minclickMe" value=""   v-model="minHourMin"></div>
                <!-- 最大日期选择 -->  
				<div @click.stop="maxSelectedEvent($event)" class="max_date"><span>{{maxDate.year}}/{{maxDate.mon}}/{{maxDate.day}}</span><input  disabled="true" type="text" class="calendarMin" id="maxclickMe" value=""   v-model="maxHourMin"></div>
			    <div class="remarks"><span>备注:选择时间按钮,自动回填上面日期</span></div> 
			    <!-- tab -->
				<div class="date_tab">
					<div class="item" :class="{date_active: tab_index === index}" @click="dateselectEvent(item,index)" v-for="(item,index) in dateTabs" :key="item">
						<span>{{item}}</span>
					</div>
				</div>
				<!-- 查询 -->
				<div class="query" @click="gussingQueryEvent()"><span>查询</span></div>
				<!-- 最小日期选择 -->
				<!-- <van-overlay :show="minShow" > -->
				<div v-show="minShow" @click="minShow = false" class="vant_overlay">
					<div class="cus_header clockpicker" v-if="calendarHeaderShow">
						<div class="mon_date">
							<span>{{minDate.mon}}月{{minDate.day}}日</span>
						</div>
						<div class="year_lunar">
							<span>{{minDate.year}}</span>
							<span>{{minDate.lunar}}</span>
						</div>
						<!-- <div @click="calendarEvent()"  v-if="calendarBtnShow" >
						        <span >确认</span>
						</div> -->
						<!-- 时钟 -->
						<!-- <input @change="changeInput" @click="inputClockEvent" type="hidden" class="form-control" id="clickMe" value="09:16"  placeholder="Now" > -->
					</div>
					<!-- 日历 -->
					<div class="calendar_bg" @click.stop="minStopPropagation($event)">
					<!-- <div class="calendar_bg" > -->
						<van-calendar  :min-date="historyDate" :poppable="false" :formatter="formatter"  @confirm="minformatterEvent"   :show-subtitle="false" :show-title="false" :show-confirm="false"  v-if="minCalendarShow"     v-model="minCalendarShow" :show-confirm="false"   >
						</van-calendar>
					</div>
						
					<!-- 时钟 -->
					
					<!-- <button type="button" id="check-minutes" @click="aa($event)">Check the minutes</button> -->
					
					   
					 
				</div>
				<!-- </van-overlay>	 -->
				<!-- 最大日期选择 -->
			 <!-- <van-overlay :show="maxShow" @click="maxShow = false"> -->
				  <div v-show="maxShow" @click="maxShow = false" class="vant_overlay">	
						<div class="cus_header">
							<div class="mon_date">
								<span>{{maxDate.mon}}月{{maxDate.day}}日</span>
							</div>
							<div class="year_lunar">
								<span>{{maxDate.year}}</span>
								<span>{{maxDate.lunar}}</span>
							</div>
						</div>
						<!-- 日历 -->
						<div class="calendar_bg" @click.stop="maxStopPropagation($event)">
						  <van-calendar :min-date="historyDate" :poppable="false" :formatter="formatter"  @confirm="maxformatterEvent"   :show-subtitle="false" :show-title="false" :show-confirm="false"  v-if="maxCalendarShow"     v-model="maxCalendarShow" :show-confirm="false" >
						  </van-calendar>	
						</div>
				  </div>		
			 <!-- </van-overlay> 	 -->
			</div>
		</div>
	</body>
    
    </script>
	<script type="module">
		import calendar from '/Style/Old/js/calendar.js';
		import {getParameter,requestAjax,disposeDate} from '/Style/Old/js/common.js'
		// $('.calendar_input').click=function(){
		// 	console.log('input')
		// }
		// $('.clockpicker').clockpicker()
		//         .find('input').change(function(){
		//             // TODO: time changed
		// 			// this.operationsValue = this.value
		// 			console.log("this.operationsValue======",this.operationsValue);
		//         });
		// var calendar_input = document.getElementById('calendar_input')
		
		// var clickMe = document.getElementById('clickMe')
		// 针对btn阻止冒泡事件的
		// var input = $('#clickMe').clockpicker({
		//     placement: 'bottom',
		//     align: 'left',
		//     autoclose: false,
		//     'default': 'now',
		// 	vibrate:true,
		// 	donetext: '完成',
			
		// });
		var app = new Vue({
			el:'#guessingreport',
			data:{
				//竞猜报表数据
				guessingObj:{},
				dateTabs:['昨天','今天','本周','上周','本月','上月'],
				tab_index:0,
				minShow:false,
				maxShow:false,
				// 最小日期
				minDate:{
					year:'2020',
					mon:'07',
					day:'01',
					// hour:'06',
					// min:'00',
					lunar:'初四'
				},
				// minShow:false,
				// 最大日期
				maxDate:{
					year:'2020',
					mon:'07',
					day:'04',
					// hour:'06',
					// min:'00',
					lunar:'初四'
				},
				historyDate: new Date(2010, 0, 1),
				overlayshow:false,
				minCalendarShow:false,
				maxCalendarShow:false,
				// 时间双向绑定
				operationsValue:'06:00',
				// 时间确认按钮show
				calendarBtnShow:false,
				minValue:'06:30',
				// calendar header show
				calendarHeaderShow:true,
				//input value
				minHourMin:'06:00',
				maxHourMin:'06:00'
				
			},
			mounted(){
				this.initDateTime()
				// this.test()
			},
			methods:{
				// 查询数据
				getData(startTime,endTime,done){
					let data = {
						f:'getReport',
						userid:info.userid,
						roomid:'1',
						startTime,
						endTime
					}
					let params = {
						type:'GET',
						dataType:'json'
					}
					let Arr=[]
					requestAjax('/Public/ShiroiInterface.php',data,params).then((res)=>{
						if(res.data){
							console.log('numChangArr-----',res)
							this.guessingObj = res.data
							done(res.data)
							// this.counts = res.data.count
							 // for(let i =0;i<res.data.data.length;i++){
								 // res.data.data[i].isResults = false
								 // console.log('dasdad')
								   
							 // }
							 // console.log('gamerecords----====',res)
							
								// for(let i=0;i<res.data.length;i++){
								// 	if(res.data[i].money>0){
								// 		console.log('大于0')
								// 		// res.data[i].totalnums =this.money.toFixed(2)+res.data[i].money.toFixed(2)
								// 		// this.money =pthis.money.toFixed(2)+res.data[i].money.toFixed(2)
								// 		res.data[i].totalnums =parseInt(this.money)+parseInt(res.data[i].money)   
								// 		this.money =parseInt(this.money)+parseInt(res.data[i].money)
								// 	}else{
								// 		console.log('小于0')
								// 		res.data[i].totalnums = this.money - Math.abs(res.data[i].money)
								// 	    this.money =this.money - Math.abs(res.data[i].money)
								// 	}
								// 	// res.data.totalnums = this.money res.data[i]
								// }
								
								// this.numChangArr=res.data
							
						     console.log('numChangArr====',this.guessingObj)
						}
						
					}).complete(()=>{
						console.log('请求完成')
						this.isDownLoading = false
						this.isUpLoading = false    
					})
				},
				// 初始化日期时间--默认昨天
				initDateTime(){
					var datetime = new Date();
					// console.log('-------',this.mindatetime)
					var year = datetime.getFullYear(); 
					var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1; 
					var date = datetime.getDate() < 10 ? "0" : datetime.getDate(); 
					this.maxDate.year = year 
					this.maxDate.mon = month
					this.maxDate.day = date
					
					this.minDate.year = year
					this.minDate.mon = month
					this.minDate.day = parseInt(date)-parseInt(1)
					
					
				},
				// 日期时间
				DateTime(week){
					var datetime = new Date();
					var year = datetime.getFullYear();
					var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1; 
					var date = datetime.getDate() < 10 ? "0" : datetime.getDate(); 
					var weeks = datetime.getDay()
					if(week === '昨天'){
						this.initDateTime()
					}else if(week ==='今天'){
						this.minDate.year = year
						this.minDate.mon = month
						this.minDate.day = date
						this.maxDate.year = year
						this.maxDate.mon = month
						this.maxDate.day = parseInt(date)+parseInt(1)
						this.maxDate.day=this.addzero(this.maxDate.day)
					}else if(week ==='本周'){
						this.minDate.year = year
						this.minDate.mon = month
						this.minDate.day = parseInt(date)-(parseInt(weeks)-parseInt(1))
						this.minDate.day=this.addzero(this.minDate.day)
						
						this.maxDate.year = year
						this.maxDate.mon = month
						this.maxDate.day = parseInt(this.minDate.day)+parseInt(7)
						this.maxDate.day=this.addzero(this.maxDate.day)
					}else if(week ==='上周'){
						this.maxDate.year = year
						this.maxDate.mon = month
						this.maxDate.day = parseInt(date)-(parseInt(weeks)-parseInt(1))
						this.maxDate.day=this.addzero(this.maxDate.day)
						
						this.minDate.year = year
						this.minDate.mon = month
						this.minDate.day = parseInt(this.maxDate.day)-parseInt(7)
						this.minDate.day=this.addzero(this.minDate.day)
					}else if(week ==='本月'){
						this.minDate.year = year
						this.minDate.mon = month
						this.minDate.day = '01'
						
						this.maxDate.year = year
						this.maxDate.mon = parseInt(month)+parseInt(1)
						this.maxDate.mon=this.addzero(this.maxDate.mon)
						this.maxDate.day = '01'
					}else{
						this.maxDate.year = year
						this.maxDate.mon = month
						this.maxDate.day = '01'
						
						this.minDate.year = year
						this.minDate.mon = parseInt(month)-parseInt(1)
						this.minDate.mon=this.addzero(this.minDate.mon)
						
						this.minDate.day = '01'
					}
					
				},
				addzero(time){
					if(time < 10){
						return "0"+time
					}
					return time
				},
				// 时钟选中事件
				calendarEvent(){
					console.log('时钟选中')
					this.minShow = false
					
					
				},
				changeInput(){
					console.log('input输入事件')
				},
				inputClockEvent(value){
					this.minShow = false
					console.log("this.operationsValue======",value)
				},
				minSelectedEvent(e){
					e.stopPropagation();
					
					// this.minShow = true
					// this.minCalendarShow = true 
					// this.minShow = !this.minShow
					// this.minCalendarShow = !this.minCalendarShow 
					this.minShow = true
					this.minCalendarShow = true 
					this.maxHourMin = $('#maxclickMe').val()
						// input.clockpicker('show')
						//         .clockpicker('toggleView', 'minutes');
					// console.log('vue取消冒泡')
				},
				maxSelectedEvent(e){
					e.stopPropagation();
					
					// this.minShow = true
					// this.minCalendarShow = true 
					// this.minShow = !this.minShow
					// this.minCalendarShow = !this.minCalendarShow 
					this.maxShow = true
					this.maxCalendarShow = true
					 this.minHourMin = $('#minclickMe').val()
						// input.clockpicker('show')
						//         .clockpicker('toggleView', 'minutes');
					// console.log('vue取消冒泡')
				},
				minStopPropagation(e){
					console.log('vue11取消冒泡')
					// e.stopPropagation();
					e.stopPropagation();
					// input.clockpicker('show')
					 		// .clockpicker('toggleView', 'minutes');
		// 					$('.clockpicker').clockpicker()
        // .find('input').change(function(){
        //     // TODO: time changed
        //     console.log('SDADSADASDA',this.value);
        // });
					// // input.clockpicker('show');	
					// $('.clockpicker').clockpicker()
					//         .find('input').change(
					// 		    function(){
					// 				console.log('vue22取消冒泡',this.minValue)
					// 			}
								// ()=>{
								// 	// this.minShow = false
								// 	console.log('vue22取消冒泡',this.minValue)
								// }
					  //           // TODO: time changed
							// 	// this.operationsValue = this.value
								
							// 	console.log("this.operationsValue======",this.operationsValue,this.minValue);
					  //           // this.minCalendarShow = false 
							// 	this.minShow = false
							// 	console.log('vue22取消冒泡')
							
							
							// );	
					// 		console.log('vue22取消冒泡')
				},
				// tab选择事件
				maxStopPropagation(e){
					console.log('vue11取消冒泡')
					// e.stopPropagation();
					e.stopPropagation();
				},
				
				// test11(){
				// 	console.log('123')
				// },
				dateselectEvent(item,index){
					
					console.log('index---',index,$('#minclickMe').val())
					
					if(this.tab_index === index){
						return false
					}
					this.tab_index = index
					this.DateTime(item)
				},
				// 日期格式化
				formatter(day) {
					   const year = day.date.getFullYear();
				       const month = day.date.getMonth() + 1;
				       const date = day.date.getDate();
				       let lunar = calendar.solar2lunar(year,month,date) 
					    // console.log('lunar------',lunar)
				      day.bottomInfo = lunar.IDayCn
				       return day;
				},
				// 日历选中的日期事件
				   //最小日期选择事件
				minformatterEvent(date){
					this.minShow = false
					// console.log('minShow====',this.minShow)
					// e.stopPropagation()
					 // this.datetime = this.formatDate(day)
					 // console.log('day===',this.formatDate(day))
					 this.minDate.year = `${date.getFullYear()}`
					 this.minDate.mon = `${date.getMonth() + 1}`
					 this.minDate.day = `${date.getDate()}`
					 let lunar = calendar.solar2lunar(this.minDate.year,this.minDate.mon,this.minDate.day)
					 this.minDate.lunar = lunar.IDayCn
					 this.minCalendarShow = false
					 // 触发时钟
					 // if(!this.minCalendarShow){
						 // $('.calendar_input').focus()
						 // (function(){
							//  console.log('calendar_input方法触发')
						 // })
						 // $('.calendar_input').clockpicker()
						 // this.calendarBtnShow = true
						 this.mintest()
						//  console.log('clockpicker')
						
					 // }
					
					 // var input = $('#min_date').clockpicker({
					 // 	placement: 'bottom',
					 // 	align: 'left',
					 // 	autoclose: true,
					 // 	'default': 'now'
					 // });
					 console.log('最小')
					 // console.log('lunar------',lunar.IDayCn)
				},
				maxformatterEvent(date){
					this.maxShow = false
					this.maxDate.year = `${date.getFullYear()}`
					this.maxDate.mon = `${date.getMonth() + 1}`
					this.maxDate.day = `${date.getDate()}`
					let lunar = calendar.solar2lunar(this.maxDate.year,this.maxDate.mon,this.maxDate.day)
					this.maxDate.lunar = lunar.IDayCn
					this.maxCalendarShow = false
					 this.maxtest()
					console.log('最大')
				},
				   // 最小日期btn事件
				   mindateEvent(e){
					   this.minShow = true  
					   this.minCalendarShow = true 	
					   // e.stopPropagation();
					   // input.clockpicker('show')
					           // .clockpicker('toggleView', 'minutes');
					   // calendar_input.clockpicker()
					   // console.log(calendar_input.clockpicker())
				   },
				   maxdateEvent(){
				   	   this.maxShow = true   
				   },
				   // 针对input事件的
				   mintest(){
					   // $('.clockpicker').clockpicker({
					   //     placement: 'bottom',
					   //     align: 'left',
					   //     donetext: 'Done'
					   // });
						  
					   var input = $('#minclickMe').clockpicker({
					       placement: 'bottom',
					       align: 'left',
					       autoclose: true,
					       'default': 'now',
						  
					   });
					   input.clockpicker('show')
					   // console.log('12131')
				   },
				   maxtest(){
				   					   // $('.clockpicker').clockpicker({
				   					   //     placement: 'bottom',
				   					   //     align: 'left',
				   					   //     donetext: 'Done'
				   					   // });
				   						  
				   					   var input = $('#maxclickMe').clockpicker({
				   					       placement: 'bottom',
				   					       align: 'left',
				   					       autoclose: true,
				   					       'default': 'now',
				   						  
				   					   });
				   					   input.clockpicker('show')
				   					   // console.log('12131')
				   },
				   gussingQueryEvent(){
					   console.log('查询')
					   
					   // $('#maxclickMe').val()
					   this.startTime = this.minDate.year+'-'+this.minDate.mon+'-'+(this.minDate.day)+' '+$('#minclickMe').val()
					   this.endTime = this.maxDate.year+'-'+this.maxDate.mon+'-'+(this.maxDate.day)+' '+$('#maxclickMe').val()
					   this.getData(this.startTime,this.endTime,(data)=>{
						    console.log('time----',this.startTime,this.endTime,JSON.stringify(this.guessingObj))
					      window.location.href = '/Templates/Old/guessingreportquery.php?guessingObj='+JSON.stringify(data)
					   })
					  
					   
					   
					   // return false
					  
				   }
			},
			
		})
	</script>
</html>