<!!DOCTYPE >
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php echo $sitename ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" />
		<!-- vant css -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vant@2.9/lib/index.css"/>
		<link rel="stylesheet" href="/NewUI/css/guessingreportquery.css"/>
		<script src="/Style/Old/js/jquery.min.js"></script>
		<!-- bootstrap -->
		<script type="text/javascript" src="/Style/Old/js/bootstrap.min.js"></script>
		
		<!-- vue vant js-->
		<script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/vant@2.9/lib/vant.min.js"></script>
		<script src="/Style/Old/js/hotcss.js"></script>
		<style>
			
			
		</style>
	</head>
	<body>
		<div id="guessingreportquety" >
			<div class="header">
				<a href="javascript:history.go(-1)"><img src="/NewUI/images/public/ic_back.png" /></a>
				<span>竞猜报表</span>
			</div>
			<!-- result -->
			<div class="requery_result" >
				<div class="user"><span>wang4567</span></div>
				<div class="date">
					<span class="min_date">{{totalResults["time"]}}</span>
					<!-- <span>-</span>
					<span class="max_date">2020/08/11 06:00</span> -->
				</div>
				<div class="result">
					<div class="item" v-for="(item,key) in totalDic" :key="key">
						<span>{{key}}</span>
						<span>{{item}}</span>
					</div>
				</div>
			</div> 
			<!-- list -->
			<div class="list">
				<div class="item" v-for="(items,key) in gameArr" @click="listItemTabEvent(items)">
					<div class="header_list">
						<img class="logo" :src="items.logo"/>
						<span class="title">{{items.title}}</span>
						<img class="arrow" src="/NewUI/images/public/ic_bright_right.png"/>
					</div>
					<div class="list_li" >
						<div class="item_li" v-for="(items1,index) in listResultArr">
							<span>{{items1}}</span>
							<span v-if="items ==='注单'">{{items.orderinfo}}</span>
							<span v-else-if="items ==='玩家结果'">{{items.playerresult}}</span>
							<span v-else-if="items ==='游戏结果'">{{items.gameresult}}</span>
							<span v-else-if="items ==='注额'">{{items.money}}</span>
							<span v-else-if="items ==='返点'">{{items.backwater}}</span>
							<span v-else>0</span>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</body>
	<script type="module">
		import {getParameter,requestAjax,disposeDate} from '/Style/Old/js/common.js'
		var app = new Vue({
			el:'#guessingreportquety',
			data:{
				quetyTabs:['总主单','总注额','游戏总结果','红利合计','返点合计','玩家总结果'],
				listArrs:[
					{
						logo:'/NewUI/images/roomgame/ic_game_car_icon.png',
						title:'北京赛车',
						alias:'pk10'
					},
					{
						logo:'/NewUI/images/roomgame/ic_game_ship_icon.png',
						title:'幸运飞艇',
						alias:'xyft'
					},
					{
						logo:'/NewUI/images/roomgame/ic_game_ao5_icon.png',
						title:'澳洲幸运5',
						alias:'cqssc'
					},
					{
						logo:'/NewUI/images/roomgame/ic_game_ao10_icon.png',
						title:'澳洲幸运10',
						alias:'jssm'
					},
					{
						logo:'/NewUI/images/roomgame/ic_game_js_icon.png',
						title:'极速赛车',
						alias:'jssc'
					}
					
				],
				totalResults:{},
				totalDic:{},
				gameArr:{},
				listResultArr:['注单','红利','游戏结果','注额','返点','玩家结果']
				
			},
		mounted(){
			this.totalResults =  JSON.parse(decodeURI(getParameter('guessingObj')))
				
			for(let key in this.totalResults){
				this.totalDic["总注单"] = this.totalResults["总注单"]
				this.totalDic["总注额"] = this.totalResults["注额"]
				this.totalDic["游戏总结果"] = this.totalResults["游戏总结果"]
				this.totalDic["红利合计"] = this.totalResults["盈亏"]
				this.totalDic["返点合计"] = this.totalResults["返点合计"]
				this.totalDic["玩家总结果"] = this.totalResults["玩家总结果"]
				for(let key1 in this.totalResults["game"] ){
					for(let i=0;i<this.listArrs.length;i++){
						if(key1 === this.listArrs[i]["alias"]){
							this.totalResults["game"][key1].title= this.listArrs[i].title
							this.totalResults["game"][key1].logo= this.listArrs[i].logo
						}
					}
					
				}
				this.gameArr = this.totalResults["game"]
				
			}
		    console.log('guessingObj----',this.gameArr,this.totalDic)
			
		},
		methods:{
			listItemTabEvent(items){
				window.location.href = '/Templates/Old/gamerecord.php?title='+items.title
				console.log('window.location.href',window.location.href)
				// return false
			}
		}
		})
		
	</script>
</html>