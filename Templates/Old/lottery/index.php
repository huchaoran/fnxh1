<?php
$roomid = $_SESSION['roomid'];
$userinfo = get_query_vals('fn_user', '*', array('roomid' => $roomid, 'userid' => $_SESSION['userid']));
$fengpan = (int)get_query_val('fn_lottery1', 'fengtime', array('roomid' => $_SESSION['roomid']));
$info1 = get_query_vals('fn_lottery1', '*', array('roomid' => $_SESSION['roomid']));
$info2 = get_query_vals('fn_lottery2', '*', array('roomid' => $_SESSION['roomid']));
$info3 = get_query_vals('fn_lottery3', '*', array('roomid' => $_SESSION['roomid']));
$info4 = get_query_vals('fn_lottery4', '*', array('roomid' => $_SESSION['roomid']));
$info5 = get_query_vals('fn_lottery5', '*', array('roomid' => $_SESSION['roomid']));
$info6 = get_query_vals('fn_lottery6', '*', array('roomid' => $_SESSION['roomid']));
$info7 = get_query_vals('fn_lottery7', '*', array('roomid' => $_SESSION['roomid']));
$info8 = get_query_vals('fn_lottery8', '*', array('roomid' => $_SESSION['roomid']));
$info9 = get_query_vals('fn_lottery9', '*', array('roomid' => $_SESSION['roomid']));
$info10 = get_query_vals('fn_lottery10', '*', array('roomid' => $_SESSION['roomid']));
$pk10open = $info1['gameopen'];
$xyftopen = $info2['gameopen'];
$cqsscopen = $info3['gameopen'];
$xy28open = $info4['gameopen'];
$jnd28open = $info5['gameopen'];
$jsmtopen = $info6['gameopen'];
$jsscopen = $info7['gameopen'];
$jssscopen = $info8['gameopen'];
$kuai3open = $info9['gameopen'];
$gxk3open = $info9['gxgameopen'];
$bjk3open = $info9['bjgameopen'];
$lhcopen = $info10['gameopen'];
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?php echo $sitename ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="/NewUI/css/mui.min.css">
	<link href="/NewUI/css/common.css" rel="stylesheet" />
	<link href="/NewUI/css/index.css" rel="stylesheet" />
	<link href="/NewUI/font/iconfont.css" rel="stylesheet" />
	<!-- vant css CDN-->
	<link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
	<link href="/NewUI/css/home.css" rel="stylesheet" />
	
	<style>
		#page_loading {
			position: fixed;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			z-index: 999;
			background-color: #000;
			opacity: 0.7;
		}

		#page_loading .van-loading {
			position: absolute;
			left: 50%;
			top: 50%;
			width: 60px;
			height: 60px;
			margin-top: -30px;
			margin-left: -30px;
		}
	</style>
</head>

<body>
	<div id="home">
		<div id="page_loading">
			<div class="van-loading van-loading--spinner van-loading--vertical"><span class="van-loading__spinner van-loading__spinner--spinner" style="width: 24px; height: 24px;"><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i></span><span class="van-loading__text">加载中...</span></div>
		</div>
		<template>
			<!-- header -->
			<div class="home_header">
				<div class="home_title">傲盛</div>
				<div class="user_info" @click="userEvent">
					<div class="user">
						<div class="user_logo">
							<img :src="userInfo.headImg" alt="">
						</div>
						<div class="info">
							<span>{{userInfo.userName}}</span>
							<span>ID:{{userInfo.id}}</span>
						</div>
					</div>
					<div class="cash">
						<span>{{userInfo.money}}</span>
					</div>
				</div>
				<div class="notice">
					<img src="/NewUI/images/home/ic_horn.png" />
					<van-notice-bar>

					</van-notice-bar>
				</div>
			</div>
			<!-- body -->

			<div class="content">
				<!-- 游戏类型 -->
				<div v-for="(items,index) in homelist"   @click="room(items.data.type,index,items.data.gameopen)" :key="items.data.id">
				   <!-- <div v-if="items.data.gameopen =='true'" :class="{car_bg_jssc:items.data.title === '极速赛车',car_bg_azxy5:items.data.title === '澳洲幸运5',car_bg_azxy10:items.data.title === '澳洲幸运10',car_bg_bjsc:items.data.title === '北京赛车',car_bg_xyft:items.data.title === '幸运飞艇'}"> -->
				   <div v-if="items.data.gameopen =='true'"   class="beijing_car" :class="{car_bg_jssc:items.data.title === '极速赛车',car_bg_azxy5:items.data.title === '澳洲幸运5',car_bg_azxy10:items.data.title === '澳洲幸运10',car_bg_bjsc:items.data.title === '北京赛车',car_bg_xyft:items.data.title === '幸运飞艇'}"   >
					  <img :src="items.data.logo" />
					  <div class="beijing_car_result">
					  	<div class="nums_result" :title="items.data.sealingPlateShow?'true':'FALSE'">{{items.data.term}}期</div>
					  	<div class="result">
					  		<div class="resule_item" v-for="(item,index) in items.data.code" :key="index">
					  			<img :src='"/NewUI/images/home/ic_ball_"+item+".png"' />
					  		</div>
					  	</div>
					  	<div class="surplus_result">
					  		<div class="result_sealingPlate" v-if="items.data.sealingPlateShow">
					  			<span>封盘中</span>
					  		</div>
					  		<div class="result_drawPrize" v-else-if="items.data.drawPrizeShow">
					  			<span>开奖中</span>
					  		</div>
					  		<div class="time_result" v-else>
					  			<div class="surplus_title">距离封盘剩余</div>
					  			<van-count-down @finish="homefinish(items,index)" :time="items.data.currentTime" format="mm:ss">
					  			</van-count-down>
					  		</div>
					  	</div>
					  </div>
				  </div>
				  <!-- <div else :class="{car_bg_jssc_closed:items.data.title === '极速赛车',car_bg_azxy10_closed:items.data.title === '澳洲幸运10'">
					  <div>aaa</div>
				  </div> -->
				  <div v-else class="beijing_car" :class="{car_bg_jssc_closed:items.data.title === '极速赛车',car_bg_azxy10_closed:items.data.title === '澳洲幸运10'}" >
				  					  
				  </div>
					
				</div>
				<!-- <div v-else class="beijing_car" :class="{car_bg_jssc:items.data.title === '极速赛车',car_bg_azxy5:items.data.title === '澳洲幸运5',car_bg_azxy10:items.data.title === '澳洲幸运10',car_bg_bjsc:items.data.title === '北京赛车',car_bg_xyft:items.data.title === '幸运飞艇'}" v-for="(items,index) in homecloselist"  :key="items.data.id"> -->
					
				</div>
			</div>
			<!-- fooder -->
			<div class="fooder">
				<div class="item" v-for="(item,index) in fooders" @click="homeTabEvent(index)" :key="item.id">
					<img :src="item.src" />
				</div>
			</div>
			<!-- 房间介绍 -->
			<van-dialog class="van_room" v-model="roomshow" :show-confirm-button="false">
				<img class="cancel_logo" @click="cancelEvent" src="/NewUI/images/room/ic_pop_close.png" />
				<div class="room_header">
					<div class="room_info">
						<img src="/NewUI/images/room/ic_pop_room_tip.png" />
					</div>
					<div class="tab_contain" ref="tab_contain">
						<ul class="sc_tab" ref="sc_tab">
							<li @click="roomtabEvent(index)" ref="item" class="item" :class="{item_active: roomindex === index}" v-for="(item,index) in homebetimgs"><span>{{item.title}}</span></li>
						</ul>
					</div>
				</div>
				<div class="commit" @click="commitEvent"></div>
			</van-dialog>
		</template>
	</div>
</body>
<!-- CDN -->
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.min.js"></script>	
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js"></script>
<script src="/Style/Old/js/hotcss.js"></script>
<script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
<script type="module">
	import {requestAjax,disposeDate,testdisposeDate} from '/Style/Old/js/common.js';
    var app = new Vue({
        el: '#home',
        data:{
			userInfo: {
			    userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
			    headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
			    id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
			    money: "<?php echo get_query_val("fn_user", "money", array('userid' => $_SESSION['userid'])); ?>",
			},
			
			homenums: ['/NewUI/images/home/ic_ball_0.png', '/NewUI/images/home/ic_ball_1.png', '/NewUI/images/home/ic_ball_2.png', '/NewUI/images/home/ic_ball_3.png', '/NewUI/images/home/ic_ball_4.png', '/NewUI/images/home/ic_ball_5.png'],
			homebetimgs: [
			    {
					type:'7',
			        g:'jssc',
			        bg: '/NewUI/images/home/ic_game_list_purple_bg.png',
			        logo: '/NewUI/images/home/ic_game_js_icon.png',
			        title: '极速赛车'
			    },
			   
			    {
					type:'12',
			        g:'jssm',
			        bg: '/NewUI/images/home/ic_game_list_purple_bg.png',
			        logo: '/NewUI/images/home/ic_game_ao10_icon.png',
			        title: '澳洲幸运10'
			    }
			],
			fooders: [
			    {src: '/NewUI/images/home/ic_game_room_intro.png', title: '房间介绍', id: '1'},
			    {src: '/NewUI/images/home/ic_game_service.png', title: '客服', id: '2'}
			],
			roomshow: false,
			// 房间选中下标
			roomindex:0,
			homelist:[],
			 // 根据类型type判断游戏
			 gameType:[
				{
				 	type:'7',
				 	game:'jssc'
				},
				  {
				    	type:'12',
				    	game:'jssm'
				  }	    
			 ],
			 //定时器
			  homeTimer:'',	
			//用于计算封盘的
			nowtimessss:0,//服务器系统时间
			addTimer:'',//服务器系统时间定时器
			serverTimer:'',//每五秒确认服务器系统时间，
		    
        },
		destroyed(){
			clearInterval(this.homeTimer)
			clearInterval(this.addTimer)
			clearInterval(this.serverTimer)
		},
		created:function(){
			this.nowtimessss = parseInt("<?= time() * 1000 ?>");
			this.addTimer = setInterval(()=>{
					this.nowtimessss += 1000;
					// console.log('----服务器系统时间------',this.nowtimessss,testdisposeDate(this.nowtimessss))
			},1000);
			this.serverTimer = setInterval(()=>{
					this.getNewTimeData()
					// console.log('----服务器test系统时间------',testdisposeDate(this.nowtimessss))
			},5000);
			
		},
        mounted() {
			
			this.getdata()
            // 监听window滚动事件
			window.addEventListener('scroll', this.handleScroll)
			$('#page_loading')[0].remove();
        },
        methods:{
			getNewTimeData(){
							let data = {
								f:'gameMsg',
								type:'1',
								roomid:'1'
							}
							let params={
								'type':'GET',
								'dataType':'json'
							}
							
							let Arr=[]
							for(let i=0;i<this.gameType.length;i++){
								
								if(this.game === this.gameType[i].game){
									
									data.type = this.gameType[i].type
									requestAjaxSyn('/Public/ShiroiInterface.php',data,params,(res)=>{
										if(res.data){					
												 this.nowtimessss = res.data.now_time*1000		 
										}
									},()=>{
										
									})	
								}
							}
						},
			
						getdata(){
							let data = {
								f:'gameMsg',
								type:'1',
								roomid:'1'
							}
							let params={
								'type':'GET',
								'dataType':'json'
							}
							let Arr=[]
							// console.log('------发送请求--------------')
							for (let i=0; i<this.homebetimgs.length;i++) {
								requestAjax('/Public/ShiroiInterface.php?',{
									f:'gameMsg',
									type:this.homebetimgs[i].type,
									roomid:'1'
								},params).then((res)=>{
										
										if(res.data){
											// let gameopen =  res.data.gameopen
											// // let gameopen = 'false' 
											// if(gameopen === 'false'){
											// 	console.log('---房间游戏状态----',typeof(gameopen),gameopen  )
											// 	let message = window.confirm('该房间处于关闭状态')
											// 	window.location.href = '/'
											// 	// console.log('---该房间处于关闭状态----' )
											// 	return false
											// }
											 Arr = res.data.code.split(",")
											 for(let i=0;i < Arr.length;i++){	 
												Arr[i] = parseInt(Arr[i])	 
											 }
											 res.data.nowResult = res.data.term
											 res.data.code = Arr
											 // res.data.gameopen = 'false'
											 res.data.currentTime =disposeDate(this.nowtimessss,res.data.distance_fengpan_time*1000)
											 // 定时器
											 res.data.homeTimer = ''
											 // 
											 // 动态赋值
											 if(res.data.type === this.homebetimgs[i].type){
											 		res.data.bg = this.homebetimgs[i].bg
													res.data.logo = this.homebetimgs[i].logo
													res.data.title = this.homebetimgs[i].title
											 }
											 // 封盘、开奖
											 res.data.sealingPlateShow = false
											  res.data.drawPrizeShow = false
											  // 封盘时间
											   let distanceFengpan=res.data.distance_fengpan_time*1000
											   if(this.nowtimessss-distanceFengpan> 0 && this.nowtimessss < distanceFengpan+res.data.fengpan*1000){
														 //处于封盘状态
														 res.data.fengpan = (distanceFengpan+res.data.fengpan*1000 -  this.nowtimessss)/1000
														 // console.log('------处于封盘状态的封盘时间--------',testdisposeDate(res.data.fengpan*1000))
													 }else if(this.nowtimessss > distanceFengpan+res.data.fengpan*1000 && this.nowtimessss < res.data.next_time_str){
														 // 处于开奖状态
														 // console.log('---------处于开奖状态的时间-----------')
														  res.data.fengpan = 0
													 }
											 // console.log('currentTime==',new Date().getTime(),res.data.next_time_str*1000)
											 console.log('----------this.homelist----------',this.homelist)
											 this.homelist.push(res)
											
										}
										 
									}).catch((error)=>{
										// console.log('error=====',error)
										vant.Toast('数据请求失败，请刷新重新加载!');
									})
							}
							// console.log('this.homelist==',this.homelist)
						},
						getLastData(items,index){
							let data = {
								f:'gameMsg',
								type:items.data.type,
								roomid:'1'
							}
							let params={
								'type':'GET',
								'dataType':'json'
							}
							let Arr=[]
							
										requestAjax('/Public/ShiroiInterface.php?',{
											f:'gameMsg',
											type:items.data.type,
											roomid:'1'
											
										},params).then((res)=>{
												
												// 根据是否同期判断是否已经获取下期结果
												if(items.data.nowResult != res.data.term ){
													
													items.data.drawPrizeShow = false
													
														 Arr = res.data.code.split(",")
														 for(let i=0;i < Arr.length;i++){	 
															Arr[i] = parseInt(Arr[i])	 
														 }
														 
														 res.data.code = Arr
														 res.data.nowResult = res.data.term
														 res.data.currentTime =disposeDate(this.nowtimessss,res.data.distance_fengpan_time*1000)
														 
														 res.data.sealingPlateShow= false
														 res.data.drawPrizeShow= false
														 res.data.homeTimer= ''
														 // 动态赋值
														 for(let i =0;i<this.homebetimgs.length;i++){
															 if(res.data.type === this.homebetimgs[i].type){
															 		res.data.bg = this.homebetimgs[i].bg
																	res.data.logo = this.homebetimgs[i].logo
																	res.data.title = this.homebetimgs[i].title
															 }
														 }
														 
														 
														  this.homelist.splice(index, 1, res)
														  
														  clearInterval(items.data.homeTimer)
														 
												}	
											}).catch((error)=>{
												// console.log('error=====',error)
											})	
						},
						
						//时间倒时结束
						
						homefinish(items,index){
							//    console.log('--------时间倒时结束-------------',items.data.sealingPlateShow)
							   items.data.sealingPlateShow = true
								setTimeout(()=>{
								 items.data.sealingPlateShow = false
								 items.data.drawPrizeShow = true
								 items.data.homeTimer=setInterval(
								 ()=>{
								 	this.getLastData(items,index)
								 }
								 ,1000)
								},items.data.fengpan*1000)	
						},
						
						
						userEvent(){
							window.location.href = "/Templates/New/mine.php"
						},
			            handleScroll() {
			                let offsetTop = document.querySelector('.content').offsetTop
			                console.log('offsetTop==', offsetTop)
			            },
			            homeTabEvent(index) {
			                if (index === 1) {
			                    window.location.href = '/Templates/Old/cuservice.php'
			                }
			                if (index === 0) {
			                    console.log('index==', index)
			                    this.roomshow = !this.roomshow
			                }
			            },
			            room(game,index,gameopen) {
							console.log('game====',game)
							for(let i=0; i < this.gameType.length;i++){
								if(game === this.gameType[i].type){
									console.log('gametype===',this.gameType[i].game)
									// return false
									if(gameopen === 'false'){
										return false
									}
									window.location.href = '/?room=<?= $roomid ?>&g='+this.gameType[i].game+'&agent=<?= $_SESSION['userid'] ?>';
								}
							}
			                
			            },
			            findKey(obj, value, compare = (a, b) => a === b) {
			                return Object.keys(obj).find(k => compare(obj[k], value))
			            },
						// 房间选中
						roomtabEvent(index){
							if(this.roomidnex){
								return false
							}
							this.roomindex = index;
			
							var bw = $(this.$refs.tab_contain)[0].clientWidth;
							var itemw = $(this.$refs.item[index])[0].clientWidth;
							var itemL = $(this.$refs.item[index]).position().left;
							var l = itemL - (bw - itemw)/2;
							$(this.$refs.tab_contain)[0].scrollTo({
			                    left: l,
			                    behavior: "smooth"
			                });
			
						},
						cancelEvent(){
							this.roomshow = false
						},
						commitEvent(){
							this.room(this.homebetimgs[this.roomindex]['type'],this.roomindex);
						}
        }
    })
</script>

</html>