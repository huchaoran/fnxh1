<!DOCTYPE >
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php echo $sitename ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="/NewUI/css/mui.min.css">
		<link href="/NewUI/css/common.css" rel="stylesheet"/>
		<link href="/NewUI/css/index.css" rel="stylesheet"/>
		<link href="/NewUI/font/iconfont.css" rel="stylesheet" />
		<!-- Bootstrap stylesheet -->
		<link rel="stylesheet" type="text/css" href="/Style/Old/css/bootstrap.min.css">
		<!-- ClockPicker Stylesheet -->
		<link rel="stylesheet" type="text/css" href="/Style/Old/css/bootstrap-clockpicker.min.css">
		<!-- vant css -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vant@2.9/lib/index.css"/>
	    <script src="/Style/Old/js/jquery.min.js"></script>
		<!-- bootstrap -->
		<script type="text/javascript" src="/Style/Old/js/bootstrap.min.js"></script>
	    <!-- vue vant js-->
	    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
	    <script src="https://cdn.jsdelivr.net/npm/vant@2.9/lib/vant.min.js"></script>
	    <!-- ClockPicker script -->
	    <script type="text/javascript" src="/Style/Old/js/bootstrap-clockpicker.min.js"></script>
	    <script src="/Style/Old/js/hotcss.js"></script>
	</head>
	<style>
		body,html{
			background-color: #fff;
		}
		#guessingreport{
			font-size: 16px;
		}
		/* header */
		.header{
			position: fixed;
		    top: 0;
			left: 0;
			right: 0;
			height: 44px;
			background-color: #35A8F1;
			line-height: 44px;
			text-align: center;
		}
		.header img{
			width: 15px;
			height: 15px;
			position: absolute;
			left: 10px;
			top: 50%;
			margin-top: -7.5px;
		}
		.header span{
			font-size: 16px;
		}
		/* content */
		.content{
			padding: 20px 20px 0 20px;
		}
		.content .room_num{
			margin-top: 44px;
			margin-bottom: 20px;
			color: #35A8F1;
		}
		.content .min_date{
			color:#BFBEBE ;
			padding: 0 0 10px 0;
			border-bottom: 1px solid #BFBEBE;
			margin-bottom: 20px;
		}
		.content .min_date span:nth-child(2){
			margin-left: 10px;
		}
		.content .max_date{
			color:#BFBEBE ;
			padding: 0 0 10px 0;
			border-bottom: 1px solid #BFBEBE;
		}
		.content .max_date span:nth-child(2){
			margin-left: 10px;
		}
		.content .remarks{
			margin-top: 5px;
			color: #35A8F1;
		}
		.content .date_tab{
			display: flex;
			flex-direction: row;
			justify-content: space-between;
			padding: 20px 0;
			
		}
		.content .date_tab .item{
			color: #999999;
			padding: 10px;
			border-radius: 5px;
			font-size: 13px;
		}
		.content .query{
			text-align: center;
			background-color:#35A8F1;
			padding:10px 0 ;
			border-radius: 18px;
			color: #fff;
		}
		/* tab选择事件 */
		.content .date_tab .date_active{
			background-color:#35A8F1;
			color:#fff;
			
		}
		/* 日历 header*/
		.cus_header{
			display: flex;
			flex-direction: row;
			background-color: #fff;
			height: 60px;
			align-items: center;
			margin-top: 100px;
			
		} 
		.cus_header  .mon_date{
			font-size: 24px;
			padding: 0 0 0 30px;
		}
		.cus_header .year_lunar{
			display: flex;
			flex-direction: column;
			
		}
		.testin{
			border: 1px solid red;
			width: 100px;
			height: 50px;
		}
		.test1{
			height: 50px;
			width: ;
			background-color: red;
		}
	</style>
	<body>
		<div id="guessingreport">
			<div class="header">
				<a href="javascript:history.go(-1)"><img src="/NewUI/images/public/ic_back.png" /></a>
				<span>竞猜报表</span>
			</div>
			<!-- content -->
			<div class="content">
				<!-- <input onchange="aa()"  class="testin"/> -->
				<div class="room_num"><span>房间号:</span><span>394555</span></div>

				<div  @click.stop="doSomething($event)" class="min_date" id="min_date"><span>{{minDate.year}}/{{minDate.mon}}/{{minDate.day}}</span><span>{{minDate.hour}}:{{minDate.min}}</span></div>

				<div @click="maxdateEvent" class="max_date"><span>2020/07/01</span><span>06:00</span></div>
			    <div class="remarks"><span>备注:选择时间按钮,自动回填上面日期</span></div> 
			    <!-- tab -->
				<div class="date_tab">
					<div class="item" :class="{date_active: tab_index === index}" @click="dateselectEvent(index)" v-for="(item,index) in dateTabs" :key="item">
						<span>{{item}}</span>
					</div>
				</div>
				<!-- 查询 -->
				<div class="query"><span>查询</span></div>
				<!-- 最小日期选择 -->
				<van-overlay :show="minShow" >
					<div class="cus_header">
						<div class="mon_date">
							<span>{{minDate.mon}}月{{minDate.day}}日</span>
						</div>
						<div class="year_lunar">
							<span>{{minDate.year}}</span>
							<span>{{minDate.lunar}}</span>
						</div>
					</div>
					<!-- 日历 -->
					<div class="calendar_bg" @click.stop="doSomething11($event)">
						<van-calendar  :min-date="historyDate" :poppable="false" :formatter="formatter"  @confirm="minformatterEvent"   :show-subtitle="false" :show-title="false" :show-confirm="false"  :style="{ height: '400px' }"  v-if="minCalendarShow"  v-model="minCalendarShow" :show-confirm="false"   >
						</van-calendar>
					</div>
						
					<!-- 时钟 -->
					<input type="text" class="form-control" id="clickMe" value="" placeholder="Now">
					<!-- <button type="button" id="check-minutes" @click="aa($event)">Check the minutes</button> -->
					
					   
					 
					</div>
				</van-overlay>	
				<!-- 最大日期选择 -->
				<!-- <van-overlay :show="maxShow" @click="maxShow = false">
					<div class="cus_header">
						<div class="mon_date">
							<span>{{maxDate.mon}}月{{maxDate.date}}日</span>
						</div>
						<div class="year_lunar">
							<span>{{maxDate.year}}</span>
							<span>{{maxDate.lunar}}</span>
						</div>
					</div>
					<!-- 日历 -->
					<!-- <van-calendar :min-date="historyDate" :formatter="formatter"  @confirm="maxformatterEvent"   :show-subtitle="false" :show-title="false" :show-confirm="false"  :style="{ height: '400px' }" :poppable="false" v-model="overlayshow" :show-confirm="overlayshow" >
					</van-calendar>	
				</van-overlay> -->
				
				
				<!-- Or just a input -->
				<!-- <input id="demo-input" /> -->
				
				<!-- <div class="clearfix">
						<div class="pull-center clearfix" style="margin-bottom:10px;">
							<input class="form-control pull-right" id="single-input" @focus="test11" value=""  placeholder="Now"/>
						</div>
				</div> -->
			</div>
		</div>
	</body>
    
    </script>
	<script type="module">
		import calendar from '/Style/Old/js/calendar.js';
		
		// $('.calendar_input').click=function(){
		// 	console.log('input')
		// }
		var calendar_input = document.getElementById('calendar_input')
		// var clickMe = document.getElementById('clickMe')
		// 针对btn阻止冒泡事件的
		var input = $('#clickMe').clockpicker({
		    placement: 'bottom',
		    align: 'left',
		    autoclose: true,
		    'default': 'now'
		});
		var app = new Vue({
			el:'#guessingreport',
			data:{
				dateTabs:['昨天','今天','本周','上周','本月','上月'],
				tab_index:0,
				minShow:false,
				maxShow:false,
				// 最小日期
				minDate:{
					year:'2020',
					mon:'07',
					day:'01',
					hour:'06',
					min:'00',
					lunar:'初四'
				},
				// minShow:false,
				// 最大日期
				maxDate:{
					year:'2020',
					mon:'07',
					day:'04',
					hour:'06',
					min:'00',
					lunar:'初四'
				},
				historyDate: new Date(2010, 0, 1),
				overlayshow:false,
				minCalendarShow:false
				
			},
			methods:{
				doSomething(e){
					e.stopPropagation();
					
					this.minShow = true
					this.minCalendarShow = true 
						// input.clockpicker('show')
						//         .clockpicker('toggleView', 'minutes');
					// console.log('vue取消冒泡')
				},
				doSomething11(e){
					console.log('vue11取消冒泡')
					// e.stopPropagation();
					e.stopPropagation();
					input.clockpicker('show')
					        .clockpicker('toggleView', 'minutes');
				},
				// tab选择事件
				
				aa(e){
					e.stopPropagation();
					input.clockpicker('show')
					        .clockpicker('toggleView', 'minutes');
					console.log('sadada')
				},
				// test11(){
				// 	console.log('123')
				// },
				dateselectEvent(index){
					console.log('index---',index)
					if(this.tab_index === index){
						return false
					}
					this.tab_index = index
				},
				// 日期格式化
				formatter(day) {
					   const year = day.date.getFullYear();
				       const month = day.date.getMonth() + 1;
				       const date = day.date.getDate();
				       let lunar = calendar.solar2lunar(year,month,date) 
					    // console.log('lunar------',lunar)
				      day.bottomInfo = lunar.IDayCn
				       return day;
				},
				// 日历选中的日期事件
				   //最小日期选择事件
				minformatterEvent(date){
					this.minShow = true
					// console.log('minShow====',this.minShow)
					// e.stopPropagation()
					 // this.datetime = this.formatDate(day)
					 // console.log('day===',this.formatDate(day))
					 this.minDate.year = `${date.getFullYear()}`
					 this.minDate.mon = `${date.getMonth() + 1}`
					 this.minDate.day = `${date.getDate()}`
					 let lunar = calendar.solar2lunar(this.minDate.year,this.minDate.mon,this.minDate.day)
					 this.minDate.lunar = lunar.IDayCn
					 this.minCalendarShow = false
					 // 触发时钟
					 if(!this.minCalendarShow){
						 // $('.calendar_input').focus()
						 // (function(){
							//  console.log('calendar_input方法触发')
						 // })
						 // $('.calendar_input').clockpicker()
						 // this.test()
						 console.log('clockpicker')
					 }
					
					 // var input = $('#min_date').clockpicker({
					 // 	placement: 'bottom',
					 // 	align: 'left',
					 // 	autoclose: true,
					 // 	'default': 'now'
					 // });
					 console.log('最小')
					 // console.log('lunar------',lunar.IDayCn)
				},
				maxformatterEvent(date){
					this.maxDate.year = `${date.getFullYear()}`
					this.maxDate.mon = `${date.getMonth() + 1}`
					this.maxDate.day = `${date.getDate()}`
					let lunar = calendar.solar2lunar(this.maxDate.year,this.maxDate.mon,this.maxDate.day)
					this.maxDate.lunar = lunar.IDayCn
					console.log('最大')
				},
				   // 最小日期btn事件
				   mindateEvent(e){
					   this.minShow = true  
					   this.minCalendarShow = true 	
					   // e.stopPropagation();
					   // input.clockpicker('show')
					           // .clockpicker('toggleView', 'minutes');
					   // calendar_input.clockpicker()
					   // console.log(calendar_input.clockpicker())
				   },
				   maxdateEvent(){
				   	   this.maxShow = true   
				   },
				   // 针对input事件的
				   test(){
					   
					   var input = $('#clickMe').clockpicker({
					       placement: 'bottom',
					       align: 'left',
					       autoclose: true,
					       'default': 'now'
					   });
					   // console.log('12131')
				   }
			},
			mounted(){
				// this.test()
			}
		})
	</script>
</html>