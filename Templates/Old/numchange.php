<?php include_once(dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php");
?>
<?php $usermoney = get_query_val('fn_user', 'money', array('userid' => $_SESSION['userid'])); ?>

<!DOCTYPE >
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php echo $sitename ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="/NewUI/css/mui.min.css">
		<link href="/NewUI/css/common.css" rel="stylesheet"/>
		<link href="/NewUI/css/index.css" rel="stylesheet"/>
		<link href="/NewUI/font/iconfont.css" rel="stylesheet" />
		<!-- vant css -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vant@2.9/lib/index.css"/>
		<link rel="stylesheet" href="/NewUI/css/numchange.css"/>
		<script src="/Style/Old/js/jquery.min.js"></script>
		<!-- vue vant js-->
		
		<script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/vant@2.9/lib/vant.min.js"></script>
		<script src="/Style/Old/js/hotcss.js"></script>
	    <style>
			/* .van-calendar__day{
				color: red;
			} */
			
			
		</style>
	</head>
	<body>
		<script type="text/javascript">
		    var info = {
		        'nickname': "<?php echo $_SESSION['username'] ?>",
		        'headimg':"<?php echo $_SESSION['headimg'] ?>",
		        'userid':"<?php echo $_SESSION['userid'] ?>",
		        'roomid':"<?php echo $_SESSION['roomid'] ?>",
		        'game': "<?php echo $_SESSION['game'];
		            ?>"
		    };
			
		    console.log(info);
		   
		
		</script>
		
		
		<div id="numchange">
			
			<template>
				<div class="num_header">
					<a href="javascript:history.go(-1)"><img src="/NewUI/images/public/ic_back.png" /></a>
					<span>积分账变</span>
				</div>
				<div class="date_choice">
					<div class="min_max_date">
						<div class="min_date" @click="minEvent">{{mindatetime.year}}/{{mindatetime.mon}}/{{mindatetime.day}}</div>
						<div class="util"><span>至</span></div>
						<div class="max_date" @click="maxEvent">{{maxdatetime.year}}/{{maxdatetime.mon}}/{{maxdatetime.day}}</div>
					</div>
					<div class="type_query">
						<div class="type" @click="selecttyleEvent">
							<span id="seleteTypeID">{{seleteType}}</span>
							<img src="/NewUI/images/public/ic_drop_down_icon.png" />
						</div>
						<div class="query" @click="queryEvent">查询</div>
					</div>
				</div>
				<!-- 时间  变动记录  变动金额  变动后金额 -->
				<div class="list_th" >
					<span class="item" v-for="(item,index) in listths" :key="item">{{item}}</span>
				</div>
				<!-- list -->
				
				<div class="list_wrap">
						<van-pull-refresh v-model="isDownLoading" @refresh="onDownRefresh">
							<van-list
								v-model="isUpLoading"
								:finished="isUpfinished"
								finished-text="没有更多了"
								@load="onUpLoad"
								:offset="50"
								:immediate-check="false"
							>
									  <div class="list">
										  <div class="list_item" v-for="(item,index) in numChangArr" :key="index">
												<span><b>{{item.addtime}}</b></span>
												 <span><b>{{item.type}}</b></span>
												<span :class="{'red':item.money>0,'green':item.money<0}"><b>{{item.money}}</b></span>
												<span>{{item.totalnums}}</span>
										  </div>
									  </div>
									
							</van-list>
						</van-pull-refresh>
				</div>
				
				<!-- <div class="btn" @click="btnEvent">{{datetime.mon}}月{{datetime.day}}日</div> -->
				<!-- 最小日期选择 -->
				<!-- <van-overlay :show="minShow" @click="minShow = false"> -->
				<div v-show="minShow" @click="minShow = false" class="vant_overlay">
					<div class="cus_header">
						<div class="mon_date">
							<span>{{mindatetime.mon}}月{{mindatetime.day}}日</span>
						</div>
						<div class="year_lunar">
							<span>{{mindatetime.year}}</span>
							<span>{{mindatetime.lunar}}</span>
						</div>
					</div>
					<van-calendar :min-date="historyDate" :formatter="formatter"  @confirm="minformatterEvent"   :show-subtitle="false" :show-title="false" :show-confirm="false"  :style="{ height: '400px' }" :poppable="false" v-model="overlayshow" :show-confirm="overlayshow" >
					</van-calendar>
				</div>
					
				<!-- </van-overlay>	 -->
				<!-- 最大日期选择 -->
				<!-- <van-overlay :show="maxShow" @click="maxShow = false"> -->
				<div v-show="maxShow" @click="maxShow = false" class="vant_overlay">
					<div class="cus_header">
						<div class="mon_date">
							<span>{{maxdatetime.mon}}月{{maxdatetime.day}}日</span>
						</div>
						<div class="year_lunar">
							<span>{{maxdatetime.year}}</span>
							<span>{{maxdatetime.lunar}}</span>
						</div>
					</div>
					<van-calendar :min-date="historyDate" :formatter="formatter"  @confirm="maxformatterEvent"   :show-subtitle="false" :show-title="false" :show-confirm="false"  :style="{ height: '400px' }" :poppable="false" v-model="overlayshow" :show-confirm="overlayshow" >
					</van-calendar>	
				<!-- </van-overlay> -->
				</div>
					<!-- 全部  类型 -->
					
					<van-overlay :style="{background: 'rgb(0, 0, 0, 0)'}" :show="selectShow" @click="selectShow = false" >
						<div class="other_record_select"   >
							<div class="interval">
								<div class="item" v-for="(item,index) in otherRecords" :key="item" @click="selectTypeEvent(item)">
									<!-- <span class="title">{{item}}</span> -->
									{{item.title}}
								</div>
							</div>	
						</div>
					</van-overlay>
				
			</template>
				
		</div>
	</body>
	<script type="module">
		import calendar from '/Style/Old/js/calendar.js'
		import {getParameter,requestAjax,disposeDate} from '/Style/Old/js/common.js'
		// import index from '/Style/Old/js/index.js'
		var app = new Vue({
			el:'#numchange',
			data:{
				// user money
				money:0,
				//处理上下拉刷新
				numChangArr:[],
				isDownLoading:false,//下拉刷新
				isUpLoading:false,//上拉加载
				isUpfinished:false ,//加载完成
				counts:0,//当前数据条数	
				page:1,// 当前页数
				game:'',
				startTime:'',
				endTime:'',
				overlayshow:false,
				// 日期
				mindatetime:{
					year:'2020',
					mon:'07',
					day:'24',
					lunar:"初四"
				},
				maxdatetime:{
					year:'2020',
					mon:'07',
					day:'24',
					lunar:"初四"
				},
				historyDate: new Date(2010, 0, 1),
				// minDate:new Date(2020,07,24),
				// maxDate:new Date(2020,07,24),
				listths:['时间','变动记录','变动金额','变动后金额'],
				// 日期选择
				minShow:false,
				maxShow:false,
				// 全部类型选择框
				selectShow:false,
				seleteType:'全部',
				
				// vanrecordshow:false,
				// 全部 选择类型
				otherRecords:[
					{
						title:'极速赛车',
						alias:'jssc'
					},
					{
						title:'澳洲幸运10',
						alias:'jssm'
					}
					]
				
			},
			computed:{
				// money:()=>{
					
				// }
			},
			mounted(){
				console.log('money----',"<?=$usermoney?>")
				// console.log('1233')
				let  game = info.game
				for(let i =0;i<this.otherRecords.length;i++){
					if(game === this.otherRecords[i].alias){
						this.seleteType = this.otherRecords[i].title
					}
				}
				
				this.money = "<?=$usermoney?>"
				this.initDateTime()
				// this.getDate(); 
				// 初始化日期时间
				
				this.startTime = this.mindatetime.year+'-'+this.mindatetime.mon+'-'+(this.mindatetime.day-1)
				this.endTime = this.maxdatetime.year+'-'+this.maxdatetime.mon+'-'+(this.maxdatetime.day)
				// console.log('---------',this.startTime,this.endTime)
				// this.getData(this.startTime,this.endTime,this.seleteType,this.page)
			},
			methods:{
				// 初始化日期时间
				initDateTime(){
					var datetime = new Date();
					// console.log('-------',this.mindatetime)
					var year = datetime.getFullYear(); 
					var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1; 
					var date = datetime.getDate() < 10 ? "0" : datetime.getDate(); 
					 this.mindatetime.year = year
					 
					this.mindatetime.mon = month
					this.mindatetime.day = date
					this.maxdatetime.year = year
					this.maxdatetime.mon = month
					this.maxdatetime.day = date
					
				},
				// 请求数据
				getData(startTime,endTime,game,page){
					let data={}
					if(game==='全部'){
						 data = {
							f:'getAccountChange',
							userid:info.userid,
							startTime,
							endTime,
							page,
							game:'',
							limit:10
						}
					}else{
						 data = {
							f:'getAccountChange',
							userid:info.userid,
							startTime,
							endTime,
							game,
							page,
							limit:10
						}
					}
					
					let param = {
						type:'GET',
						dataType:'json'
					}
					let Arr=[]
					console.log('param-----',data)
					
					requestAjax('/Public/ShiroiInterface.php',data,param).then((res)=>{
						if(res.data){
							console.log('numChangArr-----',res)
							this.counts = res.data.count
							 // for(let i =0;i<res.data.data.length;i++){
								 // res.data.data[i].isResults = false
								 // console.log('dasdad')
								   
							 // }
							  // console.log('数据加载完成',this.numChangArr.length)
							 if(this.numChangArr.length >= this.counts){
								 console.log('数据加载完成',this.numChangArr.length)
								 this.isUpfinished = true;
								 return false
							 }
							 console.log('----------page------',this.page)
							 if (this.page === 1) {
								console.log('-----等于1----',this.page)
								
								this.numChangArr=res.data.data
							 } else {
								 console.log('-----大于1----',this.page,)
							  this.numChangArr.push(...res.data.data) 
							 }  
							 for(let i=0;i<this.numChangArr.length;i++){
							 	if(this.numChangArr[i].money>0){
							 		// console.log('大于0')
							 		// res.data[i].totalnums =this.money.toFixed(2)+res.data[i].money.toFixed(2)
							 		// this.money =pthis.money.toFixed(2)+res.data[i].money.toFixed(2)
							 		this.numChangArr[i].totalnums =parseFloat(this.money)+parseFloat(this.numChangArr[i].money)   
							 		this.money =(parseFloat(this.money)+parseFloat(this.numChangArr[i].money)).toFixed(2)
							 	}else{
							 		// console.log('小于0')
							 		this.numChangArr[i].totalnums = this.money - Math.abs(this.numChangArr[i].money)
							 	    this.money =(this.money - Math.abs(this.numChangArr[i].money)).toFixed(2)
							 	}
							 	// res.data.totalnums = this.money res.data[i]
							 }
						     // console.log('numChangArr====',this.numChangArr)
						}
						
					}).complete(()=>{
						console.log('请求完成')
						this.isDownLoading = false
						this.isUpLoading = false    
					})
							
					
				},
				formatDate(date) {
                      return `${date.getMonth() + 1}/${date.getDate()}`;
				},
				btnEvent(){
					this.overlayshow = !this.overlayshow
				},
				 // formatter(day){
					 // console.log('day===',day)
				 // }
				 // 日历选中的日期事件
				    //最小日期选择事件
				 minformatterEvent(date){
					 // this.datetime = this.formatDate(day)
					 // console.log('day===',this.formatDate(day))
					 this.mindatetime.year = `${date.getFullYear()}`
					 this.mindatetime.mon = `${date.getMonth() + 1}`
					 this.mindatetime.day = `${date.getDate()}`
					 let lunar = calendar.solar2lunar(this.mindatetime.year,this.mindatetime.mon,this.mindatetime.day)
					 this.mindatetime.lunar = lunar.IDayCn
					 // console.log('lunar------',lunar.IDayCn)
				 },
				     //最大日期选择事件
				 maxformatterEvent(date){
					 // this.datetime = this.formatDate(day)
					 // console.log('day===',this.formatDate(day))
					 this.maxdatetime.year = `${date.getFullYear()}`
					 this.maxdatetime.mon = `${date.getMonth() + 1}`
					 this.maxdatetime.day = `${date.getDate()}`
					 let lunar = calendar.solar2lunar(this.maxdatetime.year,this.maxdatetime.mon,this.maxdatetime.day)
					 this.maxdatetime.lunar = lunar.IDayCn
					 // console.log('lunar------',lunar.IDayCn)
				 },
				 formatter(day) {
					   const year = day.date.getFullYear();
				       const month = day.date.getMonth() + 1;
				       const date = day.date.getDate();
				       let lunar = calendar.solar2lunar(year,month,date) 
					    // console.log('lunar------',lunar)
				      day.bottomInfo = lunar.IDayCn
				       return day;
				},
				// 选择最小日期类型
				minEvent(){
					console.log('选择最小日期')
					this.minShow = true
				},
				// 选择最大日期类型
				maxEvent(){
					console.log('选择最大日期')
					this.maxShow = true
				},
				// 选择类型
				selecttyleEvent(){
					console.log('选择类型')
					this.selectShow = !this.selectShow
				},
				// 查询
				queryEvent(){
					
					this.numChangArr = []
					console.log('查询')
					// this.getData()
					this.onDownRefresh()
				},
				// 类型选择
				selectTypeEvent(item){
					if(item.length>2){
						console.log('2132132')
						$('#seleteTypeID').css({'text-align':'left'})
					}
					
					console.log('item--',item)
					this.seleteType = item.title
				},
				// 上下拉刷新、加载
				onDownRefresh(){
					this.numChangArr= []
					this.page = 1
					this.isUpfinished = false
					// 将loading 设置为true,表示处于加载状态
					// this.isUpLoading = true
					
					this.startTime = this.mindatetime.year+'-'+this.mindatetime.mon+'-'+(this.mindatetime.day-8)
					this.endTime = this.maxdatetime.year+'-'+this.maxdatetime.mon+'-'+(this.maxdatetime.day)
					this.getData(this.startTime,this.endTime,this.seleteType,this.page)
				},
				onUpLoad(){
					this.page++;
					if(this.isDownLoading){
						this.numChangArr = []
						this.isDownLoading= false
					}
					this.startTime = this.mindatetime.year+'-'+this.mindatetime.mon+'-'+(this.mindatetime.day-8)
					this.endTime = this.maxdatetime.year+'-'+this.maxdatetime.mon+'-'+(this.maxdatetime.day)
					//请求complete 处理上拉状态为false
					this.getData(this.startTime,this.endTime,this.seleteType,this.page)
					
					// 判断数据是否加载完成
					// if(this.numChangArr.length > this.counts){
					// 	this.isUpfinished = true;
					// }
				}
				
			}
		})
	</script>
</html>