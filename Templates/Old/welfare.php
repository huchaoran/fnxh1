<?php include_once(dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php");
?>
<!DOCTYPE >
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php echo $sitename ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="/NewUI/css/mui.min.css">
		<link href="/NewUI/css/common.css" rel="stylesheet"/>
		<link href="/NewUI/css/index.css" rel="stylesheet"/>
		<link href="/NewUI/font/iconfont.css" rel="stylesheet" />
		<!-- vant css -->
		<link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
		
		<script src="/Style/Old/js/jquery.min.js"></script>
		<!-- vue vant js-->
		<script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
		<script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
		<style>
			body,html{
				background-color: #fff;
			}
			/* .van-nav-bar{
				background-color: #35A8F1;
				
			}
			.van_header{
				color: #fff;
			}
			.van-nav-bar__title{
				color: #fff;
			} */
			#game_record{
				padding: 10px;
				display: flex;
				flex-direction: column;
				justify-content: center;
			}
			/* header */
			.header{
				position: fixed;
				top: 0;
				left: 0;
				right: 0;
				height: 44px;
				color: #fff;
				font-size: 16px;
				background-color: #35A8F1;
				display: flex;
				flex-direction: row;
				justify-content: space-between;
				align-items: center;
			}
			.header .title {
				/* position: absolute;
				left: 0;
				right: 0;
				left: 50%;
				margin-left: -50%;
				top: 50%;
				margin-top: -50%; */
				/* width: 100%; */
				 flex-grow: 1; 
				text-align: center;
				/* background-color: #000000; */
			}
			 .header .header_back{
				 /* position: relative; */
				 /* width: 30px; */
				/* height: 44px;
				 line-height: 44px;
				 background-color: #000000; */
				 width: 25%;
				 margin-left: 10px;
			 }
			 .header .header_back img{ 
				/* position: absolute;
				left: 10px;
				top: 50%;
				margin-top: -7.5px; */
				height: 15px;
				width: 15px; 
			 } 
			 .header .other_record{
				 width: 25%;
				 /* width: 100px; */
				 margin-right: 10px;
				 font-size: 15px;
				 text-align: right;
			 }
			 .header .other_record img{
				 width: 15px;
				 height: 15px;
			 }
		/* 其他记录下拉 */
		.other_record_select{
			position: absolute;
			top: 42px;
			right: 0;
			width: 30%;
			display: flex;
			flex-direction: column;
			bottom: 0;
			padding: auto 10px;
			/* margin: 0 10px; */
			/* border-radius: 10px; */
			/* border-width: 2px solid #000 ;
			background-color:blue ; */
			/* box-sizing: border-box; */
		}
		.other_record_select .interval{
			/* margin: 0 5px; */
			background-color: #fff;
			padding: 0 5px;
			/* background-color:blue ; */
			border: 1px solid #E3E3E3 ;
			border-radius: 5px;
		}
		.other_record_select .interval .item{
			/* height: 33px; */
			/* width: 30%; */
			/* width: 90%; */
			color:#222222 ;
			/* margin: 5px; */
			padding: 15px;
			background-color: #fff;
			text-align: center;
			font-size: 14px;
			text-align: center;
			border-bottom: 1px solid #E3E3E3 ;
			 ;
			/* margin: auto 5px; */
			/* box-sizing: border-box; */
			
			
		}
		/* 内容部分 */
		.content{
			
		}
		
		</style>
	</head>
	<body>
		<div id="game_record">
			<template>
				<!-- <div class="header">
						<img src="/NewUI/images/public/ic_back.png" />
						<van-nav-bar class="van_header" title="极速赛车"  >
						  <template #right>
						    <div class="other_record">
								<span>其他记录</span>
								<img src="/NewUI/images/public/ic_arrow_right.png"/>
							</div>
						  </template>
						</van-nav-bar>
					</div> -->
					<div class="header">
						<div class="header_back" >
							<a href="javascript:history.go(-1)"><img src="/NewUI/images/public/ic_back.png" /></a>
							
						</div>
						<div class="title"><span>福利记录</span></div>
						<div class="other_record" @click="otherRecordEvent">
							<span>其他记录</span>
							<img src="/NewUI/images/public/ic_arrow_right.png" />
						</div>
						<!-- 其他记录对话框 -->
						
						<van-overlay :style="{background: 'rgb(0, 0, 0, 0)'}" :show="vanrecordshow" @click="vanrecordshow = false" >
							<div class="other_record_select"   >
								<div class="interval">
									<div class="item" v-for="(item,index) in otherRecords" :key="item">
										<!-- <span class="title">{{item}}</span> -->
										{{item}}
									</div>
								</div>
								
							</div>
						</van-overlay>
						
					</div>
					<div class="content">
						
					</div>
				
			</template>
		</div>
	</body>
	<script type="text/javascript">
		var app = new Vue({
			el:'#game_record',
			data:{
				test:'123',
				otherRecordShow:false,
				otherRecords:['分红','红包','回水','全部'],
				vanrecordshow:false
			},
			methods:{
				otherRecordEvent(){
					// this.otherRecordShow = !this.otherRecordShow
					this.vanrecordshow = !this.vanrecordshow
				}
			}
		})
	</script>
</html>