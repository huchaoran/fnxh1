<?php include_once(dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php echo $sitename ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" />
	    <link rel="stylesheet" href="/NewUI/css/mui.min.css">
	    <link href="/NewUI/css/common.css" rel="stylesheet"/>
	    <link href="/NewUI/css/index.css" rel="stylesheet"/>
	    <link href="/NewUI/font/iconfont.css" rel="stylesheet" />
		<!-- 原本客服样式 -->
		<link rel="Stylesheet" type="text/css" href="/Style/Old/css/style.css">
		<link rel="Stylesheet" type="text/css" href="/Style/Old/css/style2.css">
		<!-- vant css -->
		<link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
	    
		<script src="/Style/Old/js/jquery.min.js"></script>
		<!-- vue vant js-->
	    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
	    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
	    <script src="/Style/Old/js/kefu.js"></script>
		<style>

			
		</style>
	</head>
	<body>
		<div id="cuservice">
			<div class="kefu_header">
				<a href="javascript:history.go(-1)">
				    <img  src="/NewUI/images/chat/ic_back.png"/>
				</a>
				<span>客服</span>
			</div>
			<!-- 聊天信息 -->
			<div id="messageWindow" class="messageWindow">
				<div id="messageLoading">Loading...</div>
			</div>
			<!-- 聊天输入框 -->
			<div class="chat_input">
				<div class="fooder_input">
					<input id="msg" placeholder="点击输入"  v-model="chatvalue" @change="aa()"></input>
				</div>
				
				<van-button id="butSend" class="chat_send" type="info">发送</van-button>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		var app = new Vue({
			el:'#cuservice',
			data:{
				test:'123',
				chatvalue:''
			},
			methods:{
				aa(){
					console.log('aa')
				}
			}
			
		})
	</script>
</html>