<?php include_once(dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php");
?>
<!DOCTYPE >
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php echo $sitename ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="/NewUI/css/mui.min.css">
		<link href="/NewUI/css/common.css" rel="stylesheet"/>
		<link href="/NewUI/css/index.css" rel="stylesheet"/>
		<link href="/NewUI/font/iconfont.css" rel="stylesheet" />
		<!-- vant css -->
		<link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
		
		<script src="/Style/Old/js/jquery.min.js"></script>
		<!-- vue vant js-->
		<script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
		<script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
	    <script src="/Style/Old/js/hotcss.js"></script>
		<style>
			body,html{
				background-color: #fff;
			}
			#report_query{
				margin: 0 0 0 15px;
				background-color: #fff;
				display: flex;
				flex-direction: column;
				font-size: 16px;
			}
			.header{
				position: fixed;
				display: flex;
				
				top: 0;
				left: 0;
				right: 0;
				height: 44px;
				justify-content: center;
				align-items: center;
				background-color: #35A8F1;
			}
			.header a img{
				position: absolute;
				left: 15px;
				top: 50%;
				margin-top: -7.5px;
				width: 15px;
				height: 15px;
			}
			.header span{
				font-size: 16px;
			}
			/* list */
			.list{
				flex:1;
				margin-top: 44px;
				display: flex;
				flex-direction: column;
			}
			.list .item{
				display: flex;
				flex-direction: row;
				border-bottom: 1px solid #E9E9E9;
				padding: 10px;
				justify-content: space-between;
				align-items: center;
				
			}
			.list .item:nth-child(1){
				margin-top: 10px;
			}
			.list .item .logo_title{
				text-align: center;
			}
			.list .item .logo_title img{
				width: 20px;
				height: 20px;
			}
			.list .item .logo_title span{
				/* color: #AEAEAE; */
				color: #222222;
				padding-left:10px ;
				font-size: 18px;
			}
			.list .item .list_go img{
				width: 15px;
				height: 15px;
				
			}
		</style>
	</head>
	<body>
		<div id="report_query">
			<!-- header -->
			<div class="header">
				<a href="javascript:history.go(-1)"><img src="/NewUI/images/public/ic_back.png" /></a>
				<span>报表查询</span>
			</div>
			<!-- list -->
			<div class="list">
				<div class="item" v-for="(item,index) in reports" :key="item.id" @click="listEvent(index)">
					<div class="logo_title">
						<img :src=item.src />
						<span>{{item.title}}</span>
					</div>
					<div class="list_go">
						<img src="/NewUI/images/public/ic_item_arow_right.png" />
					</div>
					
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		var app = new Vue({
			el:'#report_query',
			data:{
				reports:[
					{
						src:'/NewUI/images/report/ic_guess_table.png',
						title:'竞猜报表',
						id:'0'
					},
					{
						src:'/NewUI/images/report/ic_room_menu_back_money.png',
						title:'福利报表',
						id:'1'
					},
					{
						src:'/NewUI/images/report/ic_redbag_table.png',
						title:'红包报表',
						id:'2'
					},
					{
						src:'/NewUI/images/report/ic_integal_tables.png',
						title:'积分报表',
						id:'3'
					}
				]
			},
			methods:{
				listEvent(index){
					// 竞猜报表
					if(index === 0){
						console.log('竞猜报表')
						window.location.href = '/Templates/Old/guessingreport.php'
					}
					// 福利报表
					if(index === 1){
						console.log('福利报表')
						window.location.href = '/Templates/Old/welfare.php'
					}
					// 红包报表
					if(index === 2){
						console.log('红包报表')
						window.location.href = '/Templates/Old/redenvelopes.php'
					}
					// 积分报表
					if(index === 3){
						window.location.href = '/Templates/Old/numchange.php'
						console.log('积分报表')
					}
				}
			}
		})
	</script>
</html>