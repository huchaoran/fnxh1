<?php

if($_POST){
	if(empty($_POST['user'])||empty($_POST['pass'])){
		$json['status']=2;
		$json['msg']='请输入完整信息！';
		echo json_encode($json);exit;		
	}
	select_query("fn_user", '*', array('loginuser'=>$_POST['user'], 'loginpasswd'=>md5($_POST['pass'])));
	$userinfo = db_fetch_array();
	if($userinfo){
		$_SESSION['userid'] = $userinfo['userid'];
		$_SESSION['username'] = $userinfo['username'];
		$_SESSION['headimg'] = $userinfo['headimg'];
		$_SESSION['roomid'] = $userinfo['roomid'];
		$json['status']=1;
		$json['msg']=$userinfo['roomid'];
		// Header("Location:/"); 
		echo json_encode($json);exit;	
	}else{
		$json['status']=2;
		$json['msg']='帐号或密码错误！';
		echo json_encode($json);exit;	
	}
}
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="favicon.ico" />
<title>登录页</title>
<script src="/Templates/home/js/jquery.min.js"></script>


  
 
  
  
  
<link rel="stylesheet" type="text/css" href="/Templates/home/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="/Templates/home/css/demo.css" />
<!--必要样式-->
<link rel="stylesheet" type="text/css" href="/Templates/home/css/component.css" />
<link rel="stylesheet" type="text/css" href="/Templates/home/css/toastr.css">
</link>
<script type="text/javascript" src="/Templates/home/js/toastr.min.js"></script>
<!--[if IE]>
    <script src="/Templates/home/js/html5.js"></script>



    <![endif]-->
</head>


 


<body data-genuitec-lp-enabled="false" data-genuitec-file-id="wc1-7" data-genuitec-path="/game-http-client/WebRoot/login.html">
<div class="container demo-1" data-genuitec-lp-enabled="false" data-genuitec-file-id="wc1-7" data-genuitec-path="/game-http-client/WebRoot/login.html">
    <div class="content">
        <div id="large-header" class="large-header">
            <canvas id="demo-canvas"></canvas>
            <div class="logo_box"
					style="background-color: white;border-radius: 30px">
                <h3 style="color: black">欢迎你</h3>
                <form id="form" action="" name="f" method="post">
                    <div class="input_outer"> <span class="u_user"></span>
                        <input name="user" id="user" class="text" style="color: black !important" type="text" placeholder="请输入账户">
                        </br>
                        <span
								style="color: red;display: none">账号不能为空</span> </div>
                    <div class="input_outer"> <span class="us_uer"></span>
                        <input name="pass" id="pass" class="text" style="color:black !important; position:absolute; z-index:100;" value="" type="password" placeholder="请输入密码">
                    </div>
                    <div class="mb2"> <a class="act-but submit" href="javascript:;" style="color: #FFFFFF">登录</a> </br>
              
                     <!-- <a class="act-right" id="a_login" href="/register.php">☞前往注册 </a > -->
              
              <a class="act-right" href="#" id="a_login">☞前往注册 </a >
          
          </div>
          
                </form>
            </div>
        </div>
    </div>
</div>



<!-- /container --> 
<script src="/Templates/home/js/tweenlite.min.js"></script> 
<script src="/Templates/home/js/easepack.min.js"></script> 
<script src="/Templates/home/js/raf.js"></script> 
<script src="/Templates/home/js/demo-1.js"></script> 
<script src="/Templates/home/js/core.js"></script> 
<script>
  
   var register = "/Templates/home/register.php"+window.location.search;//获取地址栏

    function getParam(paramName) { 
    paramValue = "", isFound = !1; 
    if (this.location.search.indexOf("?") == 0 && this.location.search.indexOf("=") > 1) { 
        arrSource = unescape(this.location.search).substring(1, this.location.search.length).split("&"), i = 0; 
        while (i < arrSource.length && !isFound) arrSource[i].indexOf("=") > 0 && arrSource[i].split("=")[0].toLowerCase() == paramName.toLowerCase() && (paramValue = arrSource[i].split("=")[1], isFound = !0), i++ 
    } 
    return paramValue == "" && (paramValue = null), paramValue 
} 
  
 
  
document.getElementById("a_login").href = register;  //转到注册页面
  
</script>

<script>
$('.submit').click(function(){
	$.post('/',$('#form').serialize(),function(data){
		if(data.status==1){
			location.href='/index.php?room='+data.msg;	
		}else{
			alert(data.msg);	
		}
	},'json')
})
</script>
</body>
</html>