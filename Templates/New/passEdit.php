<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <script src="/Style/Old/js/hotcss.js"></script>
    <style>
        #setPass{font-size: 0.42666666rem;}
        .header_height{height: 1.2266666666666666rem;}
        #setPass .header{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 2;
            height: 1.2266666666666666rem;
            line-height: 1.2266666666666666rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #setPass .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            font-size: 0.42666666rem;
            line-height: 1.2266666666666666rem;
        }
        #setPass .header .header__left{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #setPass .header .header__left{left: 0.42666666rem;}
        #setPass .header .header__left .van-icon{font-size: 0.42666666rem;}
        #setPass .content{
            position: relative;
            padding: 0 0.42666666rem 0.533333rem;
            background: #fff;
            border-radius: 0.266666rem 0.266666rem 0 0;
        }
        #setPass .content .item{
            line-height: 1.3rem;
        }

        #setPass .content{text-align: center;padding: 0 1rem;}
        #setPass .van-hairline--top-bottom::after,#setPass .van-hairline-unset--top-bottom::after{border: none;}
        #setPass .van-cell-group{border-bottom: 1px solid #eee;}
        #setPass .submit{text-align: center;margin-top: 0.5333333333333333rem;}
        #setPass .van-button{padding: 0 2rem;height: 1.1733333333333333rem;border-radius: 0.5866666666666667rem;}
        #setPass .codeBtn{position: absolute;right: 0;top: 0;}
        #setPass .codeBtn.getCode{color: #333;}
        #setPass .codeBtn.disable{color: #999;}

        #setPass .van-cell {
            padding: 0.26666666666666666rem 0.42666666rem;
            font-size: 0.37333333333333335rem;
            line-height: 0.64rem;
        }

        #setPass .van-button{line-height: 1.12rem;}
        #setPass .van-button--normal{font-size: 0.37333333333333335rem;}
        
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>修改密码</title>
</head>
<body>
    <div id="setPass">
        <div class="header">
            <div class="bar">
                <div class="header__left">
                    <van-icon name="arrow-left" @click="back"/>
                </div>
                <div class="header__title">修改密码</div>
            </div>
        </div>
        <div class="header_height"></div>
        <div class="content">
            <van-cell-group>
                <van-field v-model="oldPass" type="password" placeholder="旧密码" />
            </van-cell-group>
            <van-cell-group>
                <van-field v-model="newPass" type="password" placeholder="新密码" />
            </van-cell-group>
            <van-cell-group>
                <van-field v-model="newPass2" type="password" placeholder="重复新密码" />
            </van-cell-group>
        </div>
        <div class="submit">
            <van-button @click="submit" type="primary">确认上传</van-button>
        </div>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#setPass',
        data(){
            return {
                oldPass: '',
                newPass: '',
                newPass2: '',
                userInfo: {
                    userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
                    headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
                    id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                    userId: "<?php echo $_SESSION['userid']; ?>",
                },
            }
        },
        mounted() {
        },
        methods: {
            back(){
                window.history.go(-1)
            },
            submit(){
                var _this = this;
                if(!_this.oldPass){
                    vant.Toast({
                        message: "旧密码不能为空",
                        duration: "1000",
                    });
                    return false;
                }
                if(_this.newPass !== _this.newPass2){
                    vant.Toast({
                        message: "新密码不相等",
                        duration: "1000",
                    });
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: "/Public/ShiroiInterface.php",
                    dataType: 'text',
                    data: {
                        'f': 'upDateUserInfo',
                        'userid': _this.userInfo.userId,
                        'oldpass': _this.oldPass,
                        'newpass': _this.newPass,
                    },
                    success: function(res){
                        res = JSON.parse(res);
                        delete res.data.pass;
                        console.log(res);
                        if(res.status === 201){
                            vant.Toast({
                                message: res.message,
                                duration: "1000",
                            });
                            return false;
                        }
                        if(!!res.data){
                            vant.Toast({
                                message: "修改成功",
                                duration: "1000",
                            });
                        }
                    }
                });
            }
        }
    })
</script>

</html>