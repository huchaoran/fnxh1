<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <script src="/Style/Old/js/hotcss.js"></script>
    <style>
        #version{font-size: 0.42666666rem;}
        .header_height{height: 1.2266666666666666rem;}
        #version .header{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 2;
            height: 1.2266666666666666rem;
            line-height: 1.2266666666666666rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #version .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            font-size: 0.42666666rem;
            line-height: 1.2266666666666666rem;
        }
        #version .header .header__left,#version .header .header__right{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #version .header .header__left{left: 0.42666666rem;}
        #version .header .header__left .van-icon{font-size: 0.42666666rem;}
        #version .header .header__right{right: 0.42666666rem;bottom: auto;top: 0;}
    
        #version .content{text-align: center;color: red;}

        #version .logo{padding: 0.5333333333333333rem 0;}
        #version .logo img{width: 5.04rem;height: 5.04rem;}
        #version .downloadCode{padding: 0.5333333333333333rem 0;}
        #version .downloadCode img{width: 6.346666666666667rem;height: 5.68rem;}
        

    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>关于</title>
</head>
<body>
    <div id="version">
        <div class="header">
            <div class="bar">
                <div class="header__left">
                    <van-icon name="arrow-left" @click="back"/>
                </div>
                <div class="header__title">关于</div>
            </div>
        </div>
        <div class="header_height"></div>
        <div class="content">
            <div class="logo">
                <img :src="logo" alt="">
            </div>
            <div>{{text}}</div>
            <div class="downloadCode">
                <img :src="downloadCode" alt="">
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#version',
        data(){
            return {
                logo: "/NewUI/images/mine/ic_launcher.png",
                text: "长按识别二维码下载软件",
                downloadCode: "/NewUI/images/mine/download.png",
            }
        },
        mounted() {
        },
        methods: {
            back(){
                window.history.go(-1)
            }
        }
    })
</script>

</html>