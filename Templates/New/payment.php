<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <script src="/Style/Old/js/hotcss.js"></script>
    <style>
        #payment{font-size: 0.42666666rem;}
        .header_height{height: 1.2266666666666666rem;}
        #payment .header{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 2;
            height: 1.2266666666666666rem;
            line-height: 1.2266666666666666rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #payment .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            font-size: 0.42666666rem;
            line-height: 1.2266666666666666rem;
        }
        #payment .header .header__left{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #payment .header .header__left{left: 0.42666666rem;}
        #payment .header .header__left .van-icon{font-size: 0.42666666rem;}
        #payment .content{text-align: center;padding: 0 1rem;}
        #payment .van-hairline--top-bottom::after,#payment .van-hairline-unset--top-bottom::after{border: none;}
        #payment .van-cell-group{border-bottom: 1px solid #eee;}

        #payment .van-tabs__content{margin-top: 0.5rem;}

        #payment .text{margin-bottom: 0.5rem;font-size: 0.42666666rem;}
        #payment .submit{margin-top: 0.5333333333333333rem;}
        #payment .van-button{padding: 0 2rem;height: 1.1733333333333333rem;border-radius: 0.5866666666666667rem;}
        #payment .van-button--normal{font-size: 0.37333333333333335rem;}

        #payment .van-tabs--line .van-tabs__wrap{
            height: 1.1733333333333333rem;
        }
        #payment .van-tab {
            padding: 0 0.13333333333333333rem;
            font-size: 0.37333333333333335rem;
            line-height: 1.1733333333333333rem;
        }

        #payment .van-cell {
            padding: 0.26666666666666666rem 0.42666666rem;
            font-size: 0.37333333333333335rem;
            line-height: 0.64rem;
        }

        #payment .van-uploader__upload-text{margin-top: 0.21333333333333335rem;font-size: 0.32rem;}
        .van-uploader__upload-icon{font-size: 0.64rem;}
        .van-uploader__upload {
            width: 2.8rem;
            height: 2.8rem;
            margin: 0 0.21333333333333333rem 0.21333333333333333rem 0;
            border: 0.03rem dashed #e5e5e5;
        }
        #payment .content .upload{
            width: 2.8rem;
            height: 2.8rem;
            margin: 0 auto;
        }
        #payment .content .van-uploader{width: 100%;height: 100%;}
        #payment .content .van-uploader__wrapper{width: 100%;height: 100%;}
        #payment .content .van-uploader__input-wrapper{width: 100%;height: 100%;}
        #payment .content .van-uploader__input-wrapper img{width: 100%;height: 100%;vertical-align: bottom;object-fit: cover;display: block;}
        #payment .content .tips{color: red;font-size: 0.3rem;margin-top: 0.5rem;}
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>收款方式</title>
</head>
<body>
    <div id="payment">
        <div class="header">
            <div class="bar">
                <div class="header__left">
                    <van-icon name="arrow-left" @click="back"/>
                </div>
                <div class="header__title">收款方式</div>
            </div>
        </div>
        <div class="header_height"></div>
        <div class="content">
            <van-tabs v-model="activeName" color="#35A8F1">
                <van-tab title="微信" name="wechat">
                    <div class="text">
                        上传微信收款码：
                    </div>
                    <div class="upload">
                        <van-uploader v-model="wechatPay" :max-count="1" :max-size="1024*1024*2" upload-text="限制大小2M" :after-read="afterRead" >
                            <img v-if="wechatCode" :src="wechatCode" alt="">
                        </van-uploader>
                    </div>
                </van-tab>
                <van-tab title="支付宝" name="alipay">
                    <div class="text">
                        上传支付宝收款码：
                    </div>
                    <div class="upload">
                        <van-uploader v-model="alipay" :max-count="1" :max-size="1024*1024*2" upload-text="限制大小2M" :after-read="afterRead" >
                            <img v-if="alipayCode" :src="alipayCode" alt="">
                        </van-uploader>
                    </div>
                </van-tab>
                <van-tab title="银行卡" name="bandCard">
                    <van-cell-group>
                        <van-field v-model="cardName" placeholder="请输入持卡人姓名" />
                    </van-cell-group>
                    <van-cell-group>
                        <van-field v-model="cardNumber" placeholder="请输入银行卡号码" />
                    </van-cell-group>
                    <van-cell-group>
                        <van-field v-model="cardType" placeholder="请输入银行卡类型" />
                    </van-cell-group>
                    <van-cell-group>
                        <van-field v-model="cardAddress" placeholder="请输入开户行地址" />
                    </van-cell-group>
                </van-tab>
            </van-tabs>
            <div class="submit">
                <van-button @click="submit" type="primary">确认上传</van-button>
            </div>
            <div v-show=" activeName !== 'bandCard' " class="tips">建议上传图片比例 1:1</div>
        </div>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#payment',
        data(){
            return {
                activeName: "wechat",
                userInfo: {
                    userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
                    headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
                    id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                    userId: "<?php echo $_SESSION['userid']; ?>",
                },
                wechatPay: [],
                alipay: [],
                cardName: "<?php echo get_query_val("fn_pay", "card_username", array('userid' => $_SESSION['userid'])); ?>",
                cardNumber: "<?php echo get_query_val("fn_pay", "card_number", array('userid' => $_SESSION['userid'])); ?>",
                cardType: "<?php echo get_query_val("fn_pay", "card_type", array('userid' => $_SESSION['userid'])); ?>",
                cardAddress: "<?php echo get_query_val("fn_pay", "card_bank_address", array('userid' => $_SESSION['userid'])); ?>",
                wechatCode: "<?php echo get_query_val("fn_pay", "wechat", array('userid' => $_SESSION['userid'])); ?>",
                alipayCode: "<?php echo get_query_val("fn_pay", "alipay", array('userid' => $_SESSION['userid'])); ?>",
            }
        },
        mounted() {
            $.ajax({
                url: `/Public/ShiroiInterface.php?f=filePost&userid=${this.userInfo.userId}`,
                type: "get",
                success: function(res){
                    res = JSON.parse(res);
                    console.log(res);
                },
                error(err){
                    if(!!err){
                        vant.Toast({
                            message: "上传失败，请检测网络！",
                            duration: "1000",
                        });
                    }
                }
            });
        },
        methods: {
            back(){
                window.history.go(-1)
            },
            afterRead(file,detail){
                var _this = this;
                var formData = new FormData();
                formData.append("file",file['file']);
                $.ajax({
                    url: `/Public/ShiroiInterface.php?f=filePost&userid=${_this.userInfo.userId}&path=NewUI/images/paycode/${_this.activeName}`,
                    type: "post",
                    data : formData,
                    processData : false,
                    contentType: false,
                    success: function(res){
                        res = JSON.parse(res);
                        _this[_this.activeName+'Code'] = res.data;
                    },
                    error(err){
                        if(!!err){
                            vant.Toast({
                                message: "上传失败，请检测网络！",
                                duration: "1000",
                            });
                        }
                    }
                });
            },
            submit(e){
                var _this = this;
                var params = {};
                switch(_this.activeName){
                    case 'wechat':
                        params = {
                            'f': "payInfo",
                            'userid': _this.userInfo.userId,
                            'wechat': _this.wechatCode,
                        }
                        break;
                    case 'alipay':
                        params = {
                            'f': 'payInfo',
                            'userid': _this.userInfo.userId,
                            'alipay': _this.alipayCode,
                        }
                        break;
                    case 'bandCard':
                        params = {
                            'f': 'payInfo',
                            'userid': _this.userInfo.userId,
                            'card_username': _this.cardName,
                            'card_number': _this.cardNumber,
                            'card_type': _this.cardType,
                            'card_bank_address': _this.cardAddress,
                        }
                        break;
                };
                $.ajax({
                    url: "/Public/ShiroiInterface.php",
                    type: "get",
                    data: params,
                    success: function(res){
                        res = JSON.parse(res);
                        if(!!res.data){
                            vant.Toast({
                                message: "修改成功",
                                duration: "1000",
                            });
                        }
                    }
                });
            }
        }
    })
</script>

</html>