<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <script src="/Style/Old/js/hotcss.js"></script>
    <style>
        #share{font-size: 0.42666666rem;
            display: flex;
            flex-direction: column;
            height: 100vh;
            background-color: rgb(16,16,17);
            color: #ffffff;
        }
        .header_height{height: 1.2266666666666666rem;}
        #share .header{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 2;
            height: 1.2266666666666666rem;
            line-height: 1.2266666666666666rem;
            text-align: center;
            background-color: rgb(16,16,17);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #share .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            font-size: 0.42666666rem;
            line-height: 1.2266666666666666rem;
        }
        #share .header .header__left,#share .header .header__right{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #share .header .header__left{left: 0.42666666rem;}
        #share .header .header__left .van-icon{font-size: 0.42666666rem;}
        #share .header .header__right{right: 0.42666666rem;bottom: auto;top: 0;}
        #share .content{flex: auto;}
        #share .content .card{
            margin: 3rem 1rem 0;
            border-radius: 5px;
            background-color: #ffffff;
            color: #333;
            text-align: center;
        }
        #share .content .card .code div{
            padding-top: 0.3rem;
            font-size: 0.8rem;
            color: #000;
        }
        #share .content .card .code span{
            color: #999;
            font-size: 0.37333333333333335rem;
        }
        #qrcode{
            width: 5rem;
            height: 5rem;
            margin: 1rem auto 1rem;
        }
        #qrcode canvas{
            width: 5rem;
            height: 5rem;
        }
        #share .content .card .btns{
            display: flex;
        }
        #share .content .card .btns button{
            width: 50%;
            box-sizing: border-box;
            background-color: transparent;
            border: none;
            line-height: 1.5rem;
            border-top: 1px solid #eeeeee;
        }
        #share .content .card .btns button:nth-child(1){border-right: 1px solid #eeeeee;}
        #share .share{
            text-align: center;
            padding-top: 0.5rem;
        }
        #share .share .text{position: relative;}
        #share .share .text i{
            display: block;
            height: 1px;
            width: 1.8rem;
            position: absolute;
            top: 50%;
        }
        #share .share .text i.l{
            left: 130%;
            background-image: linear-gradient(to right, rgba(255,255,255,0.6) , rgba(255,255,255,0) );
        }
        #share .share .text i.r{
            right: 130%;
            background-image: linear-gradient(to left, rgba(255,255,255,0.6) , rgba(255,255,255,0) );
        }
        #share .share .list{
            padding: 0.5rem 1.5rem 0;
            display: flex;
            justify-content: space-between;
        }
        #share .share .list .item{}
        #share .share .list .item img{width: 1rem;height: 1rem;}
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <script src="../../Style/Old/js/jquery.qrcode.min.js"></script>
    <script src="../../NewUI/js/clipboard/clipboard.min.js"></script>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>分享</title>
</head>
<body>
    <div id="share">
        <div class="header">
            <div class="bar">
                <div class="header__left">
                    <van-icon name="arrow-left" @click="back"/>
                </div>
                <div class="header__title">分享</div>
            </div>
        </div>
        <div class="header_height"></div>
        <div class="content">
            <div class="card">
                <div class="code">
                    <div>{{code}}</div>
                    <span>我的推广码</span>
                </div>
                <div id="qrcode"></div>
                <div class="btns">
                    <button id="copyLink" @click="copyLink">复制链接</button>
                    <button id="saveCode" @click="saveCode">保存二维码</button>
                </div>
            </div>
            <!-- <div class="share">
                <span class="text">邀请好友<i class="l"></i><i class="r"></i></span>
                <div class="list">
                    <div class="item" @click="wxClick">
                        <img src="/NewUI/images/mine/wx.png" alt="">
                        <div>微信</div>
                    </div>
                    <div class="item" @click="pyqClick">
                        <img src="/NewUI/images/mine/pyq.png" alt="">
                        <div>朋友圈</div>
                    </div>
                    <div class="item" @click="qqClick">
                        <img src="/NewUI/images/mine/qq.png" alt="">
                        <div>QQ</div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</body>
<script>
    var para = {};
    para.url = decodeURIComponent(location.href.split('#')[0]);
    $.ajax({
        url: '/Public/initJs.php',
        type: 'post',
        data: para,
        dataType: 'json',
        success: function(data){
            wx.config({
                debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                appId: data.appId, // 必填，公众号的唯一标识
                timestamp: data.timestamp, // 必填，生成签名的时间戳
                nonceStr: data.noncestr, // 必填，生成签名的随机串
                signature: data.signature,// 必填，签名，见附录1
                jsApiList : [ "onMenuShareTimeline","onMenuShareAppMessage", "onMenuShareQQ","onMenuShareWeibo", "chooseImage","previewImage", "getNetworkType", "scanQRCode","chooseWXPay" ]
            });
        },
        error:function(error){ console.log(error);  }
    });

    wx.ready(function(){
        wx.onMenuShareTimeline({
            title: sharetitle, // 分享标题
            link: shareurl, // 分享链接
            imgUrl: shareImg, // 分享图标
            success: function () {

            },
            cancel: function () {

            }
        });
        wx.onMenuShareAppMessage({
            title: sharetitle, // 分享标题
            desc: sharedesc, // 分享描述
            link: shareurl, // 分享链接
            imgUrl: shareImg, // 分享图标
            type: '', // 分享类型,music、video或link，不填默认为link
            dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
            success: function () {
                // 用户确认分享后执行的回调函数
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
            }
        });
    });
    wx.error(function (res) {
        alert("share error: " + res.errMsg);
    });
</script>
<script type="text/javascript">
    var curWwwPath=window.document.location.href; 
    var pathName=window.document.location.pathname; 
    var pos=curWwwPath.indexOf(pathName); 
    var localhostPaht=curWwwPath.substring(0,pos); 
    var link = `${localhostPaht}/qr.php?agent=<?php echo $_SESSION['userid']; ?>`;
    $('#copyLink').attr('data-clipboard-text' , link);
    var app = new Vue({
        el: '#share',
        data(){
            return {
                code: "000000",
                userInfo: {
                    userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
                    headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
                    id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                    userId: "<?php echo $_SESSION['userid']; ?>",
                },
                memberList: [
                    {
                        username: "测试名字",
                        headimg: "/Templates/home/res/face/2.png",
                        sharescore: "889",
                    }
                ]
            }
        },
        mounted(){
            jQuery('#qrcode').qrcode(link);
        },
        methods: {
            back(){
                window.history.go(-1)
            },
            wxClick(){
                // alert('微信点击');
                console.log(wx.onMenuShareTimeline);
                wx.onMenuShareTimeline({
                    title: "hello 微信分享", // 分享标题
                    link: link, // 分享链接
                    imgUrl: 'https://pic.cnblogs.com/face/826333/20180523233322.png', // 分享图标
                    success: function () {
                        alert('分享成功')
                    },
                    cancel: function () {
                        alert('取消分享')
                    }
                });
            },
            pyqClick(){
                wx.onMenuShareAppMessage({
                    title: "hello 朋友圈分享", // 分享标题
                    desc: "快来加入我吧！", // 分享描述
                    link: link, // 分享链接
                    imgUrl: 'https://pic.cnblogs.com/face/826333/20180523233322.png', // 分享图标
                    type: 'link', // 分享类型,music、video或link，不填默认为link
                    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                    success: function () {
                        alert('分享成功')
                    },
                    cancel: function () {
                        alert('取消分享')
                    }
                });
            },
            qqClick(){
                wx.onMenuShareQQ({
                    title: "hello qq分享", // 分享标题
                    link: link, // 分享链接
                    imgUrl: 'https://pic.cnblogs.com/face/826333/20180523233322.png', // 分享图标
                    success: function () {
                        alert('分享成功')
                    },
                    cancel: function () {
                        alert('取消分享')
                    }
                });
            },
            copyLink(){
                new ClipboardJS('#copyLink');
                vant.Toast({
                    message: "复制成功，快去分享好友吧！",
                    duration: "1000",
                });
            },
            saveCode(){
                var date = new Date().getTime();
                var canvas = document.querySelector('#qrcode canvas');
                var base64 = canvas.toDataURL("image/png");
                var a = document.createElement("a");
                a.download = 'code'+'-'+date;
                a.href = base64; 
                document.body.appendChild(a);
                a.click();
                a.remove();
            }
        }
    })
</script>
</html>