<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <script src="/Style/Old/js/hotcss.js"></script>
    <style>
        #room{font-size: 0.42666666rem;}
        .header_height{height: 1.2266666666666666rem;}
        #room .header{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 2;
            height: 1.2266666666666666rem;
            line-height: 1.2266666666666666rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #room .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            font-size: 0.42666666rem;
            line-height: 1.2266666666666666rem;
        }
        #room .header .header__left,#room .header .header__right{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #room .header .header__left{left: 0.42666666rem;}
        #room .header .header__left .van-icon{font-size: 0.42666666rem;}
        #room .header .header__right{right: 0.42666666rem;bottom: auto;top: 0;}
    
        #room .van-pull-refresh{overflow: visible;}
        #room .content{min-height: 92vh;}
        #room .content .list{_overflow: hidden;}
        #room .content .list .item{
            width: 16rem;
            box-sizing: border-box;
            padding: 0 0.42666666rem;
            position: relative;
            height: 2.6666666rem;
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            align-items: center;
            border-bottom: 1px solid #eee;
            transition: all 0.3s;
            left: 0;
        }
        #room .content .list .item .box{
            display: flex;
            display: -webkit-flex;
            justify-content: flex-start;
            align-items: center;
        }
        #room .content .list .item .pic{margin-right: 0.26666666666666666rem;}
        #room .content .list .item .pic img{width: 1.6rem;height: 1.6rem;}
        #room .content .list .item .text div{color: #333;margin-bottom: 0.13333333333333333;font-size: 0.42666666rem;}
        #room .content .list .item .text span{color: #999;font-size: 0.373333333rem;}
        
        #room .content .list .item .links a{margin-left: 0.6666666666666666rem;}
        #room .content .list .item .links a img{width: 1.3333333333333333rem;height: 1.3333333333333333rem;}

        #room .editRoomRemark{text-align: center;padding: 1rem 1rem;box-sizing: border-box;width: 80vw;}
        #room .editRoomRemark span{font-size: 0.42666666rem;color: #333333;}
        #room .editRoomRemark .van-hairline--top-bottom::after, .editRoomRemark .van-hairline-unset--top-bottom::after{border-top-width: 0px;}
        #room .editRoomRemark .van-field__control{text-align: center;}
        #room .editRoomRemark .input{margin-top: 0.4rem;}
        #room .editRoomRemark .submit{margin-top: 0.8rem;}
        #room .editRoomRemark .submit .van-button--normal{padding: 0 2rem;border-radius: 0.5866666666666667rem;line-height: 1.1733333333333333rem;}
        #room .editRoomRemark .submit .van-button__text{color: #fff;}
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>房间记录</title>
</head>
<body>
    <div id="room">
        <div class="header">
            <div class="bar">
                <div class="header__left">
                    <van-icon name="arrow-left" @click="back"/>
                </div>
                <div class="header__title">房间记录</div>
            </div>
        </div>
        <div class="header_height"></div>
        <van-pull-refresh v-model="isLoading" @refresh="onRefresh" success-text="刷新成功">
            <div class="content">
                <div class="list">
                    <div class="item" v-for="(item,index) in roomList" :key="index" @click="itemClick(index,this)" :data-left="item.left" :style="{left: item.left,}">
                        <div class="left">
                            <div class="box">
                                <div class="pic">
                                    <img :src="item.roomheadimg" alt="">
                                </div>
                                <div class="text">
                                    <div>{{ item.roomRemark ? item.roomRemark : item.roomid }}</div>
                                    <span>积分:{{item.roomscore}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="right">
                            <div class="box">
                                <div class="icon">
                                    <van-icon name="arrow"/>
                                </div>
                                <div class="links">
                                    <a href="/Templates/Old/reportquery.php" title="报表" @click.stop=""><img src="/NewUI/images/mine/ic_swipe_table.png" alt=""></a>
                                    <a title="备注" @click.stop="showPopup(index)" ><img src="/NewUI/images/mine/ic_swipe_remark.png" alt=""></a>
                                    <a href="/" title="进入" @click.stop=""><img src="/NewUI/images/mine/ic_swipe_enter.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </van-pull-refresh>
        <van-popup v-model="showEdit" closeable >
            <div class="editRoomRemark">
                <div class="text">
                    <span>房间备注</span>
                </div>
                <div class="input">
                    <van-cell-group>
                        <van-field v-model="roomRemark" type="text" placeholder="请输入要修改的房间备注" />
                    </van-cell-group>
                </div>
                <div class="submit">
                    <van-button @click="submit" type="primary">确认</van-button>
                </div>
            </div>
        </van-popup>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#room',
        data(){
            return {
                isLoading: false,
                showEdit: false,
                roomRemark: '',
                roomIndex: '',
                userInfo: {
                    userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
                    headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
                    id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                    userId: "<?php echo $_SESSION['userid']; ?>",
                },
                roomList: [{
                    roomid : "<?php echo get_query_val("fn_user", "roomid", array('userid' => $_SESSION['userid'])); ?>",
                    roomname: "<?php $roomid = get_query_val("fn_user", "roomid", array('userid' => $_SESSION['userid'])); echo get_query_val("fn_room", "roomname" ,array('roomid' => $roomid)); ?>",
                    roomheadimg: "<?php $roomid = get_query_val("fn_user", "roomid", array('userid' => $_SESSION['userid'])); echo get_query_val("fn_room", "roomheadimg" ,array('roomid' => $roomid)); ?>",
                    roomscore : "<?php echo get_query_val("fn_user", "roomscore", array('userid' => $_SESSION['userid'])); ?>",
                    roomRemark: "<?php echo get_query_val("fn_user", "roomremark", array('userid' => $_SESSION['userid'])); ?>",
                    left: "0",
                }],
            }
        },
        methods: {
            back(){
                window.history.go(-1)
            },
            itemClick(index,g){
                var l = this.roomList[index].left;
                if(l === "0"){
                    this.roomList[index].left = "-6rem";
                    return;
                }
                this.roomList[index].left = "0";
                
            },
            showPopup(index){
                this.roomRemark = this.roomList[index].roomRemark;
                this.roomIndex = index;
                this.showEdit = true;
            },
            onRefresh() {
                
            },
            submit(){
                var _this = this;
                this.roomList[_this.roomIndex].roomRemark = _this.roomRemark;
                this.roomList[_this.roomIndex].left = '0';
                $.ajax({
                    url: "/Public/ShiroiInterface.php",
                    type: "get",
                    data: {
                        'f': 'upDateUserInfo',
                        'userid': _this.userInfo.userId,
                        'roomremark': _this.roomRemark,
                    },
                    success: function(res){
                        res = JSON.parse(res);
                        delete res.data.pass;
                        _this.showEdit = false;
                        if(!!res.data){
                            vant.Toast({
                                message: "修改成功",
                                duration: "1000",
                            });
                        }
                    }
                });
            }
        }
    })
</script>

</html>