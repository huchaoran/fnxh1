<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <script src="/Style/Old/js/hotcss.js"></script>
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <style>
        #mine{font-size: 0.42666666rem;}
        #mine .header{
            position: relative;
            height: 5.33333rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #mine .header .bar{
            line-height: 1.226666666rem;
            background-color: rgb(53,168,241);
        }
        #mine .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            line-height: 1.226666666rem;
        }
        #mine .header .header__left,#mine .header .header__right{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #mine .header .header__left{left: 0.42666666rem;}
        #mine .header .header__left .van-icon{font-size: 0.42666666rem;}
        #mine .header .header__right{right: 0.42666666rem;bottom: auto;top: 0;color: #fff;}
        #mine .header .header__right a{color: #fff;}

        #mine .header .info{position: relative;padding: 0 0.426666666rem;}
        #mine .header .info .name:after{content: "";display: block;clear: both;}
        #mine .header .info__left{
            overflow: hidden;
            /* position: absolute;
            left: 0.42666666rem;
            top: 0.8rem; */
            float: left;
            width: 1.6rem;
            height: 1.6rem;
            border-radius: 50%;
            background-color: #fff;
        }
        #mine .header .info__left img{
            width: 100%;
            height: 100%;
        }
        #mine .header .info__right{
            /* position: absolute;
            top: 0.8rem;
            right: 0.42666666rem; */
            float: right;
            text-align: left;
            line-height: 0.8rem;
        }
        #mine .header .wallet{
            display: -webkit-flex;
            display: flex;
            justify-content: space-between;
            margin-top: 0.4rem;
            font-size: 0.37333333333333335rem;
        }


        #mine .content{
            position: relative;
            margin-top: -0.533333rem;
            padding: 0 0.42666666rem 0;
            background: #fff;
            border-radius: 0.266666rem 0.266666rem 0 0;
        }
        #mine .content .list{padding: 0.2rem 0;}
        #mine .content .list .item{
            line-height: 1.3rem;
        }
        a:-webkit-any-link:active {
            color: #333;
        }
        a:-webkit-any-link {
            color: #333;
        }
        #mine .content .list .item img{
            width: 0.6rem;
            height: auto;
            vertical-align: middle;
            margin-right: 0.133333rem;
        }
        #mine .content .list .item span{}
        #mine .content .list .item .van-icon-arrow{
            float: right;
            line-height: 1.3rem;
        }
        #mine .content .item .right {
            float: right;
        }
        #mine .clear {
            clear: both;
        }

        #mine .content .exit{
            position: fixed;
            bottom: 0;
            left:0;
            right: 0;
            text-align: center;
        }
        #mine .content .exit .van-button--normal{
            width: 100%;
            height: 1.1733333rem;
            line-height: 1.1733333rem;
            font-size: 0.373333333rem;
        }
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>个人中心</title>
</head>

<body>
    <div id="mine">
        <div class="header">
            <van-sticky>
                <div class="bar">
                    <div class="header__left">
                        <van-icon name="arrow-left" @click="back"/>
                    </div>
                    <div class="header__title">个人中心</div>
                    <div class="header__right" @click="edit">
                        <a href="userEdit.php">
                            编辑
                        </a>
                    </div>
                </div>
            </van-sticky>
            <div class="info">
                <div class="name">
                    <div class="info__left">
                        <a href="headEdit.php">
                        <img :src="userInfo.headImg" :alt="userInfo.userName">
                        </a>
                    </div>
                    <div class="info__right">
                        <div>{{userInfo.userName}}</div>
                        <div>ID:{{userInfo.id}}</div>
                    </div>
                </div>
                <div class="wallet">
                    <div>
                        <div>我的钱包</div>
                        <span>{{userInfo.money}}</span>
                    </div>
                    <div>
                        <div>今日盈亏</div>
                        <span><?php echo $info['yk']; ?></span>
                    </div>
                    <div>
                        <div>今日流水</div>
                        <span><?php echo $info['liu']; ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="list">
                <div v-for="(item,index) in links" :key="index" class="item">
                    <a :href="item.link">
                        <div>
                            <img :src="item.ico" width="15" height="15">
                            <span>{{item.name}}</span>
                            <div class="right">
                                <span v-if="item.remark" v-html="item.remark" style="margin-right: 5px;"></span>
                                <van-icon name="arrow"/>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="exit">
                <van-button type="danger" @click="logout">退出</van-button>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#mine',
        data: {
            userInfo: {
                userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
                headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
                id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                userId: "<?php echo $_SESSION['userid']; ?>",
                money: "<?php echo get_query_val("fn_user", "money", array('userid' => $_SESSION['userid'])); ?>",
            },
            links: [
                {
                    name: "房间记录",
                    ico: "/NewUI/images/mine/1.png",
                    link: "roomRecords.php",
                },
                {
                    name: "房间成员",
                    ico: "/NewUI/images/mine/6.png",
                    link: "roomMember.php",
                },
                // {
                //     name: "我的消息",
                //     ico: "/NewUI/images/mine/ic_mine_message.png",
                //     link: "news.php",
                // },
                {
                    name: "添加收款方式",
                    ico: "/NewUI/images/mine/4.png",
                    link: "payment.php",
                },
                {
                    name: "团队信息",
                    ico: "/NewUI/images/mine/team.png",
                    link: "team.php",
                },
                {
                    name: "投注信息",
                    ico: "/NewUI/images/mine/order.png",
                    link: "order.php",
                },
                {
                    name: "我的下线",
                    ico: "/NewUI/images/mine/agent.png",
                    link: "underline.php",
                    remark: "共<c style='color: #ad9049;'><?php echo get_query_val("fn_user", "count(*)", array("roomid" => $_SESSION['roomid'], 'agent' => $_SESSION['userid'])); ?></c>人",
                },
                // {
                //     name: "财务记录",
                //     ico: "/NewUI/images/mine/ic_mine_receipt.png",
                //     link: "/",
                // },
                // {
                //     name: "交易明细",
                //     ico: "/NewUI/images/mine/ic_mine_receipt.png",
                //     link: "/",
                // },
                {
                    name: "我要分享",
                    ico: "/NewUI/images/mine/ic_mine_share.png",
                    link: "share.php",
                },
                {
                    name: "关于",
                    ico: "/NewUI/images/mine/ic_mine_about.png",
                    link: "version.php",
                },
                {
                    name: "设置",
                    ico: "/NewUI/images/mine/ic_mine_setting.png",
                    link: "set.php",
                },
            ]
        },
        mounted() {

        },
        methods: {
            back(){
                window.location.href= '/';
            },
            edit(){
                console.log("编辑个人信息");
            },
            logout(){
                document.cookie = "PHPSESSID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
                document.cookie = "game=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
                document.cookie = "logintime=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
                window.location.href = '/'
            }
        }
    })
</script>

</html>