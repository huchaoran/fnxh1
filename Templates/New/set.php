<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <script src="/Style/Old/js/hotcss.js"></script>
    <style>
        #set{font-size: 0.42666666rem;}
        .header_height{height: 1.2266666666666666rem;}
        #set .header{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 2;
            height: 1.2266666666666666rem;
            line-height: 1.2266666666666666rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #set .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            font-size: 0.42666666rem;
            line-height: 1.2266666666666666rem;
        }
        #set .header .header__left{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #set .header .header__left{left: 0.42666666rem;}
        #set .header .header__left .van-icon{font-size: 0.42666666rem;}
        #set .content{
            position: relative;
            padding: 0 0.42666666rem 0.533333rem;
            background: #fff;
            border-radius: 0.266666rem 0.266666rem 0 0;
        }
        #set .content .item{
            line-height: 1.3rem;
        }

        a:-webkit-any-link:active {
            color: #333;
        }
        a:-webkit-any-link {
            color: #333;
        }
        #set .content .item span{}
        #set .content .item .van-icon-arrow{
            float: right;
            line-height: 1.3rem;
        }

        #set .content .item .van-switch{
            float: right;
            margin-top: 0.2133333rem;
        }
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>设置</title>
</head>
<body>
    <div id="set">
        <div class="header">
            <div class="bar">
                <div class="header__left">
                    <van-icon name="arrow-left" @click="back"/>
                </div>
                <div class="header__title">设置</div>
            </div>
        </div>
        <div class="header_height"></div>
        <div class="content">
            <div class="item">
                <a href="setPhone.php">
                    <div>
                        <span>账号升级</span>
                        <van-icon name="arrow"/>
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="payment.php">
                    <div>
                        <span>添加收款方式</span>
                        <van-icon name="arrow"/>
                    </div>
                </a>
            </div>
            <div class="item">
                <div>
                    <span>背景音乐</span>
                    <van-switch v-model="checked1" disabled />
                </div>
            </div>
            <div class="item">
                <div>
                    <span>音效</span>
                    <van-switch v-model="checked2" disabled />
                </div>
            </div>
            <div class="item">
                <div>
                    <span>通知</span>
                    <van-switch v-model="checked3" disabled />
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#set',
        data(){
            return {
                checked1: false,
                checked2: false,
                checked3: false,
                userInfo: {
                    userName: "<?php echo $_SESSION['username'];?>",
                    headImg:"<?php echo $_SESSION['headimg']; ?>",
                    userId: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                },
                wechatPay: [],
                aliPay: [],
            }
        },
        mounted() {
        },
        methods: {
            back(){
                window.history.go(-1)
            }
        }
    })
</script>

</html>