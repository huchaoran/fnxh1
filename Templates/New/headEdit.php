<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <script src="/Style/Old/js/hotcss.js"></script>
    <style>
        #headEdit{font-size: 0.42666666rem;}
        .header_height>div{height: 1.2266666666666666rem;}
        #headEdit .header{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 2;
            height: 1.2266666666666666rem;
            line-height: 1.2266666666666666rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #headEdit .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            font-size: 0.42666666rem;
            line-height: 1.2266666666666666rem;
        }
        #headEdit .header .header__left{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #headEdit .header .header__left{left: 0.42666666rem;}
        #headEdit .header .header__left .van-icon{font-size: 0.42666666rem;}

        
        #headEdit .content{text-align: center;display: ;}
        #headEdit .content .top .box{
            width: 2rem;
            height: 2rem;
            margin: 0.3rem auto;
            background-color: #F5F5F5;
            border: 4px solid #EDC384;
            border-radius: 5px;
        }
        #headEdit .content .top .van-uploader{width: 100%;height: 100%;}
        #headEdit .content .top .van-uploader__wrapper{width: 100%;height: 100%;}
        #headEdit .content .top .van-uploader__input-wrapper{width: 100%;height: 100%;}
        #headEdit .content .top .box img{width: 100%;height: 100%;vertical-align: bottom;object-fit: cover;display: block;}
        #headEdit .list .pic{position: relative;}
        #headEdit .list img{width: 2rem;height: 2rem;vertical-align: bottom;}

        
        #headEdit .submit{margin-top: 0.5rem;padding-bottom: 0.5rem;}
        #headEdit .van-button{padding: 0 2rem;height: 1.1733333333333333rem;border-radius: 0.5866666666666667rem;}
        #headEdit .van-button--normal{font-size: 0.37333333333333335rem;}
        #headEdit .van-button__text{line-height: 1.1733333333333333rem;display: inline-block;vertical-align: top;}

        #headEdit .content{
            height: 100vh;
        }
        #headEdit .content .list .van-grid-item__content{padding: 0.4rem 0.2rem;}
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>修改头像</title>
</head>
<body>
    <div id="headEdit">
        <div class="header">
            <div class="bar">
                <div class="header__left">
                    <van-icon name="arrow-left" @click="back"/>
                </div>
                <div class="header__title">修改头像</div>
            </div>
        </div>
        <div class="content">
            <div class="header_height">
                <div></div>
            </div>
            <div class="top">
                <div class="box">
                <van-uploader :after-read="afterRead">
                    <img :src="userInfo.headImg" alt="">
                </van-uploader>
                </div>
            </div>
            <div class="list">
                <div class="scroll">
                    <van-grid :border="false" :column-num="4">
                        <van-grid-item
                            v-for="(item,index) in imgList"
                            :key="index"
                            icon="photo-o"
                        >
                            <div class="pic" @click="picChange(item.pic)">
                                <img :src="item.pic" rel="external nofollow" >
                            </div>
                        </van-grid-item>
                    </van-grid>
                </div>
            </div>
            <div class="submit">
                <van-button @click="submit" type="primary">提交</van-button>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#headEdit',
        data(){
            return {
                userInfo: {
                    userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
                    headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
                    id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                    userId: "<?php echo $_SESSION['userid']; ?>",
                },
                imgList:[
                    {pic: "/Templates/home/res/face/1.png",},
                    {pic: "/Templates/home/res/face/2.png",},
                    {pic: "/Templates/home/res/face/3.png",},
                    {pic: "/Templates/home/res/face/4.png",},
                    {pic: "/Templates/home/res/face/5.png",},
                    {pic: "/Templates/home/res/face/6.png",},
                    {pic: "/Templates/home/res/face/7.png",},
                    {pic: "/Templates/home/res/face/8.png",},
                    {pic: "/Templates/home/res/face/9.png",},
                    {pic: "/Templates/home/res/face/10.png",},
                    {pic: "/Templates/home/res/face/11.png",},
                    {pic: "/Templates/home/res/face/12.png",},
                    {pic: "/Templates/home/res/face/13.png",},
                    {pic: "/Templates/home/res/face/14.png",},
                    {pic: "/Templates/home/res/face/15.png",},
                    {pic: "/Templates/home/res/face/16.png",},
                    {pic: "/Templates/home/res/face/17.png",},
                    {pic: "/Templates/home/res/face/18.png",},
                    {pic: "/Templates/home/res/face/19.png",},
                    {pic: "/Templates/home/res/face/20.png",},
                    {pic: "/Templates/home/res/face/21.png",},
                    {pic: "/Templates/home/res/face/22.png",},
                    {pic: "/Templates/home/res/face/23.png",},
                    {pic: "/Templates/home/res/face/24.png",},
                    {pic: "/Templates/home/res/face/25.png",}
                ],
            }
        },
        mounted() {
            
        },
        methods: {
            back(){
                window.history.go(-1)
            },
            picChange(pic){
                this.userInfo.headImg = pic;
            },
            afterRead(file){
                var _this = this;
                var formData = new FormData();
                formData.append("file",file['file']);
                $.ajax({
                    url: `/Public/ShiroiInterface.php?f=filePost&userid=${_this.userInfo.userId}&path=NewUI/images/headimg`,
                    type: "post",
                    data : formData,
                    processData : false,
                    contentType: false,
                    success: function(res){
                        res = JSON.parse(res);
                        _this.userInfo.headImg = res.data;
                    },
                    error(err){
                        if(!!err){
                            vant.Toast({
                                message: "上传失败，请检测网络！",
                                duration: "1000",
                            });
                        }
                    }
                });
            },
            submit(){
                var _this = this;
                $.ajax({
                    url: "/Public/ShiroiInterface.php",
                    type: "get",
                    data: {
                        'f': 'upDateUserInfo',
                        'userid': _this.userInfo.userId,
                        'headimg': _this.userInfo.headImg,
                    },
                    success: function(res){
                        res = JSON.parse(res);
                        delete res.data.pass;
                        console.log(res);
                        if(!!res.data){
                            vant.Toast({
                                message: "修改成功",
                                duration: "1000",
                            });
                        }
                    }
                });
            }
        }
    })
</script>

</html>