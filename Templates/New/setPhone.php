<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <script src="/Style/Old/js/hotcss.js"></script>
    <style>
        #setPhone{font-size: 0.42666666rem;}
        .header_height{height: 1.2266666666666666rem;}
        #setPhone .header{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 2;
            height: 1.2266666666666666rem;
            line-height: 1.2266666666666666rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #setPhone .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            font-size: 0.42666666rem;
            line-height: 1.2266666666666666rem;
        }
        #setPhone .header .header__left{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #setPhone .header .header__left{left: 0.42666666rem;}
        #setPhone .header .header__left .van-icon{font-size: 0.42666666rem;}
        #setPhone .content{
            position: relative;
            padding: 0 0.42666666rem 0.533333rem;
            background: #fff;
            border-radius: 0.266666rem 0.266666rem 0 0;
        }
        #setPhone .content .item{
            line-height: 1.3rem;
        }

        #setPhone .content{text-align: center;padding: 0 1rem;}
        #setPhone .van-hairline--top-bottom::after,#setPhone .van-hairline-unset--top-bottom::after{border: none;}
        #setPhone .van-cell-group{border-bottom: 1px solid #eee;}
        #setPhone .submit{text-align: center;margin-top: 0.5333333333333333rem;}
        #setPhone .van-button{padding: 0 2rem;height: 1.1733333333333333rem;border-radius: 0.5866666666666667rem;}
        #setPhone .codeBtn{position: absolute;right: 0;top: 0;}
        #setPhone .codeBtn.getCode{color: #333;}
        #setPhone .codeBtn.disable{color: #999;}

        #setPhone .van-cell {
            padding: 0.26666666666666666rem 0.42666666rem;
            font-size: 0.37333333333333335rem;
            line-height: 0.64rem;
        }

        #setPhone .van-button{line-height: 1.12rem;}
        #setPhone .van-button--normal{font-size: 0.37333333333333335rem;}
        
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>绑定手机号</title>
</head>
<body>
    <div id="setPhone">
        <div class="header">
            <div class="bar">
                <div class="header__left">
                    <van-icon name="arrow-left" @click="back"/>
                </div>
                <div class="header__title">绑定手机号</div>
            </div>
        </div>
        <div class="header_height"></div>
        <div class="content">
            <van-cell-group>
                <van-field v-model="tel" type="tel" placeholder="请输入手机号" />
            </van-cell-group>
            <!-- <div class="van-cell-group van-hairline--top-bottom">
                <div class="van-cell van-field">
                    <div class="van-cell__value van-cell__value--alone">
                        <div class="van-field__body">
                            <input v-model="code" placeholder="请输入验证码" class="van-field__control">
                            <span class="codeBtn" @click="sendCode" :class="getCode ? 'able' : 'disable' ">{{getCode? '获取验证码':'重新发送('+timer + ')' }}</span>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="submit">
            <van-button @click="submit" type="primary">确认上传</van-button>
        </div>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#setPhone',
        data(){
            return {
                timer: 60,
                // getCode: true,
                // serviceCode: '',
                // serviceTel: '',
                tel: '<?php echo get_query_val("fn_user", "phone", array('userid' => $_SESSION['userid'])); ?>',
                // code: '',
                userInfo: {
                    userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
                    headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
                    id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                    userId: "<?php echo $_SESSION['userid']; ?>",
                },
            }
        },
        mounted() {
        },
        methods: {
            back(){
                window.history.go(-1)
            },
            // sendCode(){
            //     var reg = /^1[3456789]\d{9}$/;
            //     if(!reg.test(this.tel)){
            //         vant.Toast({
            //             message: "请输入正确的手机号码",
            //             duration: "1000",
            //         });
            //         return false;
            //     };
            //     let interval = setInterval(() => {
            //         this.timer--;
            //         if(this.timer === 0){
            //             this.getCode = true;
            //             this.editPhone = true;
            //             this.serviceCode = "";
            //             this.timer= 60;
            //             clearInterval(interval);
            //             return;
            //         }
            //     }, 1000);
            //     this.editPhone = false;
            //     this.getCode = false;
            //     this.serviceCode = "1234";
            //     this.serviceTel = this.tel;
            //     vant.Toast({
            //         message: "发送成功"+this.serviceCode,
            //         duration: "1000",
            //     });
            // },
            submit(){
                var _this = this;
                var reg = /^1[3456789]\d{9}$/;
                if(!reg.test(this.tel)){
                    vant.Toast({
                        message: "请输入正确的手机号码",
                        duration: "1000",
                    });
                    return false;
                };

                // if(this.serviceTel !== this.tel || this.serviceCode !== this.code){
                //     vant.Toast({
                //         message: "手机号或验证码不正确",
                //         duration: "1000",
                //     });
                //     return false;
                // }
                $.ajax({
                    type: "get",
                    url: "/Public/ShiroiInterface.php",
                    data: {
                        'f': 'upDateUserInfo',
                        'userid': _this.userInfo.userId,
                        'phone': _this.tel,
                    },
                    success: function(res){
                        res = JSON.parse(res);
                        delete res.data.pass;
                        if(!!res.data){
                            vant.Toast({
                                message: "修改成功",
                                duration: "1000",
                            });
                        }
                    }
                });
            }
        }
    })
</script>

</html>