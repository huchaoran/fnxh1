<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <script src="/Style/Old/js/hotcss.js"></script>
    <style>
        #userEdit{font-size: 0.42666666rem;}
        .header_height{height: 1.2266666666666666rem;}
        #userEdit .header{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 2;
            height: 1.2266666666666666rem;
            line-height: 1.2266666666666666rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #userEdit .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            font-size: 0.42666666rem;
            line-height: 1.2266666666666666rem;
        }
        #userEdit .header .header__left{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #userEdit .header .header__left{left: 0.42666666rem;}
        #userEdit .header .header__left .van-icon{font-size: 0.42666666rem;}
        #userEdit .content{
            margin-top: 0.4rem;
            position: relative;
            padding: 0 0.42666666rem 0.533333rem;
            background: #fff;
            border-radius: 0.266666rem 0.266666rem 0 0;
        }
        #userEdit .content .item{
            line-height: 1.3rem;
        }

        a:-webkit-any-link:active {
            color: #333;
        }
        a:-webkit-any-link {
            color: #333;
        }
        #userEdit .content .item span{}
        #userEdit .content .item .right span{display: inline-block;line-height: 1.3rem;vertical-align: top;}
        #userEdit .content .item .right{float: right;}
        #userEdit .headImg{width: 1.3rem;height: 1.3rem;vertical-align: bottom;}
        #userEdit .content .item .van-icon-arrow{
            vertical-align: top;
            line-height: 1.3rem;
        }

        #userEdit .content .item .van-switch{
            float: right;
            margin-top: 0.2133333rem;
        }
        #userEdit .clear{clear: both;}

        .van-toast {
            max-width: 80%;
        }

        .editName{text-align: center;padding: 1rem 1rem;box-sizing: border-box;width: 80vw;}
        .editName span{font-size: 0.42666666rem;color: #333333;}
        .editName .van-hairline--top-bottom::after, .editName .van-hairline-unset--top-bottom::after{border-top-width: 0px;}
        .editName .van-field__control{text-align: center;}
        .editName .input{margin-top: 0.4rem;}
        .editName .submit{margin-top: 0.8rem;}
        .editName .submit .van-button--normal{padding: 0 2rem;border-radius: 0.5866666666666667rem;line-height: 1.1733333333333333rem;}
        .editName .submit .van-button__text{color: #fff;}
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>用户管理</title>
</head>
<body>
    <div id="userEdit">
        <div class="header">
            <div class="bar">
                <div class="header__left">
                    <van-icon name="arrow-left" @click="back"/>
                </div>
                <div class="header__title">用户管理</div>
            </div>
        </div>
        <div class="header_height"></div>
        <div class="content">
            <div class="item">
                <a href="headEdit.php">
                    <div>
                        <span>头像</span>
                        <div class="right">
                            <img class="headImg" :src="userInfo.headImg" alt="">
                            <van-icon name="arrow"/>
                        </div>
                        <div class="clear"></div>
                    </div>
                </a>
            </div>
            <div class="item" @click="showPopup1">
                <div>
                    <span>昵称</span>
                    <div class="right">
                        <span>{{userInfo.userName}}</span>
                        <van-icon name="arrow"/>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <!-- <div class="item" @click="showPopup2">
                <div>
                    <span>性别</span>
                    <div class="right">
                        <span>{{userInfo.sex}}</span>
                        <van-icon name="arrow"/>
                    </div>
                    <div class="clear"></div>
                </div>
            </div> -->
            <div class="item">
                <a href="setPhone.php">
                    <div>
                        <span>手机号</span>
                        <div class="right">
                            <span>{{userInfo.tel ? userInfo.tel : "你未绑定手机号"}}</span>
                            <van-icon name="arrow"/>
                        </div>
                        <div class="clear"></div>
                    </div>
                </a>
            </div>
            <div class="item" @click="passEdit">
                <div>
                    <span>密码</span>
                    <div class="right">
                        <!-- <span>{{userInfo.pass ? userInfo.pass : "未设置"}}</span> -->
                        <van-icon name="arrow"/>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <van-popup v-model="showEdit" closeable >
            <div class="editName">
                <div class="text">
                    <span>修改昵称</span>
                </div>
                <div class="input">
                    <van-cell-group>
                        <van-field v-model="nickTemp" type="text" placeholder="请输入要修改的昵称" />
                    </van-cell-group>
                </div>
                <div class="submit">
                    <van-button @click="submit" type="primary">确认</van-button>
                </div>
            </div>
        </van-popup>
        <van-popup v-model="showPicker" position="bottom" closeable :style="{width: '100%',}">
            <van-picker :columns="columns" :default-index="userInfo.sex == '男' ? 0 : 1" @change="onChange" />
        </van-popup>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#userEdit',
        data(){
            return {
                nickTemp: "",
                showEdit: false,
                showPicker: false,
                columns: ['男','女'],
                userInfo: {
                    userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
                    headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
                    id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                    userId: "<?php echo $_SESSION['userid']; ?>",
                    tel: "<?php echo get_query_val("fn_user", "phone", array('userid' => $_SESSION['userid'])); ?>",
                },
            }
        },
        mounted() {
        },
        methods: {
            back(){
                window.history.go(-1)
            },
            passEdit(){
                if(this.userInfo.tel === ""){
                    vant.Toast({
                        message: "当前帐户未绑定/修改手机号，不能修改密码",
                        duration: "1500",
                    });
                    return false;
                }
                window.location.href = 'passEdit.php';
            },
            showPopup1() {
                this.showEdit = true;
            },
            showPopup2() {
                this.showPicker = true;
            },
            submit(){
                var _this = this;
                $.ajax({
                    url: "/Public/ShiroiInterface.php",
                    type: "get",
                    data: {
                        'f': 'upDateUserInfo',
                        'userid': _this.userInfo.userId,
                        'username': _this.nickTemp,
                    },
                    success: function(res){
                        res = JSON.parse(res);
                        delete res.data.pass;
                        _this.userInfo.userName = _this.nickTemp;
                        _this.showEdit = false;
                        if(!!res.data){
                            vant.Toast({
                                message: "修改成功",
                                duration: "1000",
                            });
                        }
                    }
                });
            },
            onChange(picker, value, index){
                this.userInfo.sex = value;
            }
        }
    })
</script>

</html>