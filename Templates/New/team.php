<?php
include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
$time = $_GET['time'] == "" ? 1 : (int)$_GET['time'];
switch($time){
case 1: $time = date('Y-m-d') . ' 00:00:00 - ' . date('Y-m-d') . ' 23:59:59';
    break;
case 7: $time = date('Y-m-d', strtotime('-7 day')) . ' 00:00:00 - ' . date('Y-m-d') . ' 23:59:59';
    break;
case 30: $time = date('Y-m-d', strtotime('-30 day')) . ' 00:00:00 - ' . date('Y-m-d') . ' 23:59:59';
    break;
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <script src="/Style/Old/js/hotcss.js"></script>
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <link rel="stylesheet" type="text/css" href="/Templates/user/css/bootstrap.min.css?v=1.2" />
    <style>
        #mine{font-size: 0.42666666rem;}
        #mine .header{
            position: relative;
            height: 5.33333rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #mine .header .bar{
            line-height: 1.226666666rem;
            background-color: rgb(53,168,241);
        }
        #mine .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            line-height: 1.226666666rem;
        }
        #mine .header .header__left,#mine .header .header__right{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #mine .header .header__left{left: 0.42666666rem;}
        #mine .header .header__left .van-icon{font-size: 0.42666666rem;}
        #mine .header .header__right{right: 0.42666666rem;bottom: auto;top: 0;color: #fff;}
        #mine .header .header__right a{color: #fff;}

        #mine .header .info{position: relative;padding: 0 0.426666666rem;}
        #mine .header .info .name:after{content: "";display: block;clear: both;}
        #mine .header .info__left{
            overflow: hidden;
            /* position: absolute;
            left: 0.42666666rem;
            top: 0.8rem; */
            float: left;
            width: 1.6rem;
            height: 1.6rem;
            border-radius: 50%;
            background-color: #fff;
        }
        #mine .header .info__left img{
            width: 1.493333333;
            height: 1.493333333rem;
            margin: 0.053333333rem;
        }
        #mine .header .info__right{
            /* position: absolute;
            top: 0.8rem;
            right: 0.42666666rem; */
            float: right;
            text-align: left;
            line-height: 0.8rem;
        }
        #mine .header .wallet{
            display: -webkit-flex;
            display: flex;
            justify-content: space-between;
            margin-top: 0.4rem;
            font-size: 0.37333333333333335rem;
        }


        #mine .content{
            position: relative;
            margin-top: -0.533333rem;
            /* padding: 0 0.42666666rem 0.533333rem; */
            padding: 0 0 0.533333rem;
            background: #fff;
            /* border-radius: 0.266666rem 0.266666rem 0 0; */
            text-align: center;
        }
        #mine .content .dateSelect{
            padding-top: 0.5066666666666667rem;
            display: -webkit-flex;
            display: flex;
            justify-content: space-between;
            font-size: 0.37333333333333335rem;
        }
        #mine .content .dateSelect>div{
            width: 33.3333%;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            color: #fff;
            background-color: #428bca;
            border-color: #357ebd;
            padding: 0.16rem 0.32rem;
            border-radius: 0.8533333333333334rem;
        }

        #mine .content .data{
            display: -webkit-flex;
            display: flex;
            justify-content: space-between;
            margin-top: 0.8rem;
            font-size: 0.37333333333333335rem;
            color: #666;
        }
        #mine .content .data th{
            font-weight: normal;
        }

        a:-webkit-any-link:active {
            color: #333;
        }
        a:-webkit-any-link {
            color: #333;
        }
        #mine .content .exit{
            position: fixed;
            bottom: 0;
            left:0;
            right: 0;
            text-align: center;
        }
        #mine .content .exit .van-button--normal{
            width: 100%;
            padding: 0 3.2rem;
            height: 1.1733333rem;
            line-height: 1.1733333rem;
            font-size: 0.373333333rem;
        }
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>团队信息</title>
</head>

<body>
    <div id="mine">
        <div class="header">
            <van-sticky>
                <div class="bar">
                    <div class="header__left">
                        <van-icon name="arrow-left" @click="back"/>
                    </div>
                    <div class="header__title">团队信息</div>
                </div>
            </van-sticky>
            <div class="info">
                <div class="name">
                    <div class="info__left">
                        <a href="headEdit.php">
                        <img :src="userInfo.headImg" :alt="userInfo.userName">
                        </a>
                    </div>
                    <div class="info__right">
                        <div>{{userInfo.userName}}</div>
                        <div>ID:{{userInfo.id}}</div>
                    </div>
                </div>
                <div class="wallet">
                    <div>
                        <span>{{userInfo.money}}</span>
                        <div>我的钱包</div>
                    </div>
                    <div>
                        <span><?php echo $info['yk']; ?></span>
                        <div>今日盈亏</div>
                    </div>
                    <div>
                        <span><?php echo $info['liu']; ?></span>
                        <div>今日流水</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="dateSelect">
                <div @click="select(1)">今日</div>
                <div @click="select(7)">7日</div>
                <div @click="select(30)">30日</div>
            </div>
            <div class="data">
                <table class="table table-bordered table-striped" width="100%" cellspacing="0" >
                    <thead>
                        <tr>
                            <th style="text-align:center">团队流水</th>
                            <th style="text-align:center">团队盈亏</th>
                            <th style="text-align:center">团队充值量</th>
                            <th style="text-align:center">团队余额</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr align="center">
                            <td><?php echo $team['liu']?></td>
                            <td><?php echo $team['yk']?></td>
                            <td><?php echo $team['pay']?></td>
                            <td><?php echo $team['money']?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="exit">
                <van-button type="danger" @click="logout">退出</van-button>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#mine',
        data: {
            userInfo: {
                userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
                headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
                id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                userId: "<?php echo $_SESSION['userid']; ?>",
                money: "<?php echo get_query_val("fn_user", "money", array('userid' => $_SESSION['userid'])); ?>",
            },
            dataList:[]
        },
        mounted() {

        },
        methods: {
            back(){
                window.location.href= 'mine.php';
            },
            select(num){
                window.location.href= `team.php?time=${num}`;
            },
            logout(){
                document.cookie = "PHPSESSID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
                document.cookie = "game=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
                document.cookie = "logintime=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
                window.location.href = '/'
            }
        }
    })
</script>

</html>