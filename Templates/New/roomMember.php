<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <script src="/Style/Old/js/hotcss.js"></script>
    <style>
        #roomMember{font-size: 0.42666666rem;
            /* display: flex; */
            flex-direction: column;
            height: 100vh;
        }
        .header_height{height: 1.2266666666666666rem;}
        #roomMember .header{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 2;
            height: 1.2266666666666666rem;
            line-height: 1.2266666666666666rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #roomMember .header .header__title{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            font-size: 0.42666666rem;
            line-height: 1.2266666666666666rem;
        }
        #roomMember .header .header__left,#roomMember .header .header__right{
            position: absolute;
            font-size: 0.373333333rem;
            bottom: auto;
            top: 0;
        }
        #roomMember .header .header__left{left: 0.42666666rem;}
        #roomMember .header .header__left .van-icon{font-size: 0.42666666rem;}
        #roomMember .header .header__right{right: 0.42666666rem;bottom: auto;top: 0;}
    
        #roomMember .van-pull-refresh{overflow: visible;flex: auto;}
        #roomMember .van-pull-refresh .van-pull-refresh__track{height: 100%;}
        #roomMember .content .list{padding-bottom: 1.4933333333333334rem;}
        #roomMember .content .list .item{
            width: 10rem;
            box-sizing: border-box;
            padding: 0 0.42666666rem;
            position: relative;
            height: 2rem;
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            align-items: center;
            border-bottom: 1px solid #eee;
            transition: all 0.3s;
            left: 0;
        }
        #roomMember .content .list .item .box{
            display: flex;
            display: -webkit-flex;
            justify-content: flex-start;
            align-items: center;
        }
        #roomMember .content .list .item .pic{margin-right: 0.26666666666666666rem;}
        #roomMember .content .list .item .pic img{width: 1.6rem;height: 1.6rem;}
        #roomMember .content .list .item div{color: #333;margin-bottom: 0.13333333333333333;font-size: 0.42666666rem;}
        #roomMember .content .list .item span{color: #999;font-size: 0.373333333rem;}

        #roomMember .content .list .item .score{float: right;}
        
        #roomMember .content .list .item .links a{margin-left: 0.6666666666666666rem;}
        #roomMember .content .list .item .links a img{width: 1.3333333333333333rem;height: 1.3333333333333333rem;}

        .allScore{
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            line-height: 1.5rem;
            font-size: 0.42666666rem;
            background-color: rgb(53,168,241);
            text-align: center;
            color: #ffffff;
        }
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>房间成员</title>
</head>
<body>
    <div id="roomMember">
        <div class="header">
            <div class="bar">
                <div class="header__left">
                    <van-icon name="arrow-left" @click="back"/>
                </div>
                <div class="header__title">房间成员 ( {{memberList.length}} )</div>
                <div class="header__right" @click="doSort">
                    {{ sort? '升序' : '降序' }}
                </div>
            </div>
        </div>
        <div class="header_height"></div>
        <van-pull-refresh v-model="isLoading" @refresh="onRefresh" success-text="刷新成功">
            <div class="content">
                <div class="search">
                    <van-search v-model="value" placeholder="请输入您要搜索的玩家ID或昵称" @search="onSearch"/>
                </div>
                <div class="list">
                    <div class="item" v-for="(item,index) in memberList" :key="index">
                        <div class="left">
                            <div class="box">
                                <div class="pic">
                                    <img :src="item.headimg" alt="">
                                </div>
                                <div class="name" >
                                    <div>{{item.username}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="right">
                            <div class="score">
                                <div>{{item.money}}分</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </van-pull-refresh>
        <div class="allScore">总计：{{allScore}}</div>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#roomMember',
        data(){
            return {
                isLoading: false,
                value: '',
                sort: false,//是否升序，默认降序
                userInfo: {
                    userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
                    headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
                    id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                    userId: "<?php echo $_SESSION['userid']; ?>",
                    roomId: "<?php echo get_query_val("fn_user", "roomid", array('userid' => $_SESSION['userid'])); ?>",
                },
                memberList: [],
                allMembers:[]
            }
        },
        mounted(){
            var _this = this;
            $.ajax({
                type: 'get',
                url: "/Public/ShiroiInterface.php",
                dataType: 'text',
                data: {
                    'f': 'getRoomUser',
                    'roomid': _this.userInfo.roomId,
                    'limit': 1000,
                    'page': 1,
                },
                success: function(res){
                    res = JSON.parse(res);
                    _this.memberList = res.data.data;
                    _this.allMembers = res.data.data;
                }
            });
            this.memberList.sort( (a,b) => b.money - a.money );
        },
        computed: {
            allScore: function(){
                var score = 0;
                const map1 = this.memberList.map( (x) => score += parseFloat(x.money) );
                return score;
            }
        },
        methods: {
            back(){
                window.history.go(-1);
            },
            doSort(){
                this.sort = !this.sort;
                if(this.sort){
                    this.memberList.sort( (a,b) => a.money - b.money );
                }else{
                    this.memberList.sort( (a,b) => b.money - a.money );
                }
            },
            onRefresh() {
                _this = this;
                $.ajax({
                    type: 'get',
                    url: "/Public/ShiroiInterface.php",
                    dataType: 'text',
                    data: {
                        'f': 'getRoomUser',
                        'roomid': _this.userInfo.roomId,
                        'limit': 1000,
                        'page': 1,
                    },
                    success: function(res){
                        res = JSON.parse(res);
                        _this.memberList = res.data.data;
                        _this.allMembers = res.data.data;
                        _this.isLoading = false;
                    }
                });
            },
            onSearch(){
                var arr = this.allMembers.filter( member => member.id.includes(this.value) || member.username.includes(this.value) );
                this.memberList = arr;
            },
        }
    })
</script>

</html>