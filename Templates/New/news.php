<?php

include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
require "function.php";
$info = getinfo($_SESSION['userid']);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <script src="/Style/Old/js/hotcss.js"></script>
    <link rel="stylesheet" href="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/index.css">
    <style>
        #news{font-size: 0.4266666666666667rem;}
        .header_height{height: 1.2266666666666666rem;}
        #news .header{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 2;
            height: 1.2266666666666666rem;
            line-height: 1.2266666666666666rem;
            text-align: center;
            background-color: rgb(53,168,241);
            color: #ffffff;
            -webkit-user-select: none;
            user-select: none;
        }
        #news .header .tab{
            max-width: 60%;
            margin: 0 auto;
            font-weight: 500;
            font-size: 0.4266666666666667rem;
            height: 1.2266666666666666rem;
            line-height: 0.6666666666666666rem;
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            align-items: center;
        }
        #news .header .tab .btn{
            text-align: center;
            width: 50%;
        }
        #news .header .tab .btn:after{
            content: "";
            display: block;
            width: 1.3333333333333333rem;
            height: 0.05333333333333334rem;
            background: transparent;
            margin: auto;
        }
        #news .header .tab .btn.active:after{background: #fff;}
        #news .header .header__left{
            position: absolute;
            font-size: 0.37333333333333335rem;
            bottom: auto;
            top: 0;
        }
        #news .header .header__left{left: 0.4266666666666667rem;}
        #news .header .header__left .van-icon{font-size: 0.4266666666666667rem;}
    
        #news .van-pull-refresh{overflow: visible;}
        #news .content{min-height: 92vh;}
        #news .tab-content .list{padding: 0.26666666666666666rem 0.4266666666666667rem;border-bottom: 1px solid #eee;}
        #news .tab-content .list .title{color: #333;margin-bottom: 0.05333333333333334rem;}
        #news .tab-content .list .details{font-size: 0.37333333333333335rem;color: #999;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;}
    </style>
    <script src="../../Style/Old/js/jquery.min.js"></script>
    <!-- vue vant js-->
    <script src="https://cdn.suoluomei.com/common/js2.0/vue/v2.5.16/vue.js"></script>
    <script src="https://cdn.suoluomei.com/common/js2.0/npm/vant@2.2/lib/vant.min.js"></script>
    <title>我的消息</title>
</head>
<body>
    <div id="news">
        <div class="header">
            <div class="bar">
                <div class="header__left">
                    <van-icon name="arrow-left" @click="back"/>
                </div>
                <div class="tab">
                    <div class="btn" v-bind:class="{ active : emails }" @click="tab('emails')" name="emails" >邮件</div>
                    <div class="btn" v-bind:class="{ active : notices }" @click="tab('notices')" name="notices" >公告</div>
                </div>
            </div>
        </div>
        <div class="header_height"></div>
        <van-pull-refresh v-model="isLoading" @refresh="onRefresh" success-text="刷新成功">
            <div class="content">
                <div class="tab-content">
                    <div class="emails" v-show="emails">
                        <div class="list" v-for="(item,index) in news.emails" @click="read('emails',index)">
                            <!-- <a :href="item.link"> -->
                                <!-- <div class="title">管理员</div> -->
                                <div class="details">{{item.username}}：{{item.content}} <span style="float: right;">{{item.addtime}}</span></div>
                            <!-- </a> -->
                        </div>
                    </div>
                    <div class="notices" v-show="notices">
                        <div class="list" v-for="(item,index) in news.notices" @click="read('notices',index)">
                            <!-- <a :href="item.link"> -->
                                <div class="title">{{item.title}}</div>
                                <div class="details">{{item.isRead? "[已读]":"[未读]"}}{{item.details}}</div>
                            <!-- </a> -->
                        </div>
                    </div>
                </div>
            </div>
        </van-pull-refresh>
    </div>
</body>
<script type="text/javascript">
    var app = new Vue({
        el: '#news',
        data(){
            return {
                emails: true,
                notices: false,
                isLoading: false,
                userInfo: {
                    userName: "<?php echo get_query_val("fn_user", "username", array('userid' => $_SESSION['userid'])); ?>",
                    headImg:"<?php echo get_query_val("fn_user", "headimg", array('userid' => $_SESSION['userid'])); ?>",
                    id: "<?php echo get_query_val("fn_user", "id", array('userid' => $_SESSION['userid'])); ?>",
                    userId: "<?php echo $_SESSION['userid']; ?>",
                },
                news:{
                    emails:[],
                    notices:[]
                },
            }
        },
        mounted() {
            var _this = this;
            $.ajax({
                url: '/Public/ShiroiInterface.php',
                type: 'GET',
                data: {
                    'f': 'custom',
                    'userid': _this.userInfo.userId,
                    'user_group': '',
                },
                success(res){
                    res = JSON.parse(res);
                    console.log(res.data);
                    _this.news.emails = res.data;
                    _this.isLoading = false;
                },
                error(err){
                    console.log(err);
                    _this.isLoading = false;
                }
            });
        },
        methods: {
            back(){
                window.history.go(-1);
            },
            tab(active){
                if(active==="notices"){
                    this.notices = true;
                    this.emails = false;
                }else if(active==="emails"){
                    this.notices = false;
                    this.emails = true;
                }
            },
            read(name,index){
                this.news[name][index].isRead = true;
                vant.Toast({
                    message: "阅读成功",
                    duration: "1000",
                });
            },
            onRefresh() {
                var _this = this;
                $.ajax({
                    url: '/Public/ShiroiInterface.php',
                    type: 'GET',
                    data: {
                        'f': 'custom',
                        'userid': _this.userInfo.userId,
                        'user_group': '',
                    },
                    success(res){
                        res = JSON.parse(res);
                        _this.news.emails = res.data;
                        _this.isLoading = false;
                    },
                    error(err){
                        _this.isLoading = false;
                    }
                });
            }
        }
    })
</script>

</html>