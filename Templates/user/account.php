<?php
include dirname(dirname(dirname(preg_replace('@\(.*\(.*$@', '', __FILE__)))) . "/Public/config.php";
$map['userid']=$_SESSION['userid'];
$user = $mydb->table('fn_user')->where($map)->find();
if(empty($user['loginuser'])){
	$_action = 'add';
}else{
	$_action = 'edit';
}
$action = $_GET['action'];
$loginuser = $_POST['loginuser'];
$loginpass = $_POST['loginpass'];
$reloginpass = $_POST['reloginpass'];
$r = array();
$r['status'] = 0;
$r['msg'] = '未知错误';
if($action == 'add'){
	if(empty($loginuser)){
		$r['msg'] = '用户名不能为空';
		exit(json_encode($r)) ;
	}
	if(empty($loginpass)){
		$r['msg'] = '密码不能能为空';
		exit(json_encode($r)) ;
	}
	if($loginpass != $reloginpass){
		$r['msg'] = '两次密码不一致';
		exit(json_encode($r)) ;
	}
	$data['loginuser'] = $loginuser;
	$data['loginpass'] = md5($loginpass);
	$user = $mydb->table('fn_user')->where($map)->data($data)->update();
	$r['status'] = 1;
	$r['msg'] = '设置帐户信息成功';
	exit(json_encode($r));
}elseif($action == 'edit'){
	$data = array();
	$oldloginpass = $_POST['oldloginpass'];
	if(empty($oldloginpass)){
		$r['msg'] = '旧密码不能为空';
		exit(json_encode($r)) ;
	}
	if(empty($loginpass)){
		$r['msg'] = '密码不能能为空';
		exit(json_encode($r)) ;
	}
	if($loginpass != $reloginpass){
		$r['msg'] = '两次密码不一致';
		exit(json_encode($r)) ;
	}
	if(md5($oldloginpass) != $user['loginpass']){
		$r['msg'] = '旧密码不正确';
		exit(json_encode($r)) ;
	}
	$data['loginpass'] = md5($loginpass);
	$user = $mydb->table('fn_user')->where($map)->data($data)->update();
	$r['status'] = 1;
	$r['msg'] = '修改密码成功';
	exit(json_encode($r));
	
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="user-scalable=no,width=device-width" />
    <meta name="baidu-site-verification" content="W8Wrhmg6wj" />
    <meta content="telephone=no" name="format-detection">
    <meta content="1" name="jfz_login_status">
    <script type="text/javascript" src="js/record.origin.js"></script>
    
    <link rel="stylesheet" type="text/css" href="css/common.css?v=1.2" />
    <link rel="stylesheet" type="text/css" href="css/new_cfb.css?v=1.2" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css?v=1.2" />

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.7.2.js?v=1.2"></script>
    <script type="text/javascript" src="js/global.js?v=1.2"></script>
    <script type="text/javascript" src="js/common.v3.js?v=1.2"></script>
    <script type="text/javascript" src="js/jweixin-1.0.0.js"></script>
    <title>个人中心</title>
    <style>
    	.form-group input{border: 1px solid #ccc;padding: 6px 0px;}
    	
    </style>
</head>

<body>

    <div class="wx_cfb_container wx_cfb_account_center_container">
        <div class="wx_cfb_account_center_wrap">
            <div class="wx_cfb_ac_fund_detail">
                <div class="user_info clearfix">
                    <div class="user_photo"><img src="<?php echo $_SESSION['headimg'];
?>" style="width:45px; height:45px; "></div>
                    <div class="user_txt">
                        <div class="p1">
                            <?php echo $_SESSION['username'];
?>
                        </div>
                        <div class="p2">欢迎来到【<?php echo get_query_val("fn_room", "roomname", array("roomid" => $_SESSION['roomid']));
?>】娱乐房间</div>
                    </div>
                </div>
                <div class="fund_info">
                    <div class="kv_tb_list clearfix">
                        <div class="kv_item">
                            <span class="val"><?php echo get_query_val("fn_user", "money", array("roomid" => $_SESSION['roomid'], 'userid' => $_SESSION['userid']));
?></span>
                            <span class="key">我的钱包</span>
                        </div>
                        <div class="kv_item">
                            <span class="val"><?php echo $info['yk'];
?></span>
                            <span class="key">今日盈亏</span>
                        </div>
                        <div class="kv_item">
                            <span class="val"><?php echo $info['liu'];
?></span>
                            <span class="key">今日流水</span>
                        </div>
                    </div>
                </div>
            </div>
            <!--入口-->
            
            <div class="" style="padding: 10px;">
            	<p class="bg-danger" style="padding: 10px 0px;">用户名设置后不可修改</p>
                <form id = "form">
                  <?php if($_action == 'add'):?>
				  <div class="form-group ">
				    <label for="exampleInputEmail1">用户名</label>
				    <input type="text" class="form-control" id="loginuser" name="loginuser" placeholder="用户名">
				  </div>
				  <?php else:?>
				  	<div class="form-group ">
					    <label for="exampleInputEmail1">用户名：<?php echo $user['loginuser']?></label>
					    
					</div>
					<div class="form-group ">
				    	<label for="exampleInputEmail1">旧密码</label>
				    	<input type="text" class="form-control" id="oldloginpass" name="oldloginpass" placeholder="输入旧密码">
				    </div>
				  <?php endif;?>
				  <div class="form-group">
				    <label for="exampleInputPassword1">密码</label>
				    <input type="password" class="form-control" id="loginpass" name="loginpass" placeholder="密码">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">确认密码</label>
				    <input type="password" class="form-control" id="reloginpass" name="reloginpass" placeholder="确认密码">
				  </div>
				  <button id="btsubmit" type="button" class="btn btn-primary btn-block btn-lg">提交</button>
				</form>
            </div>
        </div>
    </div>

    <div class="wx_cfb_fixed_btn_box">
        <div class="wx_cfb_fixed_btn_wrap">
            <div class="btn_box clearfix">
                <a href="/qr.php?room=<?php echo $_SESSION['roomid'];
?>" class="btns tel_btn clearfix">
                    <em class="ico ui_ico_size_40 ui_tel_ico"></em><span class="txt">返回游戏</span>
                </a>
            </div>
        </div>
    </div>

    </div>
    </body>
	 <script>
            $(function(){
                $('#btsubmit').click(function(){
                    var urlstrings=$("#form").serialize();
                    $.post('account.php?action=<?php echo $_action?>',urlstrings,function(data){
                    	console.debug(data);
                    	if(data.status == 0){
                    		alert(data.msg);
                    	}else{
                    		alert(data.msg);
                    		window.location.reload(true);
                    	}
                    },'json')
                });
            });
        </script>
</html>