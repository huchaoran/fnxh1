<?php
//v9ym。com下载源码
header("Content-type:text/html;charset=utf-8");
include_once "Public/config.php";
$room = $_GET['room'] ? $_GET['room'] : '1';
$g = $_GET['g'] ? $_GET['g'] : 'pk10';
if ($room == '') {
    $room = $_SESSION['roomid'];
}
if (room_isOK($room)) {
    $_SESSION['roomid'] = $room;
    $sitename = get_query_val('fn_room', 'roomname', array('roomid' => $room));

    setcookie('logintime', 'temp', time() + 1800);
} else {
    $_SESSION['error_room'] = $room;
    //echo '555';exit;
    require "Templates/error.php";
    exit();
}
$roomtime = get_query_val('fn_room', 'roomtime', array('roomid' => $room));
if (strtotime($roomtime) - time() < 0) {
    echo "<center><strong style='font-size:80px;'>您所访问的房间ID:{$room} <br>已于 <font color=red>$roomtime</font> 到期！<br>请提醒管理员进行续费！</strong></center>";
    exit;
}
if ($_GET['agent'] != "") {
    setcookie('agent', $_GET['agent'], time() + 36000);
    $_COOKIE['agent'] = $_GET['agent'];
}
if ($_SESSION['userid'] == "") {
    $url = "/web_login.php?agent='{$_COOKIE['agent']}'&room={$_GET['room']}";
    // echo $url ;
    header('Location:' . $url);exit;

}
$templates = get_query_val('fn_setting', 'setting_templates', array('roomid' => $_SESSION['roomid']));
if ($templates == 'old') {
    require 'Templates/Old/index.php';
} elseif ($templates == 'new') {
    require 'Templates/New/index.php';
}
?>