<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"
          content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
    <link rel="stylesheet" href="css/jquery-labelauty.css">
    <link rel="stylesheet" href="/NewUI/css/mui.min.css">
    <link href="/NewUI/css/common1.css" rel="stylesheet"/>
    <link href="/NewUI/css/index1.css" rel="stylesheet"/>
    <link href="/NewUI/font/iconfont.css" rel="stylesheet"/>

    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery-labelauty.js"></script>
    <title>官方充值</title>
</head>
<style type="text/css">
    .abody {
        background-color: #f0f0f0;

    }

    .bianju {
        margin-top: 20px;
        margin-right: 5px;
        margin-bottom: 30px;
        margin-left: 5px;
    }

    .kuangjia {
        background-color: #ffffff;
        border-radius: 10px 10px 10px 10px;
        border: 1px dashed #949494;
        margin-top: 10px
    }

    .h2 {
        margin-left: 20px;
        font-size: 10px;
    }

    .botton {

        margin-right: 20px;
        margin-bottom: 10px;
        margin-left: 20px;
        text-align: center;
        width: 240px;
        height: 40px;
    }


</style>

<body class="abody">
<?php
session_start();
$_SESSION['game'] = $_GET['g'];
$_SESSION['roomid'] = $_GET['roomid'];
$_SESSION['headimg'] = $_GET['img'];
$_SESSION['username'] = $_GET['m'];
$_SESSION['userid'] = $_GET['id'];

?>
<div class="bianju">

    <img src="./images/paylogo.png" style="width:50%;height:auto;margin:0 auto;">

    <form class="kuangjia">

        <div class="h2">
            <h2>&nbsp&nbsp充值金额：</h2>
        </div>
        <ul class="dowebok">
            <li><input type="radio" name="inputprice" value="10" checked="" data-labelauty="10元"></li>
            <li><input type="radio" name="inputprice" value="20" data-labelauty="20元"></li>
            <li><input type="radio" name="inputprice" value="30" data-labelauty="30元"></li>
            <li><input type="radio" name="inputprice" value="50" data-labelauty="50元"></li>
            <li><input type="radio" name="inputprice" value="100" data-labelauty="100元"></li>
            <li><input type="radio" name="inputprice" value="200" data-labelauty="200元"></li>
            <li><input type="radio" name="inputprice" value="300" data-labelauty="300元"></li>
            <li><input type="radio" name="inputprice" value="500" data-labelauty="500元"></li>
            <li><input type="radio" name="inputprice" value="1000" data-labelauty="1000元"></li>
            <li><input type="radio" name="inputprice" value="2000" data-labelauty="2000元"></li>
            <li><input type="radio" name="inputprice" value="3000" data-labelauty="3000元"></li>
            <li><input type="radio" name="inputprice" value="5000" data-labelauty="5000元"></li>
            <li><input type="radio" name="inputprice" value="10000" data-labelauty="10000元"></li>
            <li><input type="radio" name="inputprice" value="15000" data-labelauty="15000元"></li>
        </ul>
        <div class="h2">
            <h2>&nbsp&nbsp充值方式：</h2>
        </div>
        <ul class="dowebok">
            <li><input type="radio" name="demo1" value="4" data-labelauty="QQ支付"></li>
            <li><input type="radio" name="demo1" value="1" data-labelauty="支付宝支付"></li>
            <li><input type="radio" name="demo1" value="2" checked="" data-labelauty="微信支付"></li>
            <br>
            <button type="button" id="demoBtn1" class="botton">立即充值</button>
        </ul>


    </form>


</div>
<form style='display:none;' id='formpay' name='formpay' method='post' action='https://bell.eysj.net/pay'>
    <input name='goodsname' id='goodsname' type='text' value=''/>
    <input name='istype' id='istype' type='text' value=''/>
    <input name='key' id='key' type='text' value=''/>
    <input name='notify_url' id='notify_url' type='text' value=''/>
    <input name='orderid' id='orderid' type='text' value=''/>
    <input name='orderuid' id='orderuid' type='text' value=''/>
    <input name='price' id='price' type='text' value=''/>
    <input name='return_url' id='return_url' type='text' value=''/>
    <input name='uid' id='uid' type='text' value=''/>
    <input type='submit' id='submitdemo1'>

</form>

<div class="items bg-fff p20 center-float-center">

    <!--color_style-->

    <dl class="flex-1 ">
        <a href="/">
            <dt class="text-center"><i class=" iconfont icon-youxidating color-red"></i></dt>
            <dd class="text-center color-red ">游戏大厅</dd>
    </dl>

    <dl class="flex-1 text-center">
        <a href="/Templates/New/tgzq.php">
            <dt><i class="iconfont icon-goucai"></i></dt>
            <dd class="text-center">我的推广</dd>
        </a>
    </dl>
    <dl class="flex-1 text-center">
        <a class="nav-item" href="#" onclick="openKefu()">
            <dt><i class="iconfont icon-kaijiang"></i></dt>
            <dd class="text-center">客服</dd>
        </a>


        <div style="width:100%;height:300px; background-color:#FFF; border-radius:15px; z-index::1000; display:none; position:fixed;bottom:70px;left:0px; "
             id="kefuDiv">
            <table width="95%" border="0" cellspacing="0" cellpadding="0" style="padding-top:10px; margin:0px auto;">
                <tr>
                    <td align="right" width="3%"><img src="Style/images/gamehall/11.png" width="20"/></td>
                    <td align="left"><font face="微软雅黑">联系客服</font></td>
                    <td align="right"><img src="Style/images/gamehall/22.png" width="20" onclick="openKefu()"/></td>
                </tr>
            </table>
            <div style="width:100%; height:1px; background-color:#999; margin-top:10px;"></div>
            <table width="95%" border="0" cellspacing="0" cellpadding="0" style="padding-top:10px; margin:0px auto;">
                <tr>
                    <td align="center"><font face="微软雅黑">客服微信号：</font></td>
                </tr>
                <tr>
                    <td align="center">
                        <div class="kf">
                            <a href="#"><img src="<?php echo $sql['setting_qrcode']; ?>" width="50%"/></a>
                        </div>
                    </td>
                </tr>
            </table>

        </div>


    </dl>

    <dl class="flex-1 text-center">
        <a class="nav-item" href="/Templates/user/index.php">
            <dt><i class="iconfont icon-gerenzhongxin"></i></dt>
            <dd class="text-center">个人中心</dd>
        </a>
    </dl>
    </footer>
</body>
<script src="NewUI/js/jquery-3.3.1.min.js"></script>
<script src="NewUI/js/mui.min.js"></script>
<script type="text/javascript">

    mui.plusReady(function () {
        mui.init({
            swipeBack: true //启用右滑关闭功能
        });
        //获得slider插件对象
        var gallery = mui('.mui-slider');
        gallery.slider({
            interval: 500//自动轮播周期，若为0则不自动播放，默认为0；
        });
    });


    var btnArray = ['我知道了'];
    mui.confirm('<br><img style = "width: 200px high: 200px;" src = "<?php echo $sql['setting_qrcode']; ?>" > ', '欢迎光临<?php echo $sitename ?>', btnArray, function(e) {

    })
    ;


</script>


</html>


<!-- 以下是统计及其他信息，与演示无关，不必理会 -->


<!-- Jquery files -->
<script type="text/javascript" src="https://cdn.staticfile.org/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">

    var jQuery_1_11_1 = $.noConflict(true);

</script>

<script type="text/javascript">
    $().ready(function () {

        function getistype() {
            var radioss = document.getElementsByName("demo1");
            for (var i = 0; i < radioss.length; i++) {
                if (radioss[i].checked) {
                    return (radioss[i].value);
                }
            }
        }


        function judgeRadioClicked() {
            var radios = document.getElementsByName("inputprice");
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].checked) {
                    return (radios[i].value);
                }
            }
        }

        $("#demoBtn1").click(function () {
            $.post(
                "pay.php",
                {
                    price: judgeRadioClicked(),
                    istype: getistype(),
                },
                function (data) {
                    if (data.code > 0) {
                        $("#goodsname").val(data.data.goodsname);
                        $("#istype").val(data.data.istype);
                        $('#key').val(data.data.key);
                        $('#notify_url').val(data.data.notify_url);
                        $('#orderid').val(data.data.orderid);
                        $('#orderuid').val(data.data.orderuid);
                        $('#price').val(data.data.price);
                        $('#return_url').val(data.data.return_url);
                        $('#uid').val(data.data.uid);
                        $('#submitdemo1').click();

                    } else {
                        alert(data.msg);
                    }
                }, "json"
            );
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $(':input').labelauty();
    });
</script>


</body>
</html>