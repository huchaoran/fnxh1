<?php
/**
 * ---------------------参数生成页-------------------------------
 * 在您自己的服务器上生成新订单，并把计算好的订单信息传给您的前端网页。
 * 注意：
 * 1.key一定要在服务端计算，不要在网页中计算。
 * 2.token只能存放在服务端，不可以以任何形式存放在网页代码中（可逆加密也不行），也不可以通过url参数方式传入网页。
 * 3.接口跑通后，如果发现收款二维码是我们官方的，请检查APP是否正在运行。为保障您收款功能正常，如果您的收款手机APP掉线超过一分钟，就会触发代收款机制，详情请看网站帮助。
 *  接口修复定制开发 ： QQ1878336950
 * --------------------------------------------------------------
 */
 session_start();
	include_once("../Public/config.php");
    include_once('./mysqli.php');
    $price = $_POST["price"];
    $istype = $_POST["istype"];
    $mode = $_POST["mode"];//0上分 1下分
    $userid = $_SESSION['username'];
    $headimg = $_SESSION['headimg'];
    $goodsname = $mode?'下分':'上分';
    $game = $_SESSION['game'];  
    $orderuid = $_SESSION['userid'];    
    $roomid = $_SESSION['roomid'];
    $orderid = StrOrderOne();
    $usermoney = get_query_val('fn_user', 'money', array('userid' => $orderuid, 'roomid' => $roomid));
    $jieguo = '未处理';
    $jia = 'false';
    $sql = get_query_vals('fn_setting','*',array('roomid'=>$roomid));
    $uid = $sql['sid'];
    $token = $sql['skey'];
   // $return_url = 'http://ymh8.cn/wpay/payreturn.php';
    $return_url = 'http://stitej.com/qr.php?room='.$roomid;
    $notify_url = 'http://stitej.com/wpay/paynotify.php';
    $key = md5($goodsname. $istype . $notify_url . $orderid . $roomid . $price . $return_url . $token . $uid);
    $money = intval($mode)==1?intval($usermoney)-$price:($usermoney)+$price;
    $returndata['goodsname'] = $goodsname;
    $returndata['istype'] = $istype;
    $returndata['key'] = $key;
    $returndata['notify_url'] = $notify_url;
    $returndata['orderid'] = $orderid;
    $returndata['orderuid'] = $roomid;
    $returndata['price'] = $price;
//    $returndata['balance'] = $money;
    $returndata['balance'] = $usermoney;//提交上分并不是直接改用户金额，而是后台修改
    $returndata['return_url'] = $return_url;
    $returndata['uid'] = $uid;
//" update fn_user set money=money-11 where roomid={$roomid} and userid={$orderuid}"
//插入数据
	$time = date('Y-m-d H:i:s');
   file_put_contents("callback_log1.txt", json_encode($_SESSION));
    insert_query('fn_upmark',array('userid'=>$orderuid,'headimg'=>$headimg,'username'=>$userid,'type'=>$goodsname,'money'=>$price,'time'=>$time,'status'=>$jieguo,'game'=>$game,'roomid'=>$roomid,'jia'=>$jia,'orderid'=>$orderid));
    //根據上下分抵扣用户余额
//    update_query('fn_user',['money'=>$money],"roomid={$roomid} and userid='{$orderuid}'");
    echo jsonSuccess("OK",$returndata,"");
    //返回错误
    function jsonError($message = '',$url=null) 
    {
        $return['msg'] = $message;
        $return['data'] = '';
        $return['code'] = -1;
        $return['url'] = $url;
        return json_encode($return);
    }
    //返回正确
    function jsonSuccess($message = '',$data = '',$url=null) 
    {
        $return['msg']  = $message;
        $return['data'] = $data;
        $return['code'] = 1;
        $return['url'] = $url;
        return json_encode($return);
    }	
	function StrOrderOne(){
		date_default_timezone_set('Asia/Shanghai');
		/* 选择一个随机的方案 */
		mt_srand((double) microtime() * 1000000);
		return date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
	}

?>